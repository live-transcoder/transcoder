#include <algorithm>

#include <QColor>
#include <QBrush>

#include <utility>

#include "transcodersmodel.h"

#define COLUMNS 5

TranscodersModel::TranscodersModel(QObject *parent) :
    QAbstractTableModel(parent),
    fakeData()
{
}

void TranscodersModel::updateTranscoderData(const QList<TranscoderData> &data)
{
    beginResetModel();
    this->transcoders = data;
    endResetModel();
}

void TranscodersModel::updateTranscoderData(int index, const TranscoderData &data)
{
    if (index >= rowCount())
        return;

    this->transcoders[index] = data;

    QModelIndex idx1 = this->index(index, 0);
    QModelIndex idx2 = this->index(index, columnCount() - 1);
    emit dataChanged(idx1, idx2);
}

void TranscodersModel::updateTranscoderData(const QModelIndex &index, const TranscoderData &data)
{
    if (!index.isValid() || index.row() >= rowCount())
        return;

    updateTranscoderData(index.row(), data);
}

void TranscodersModel::putTranscoderData(const QString &transcoderName, const TranscoderData &data)
{
    QList<TranscoderData>::iterator find;
    find = std::find_if(transcoders.begin(), transcoders.end(), MatchTranscoderByName(transcoderName));

    if (find == transcoders.end())
    {
        // New data
        addTranscoderData(data);
    }
    else
    {
        // Exists data
        int index = find - transcoders.begin();
        updateTranscoderData(index, data);
    }
}

const TranscoderData &TranscodersModel::getTranscoderData(int index) const
{
    if (index >= rowCount())
        return fakeData;

    return transcoders[index];
}

const TranscoderData &TranscodersModel::getTranscoderData(const QModelIndex &index) const
{
    if (!index.isValid())
        return fakeData;

    return getTranscoderData(index.row());
}

const QList<TranscoderData> &TranscodersModel::getTranscoderData() const
{
    return transcoders;
}

void TranscodersModel::addTranscoderData(const TranscoderData &data)
{
    beginInsertRows(QModelIndex(), transcoders.size(), transcoders.size());
    transcoders.append(data);
    endInsertRows();
}

void TranscodersModel::removeTranscoderData(int index)
{
    if (index >= rowCount())
        return;

    beginRemoveRows(QModelIndex(), index, index);
    transcoders.removeAt(index);
    endRemoveRows();

}

void TranscodersModel::removeTranscoderData(const QModelIndex &index)
{
    if (!index.isValid())
        return;

    removeTranscoderData(index.row());
}

void TranscodersModel::removeTranscoderData(const QString &name)
{
    QList<TranscoderData>::iterator find;
    find = std::find_if(transcoders.begin(), transcoders.end(), MatchTranscoderByName(name));

    if (find != transcoders.end())
    {
        removeTranscoderData(find - transcoders.begin());
    }
}

void TranscodersModel::clearTranscoderData()
{
    transcoders.clear();
    reset();
}

int TranscodersModel::rowCount(const QModelIndex &parent) const
{
    return transcoders.size();
}

int TranscodersModel::columnCount(const QModelIndex &parent) const
{
    return COLUMNS;
}

QVariant TranscodersModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (index.row() >= transcoders.size())
        return QVariant();

    switch (role)
    {
        case Qt::DisplayRole:
            switch (index.column())
            {
                case 0:
                    return transcoders.at(index.row()).name;
                case 1:
                    return transcoders.at(index.row()).description;
                case 2:
                    return (transcoders.at(index.row()).enabled ? tr("Yes") : tr("No"));
                case 3:
                    return (transcoders.at(index.row()).started ? tr("Yes") : tr("No"));
                case 4:
                    return (transcoders.at(index.row()).active ? tr("Yes") : tr("No"));
            }
            break;

        case Qt::BackgroundRole:
            switch (index.column())
            {
                case 2:
                    return (transcoders.at(index.row()).enabled ?
                                QBrush(QColor(0, 200, 0)) :
                                QBrush(QColor(200, 250, 0)));
                case 3:
                    return (transcoders.at(index.row()).started  ?
                                QBrush(QColor(0, 200, 0)) :
                                QBrush(QColor(200, 250, 0)));
                case 4:
                    return (transcoders.at(index.row()).active  ?
                                QBrush(QColor(0, 200, 0)) :
                                transcoders.at(index.row()).started ?
                                    QBrush(QColor(200, 0, 0)) :
                                    QBrush(QColor(200, 250, 0)));
            }
            break;

        case Qt::TextAlignmentRole:
            switch (index.column())
            {
                case 2:
                case 3:
                case 4:
                    return QVariant(Qt::AlignHCenter | Qt::AlignVCenter);
            }
            break;

        case Qt::ToolTipRole:
            // TODO: tool tip for line
            break;
    }

    return QVariant();
}

QVariant TranscodersModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Vertical && role == Qt::DisplayRole)
    {
        return QString("%1").arg(section + 1);
    }
    else
    {
        switch (role)
        {
            case Qt::DisplayRole:
                switch (section)
                {
                    case 0:
                        return tr("Name");
                    case 1:
                        return tr("Description");
                    case 2:
                        return tr("Enabled");
                    case 3:
                        return tr("Started");
                    case 4:
                        return tr("Active");
                }
                break;
        }
    }

    return QVariant();
}

