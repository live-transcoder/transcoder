#ifndef TRANSCODER_TRANSPORT_HTTP_H
#define TRANSCODER_TRANSPORT_HTTP_H

#include <map>

#include <boost/thread/mutex.hpp>

#include "applicationcontext.h"
#include "transport.h"
#include "muxer.h"

namespace transcoder {
namespace transport {

class ResourceHandler;
DECLARE_PTR_TYPE(ResourceHandler);

/**
 * HTTP transport implementation, using builtin HTTP server (that uses for administration)
 *
 * You must register it before use, like:
 *    transcoder::transport::Http::registerTransport();
 *
 */
class Http : public Transport<Http>
{
public:
    virtual BaseTransport::ResourceRegistrationStatus
    registerResource(const string &resource,
                     const ProducerPtr &producer,
                     const Options &options);

    virtual bool unregisterResource(const string &resource);

    virtual const std::string getTransportName();

private:
    std::string makeServerResource(const std::string& resource);

private:
    boost::mutex                              handlersMutex;
    std::map<std::string, ResourceHandlerPtr> handlers;

};

} // namespace transport
} // namespace transcoder

#endif // TRANSCODER_TRANSPORT_HTTP_H
