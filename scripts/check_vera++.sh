#!/bin/bash

vera++ -nofail -nodup  \
    ../src/av/*.cpp \
    ../src/pion/*.cpp \
    ../src/transcoder/*.cpp
