# transcoder sources

include_directories(.)
include_directories(..)
include_directories(../http-server/mini-http-server)
include_directories(../util)

file(GLOB_RECURSE TRANSCODER_SOURCES "*.cpp")
file(GLOB_RECURSE TRANSCODER_HEADERS "*.h" "*.hpp")

set(TRANSCODER_TARGET transcoder)

# BOOST
file(GLOB_RECURSE BOOST_SOURCES "../boost/*.cpp")
file(GLOB_RECURSE BOOST_HEADERS "../boost/*.h" "../boost/*.hpp")

# UTILS
file(GLOB_RECURSE UTIL_SOURCES "../util/*.cpp")
file(GLOB_RECURSE UTIL_HEADERS "../util/*.h" "../util/*.hpp")

list(APPEND TRANSCODER_SOURCES ${AV_SOURCES} ${BOOST_SOURCES} ${PION_SOURCES} ${UTIL_SOURCES})
list(APPEND TRANSCODER_HEADERS ${AV_HEADERS} ${BOOST_HEADERS} ${PION_HEADERS} ${UTIL_HEADERS})

include_directories(${Boost_INCLUDE_DIRS})

add_executable(${TRANSCODER_TARGET} ${TRANSCODER_SOURCES})
target_link_libraries(${TRANSCODER_TARGET}
    ${CMAKE_THREAD_LIBS_INIT}
    ${Boost_LIBRARIES}
    ${Soci_LIBRARIES}
    ${Pion_LIBRARIES}
    ${FFMPEG_LIBRARIES}
    avcpp
    http_server
    logstream
    rt
)

if(WIN32)
target_link_libraries(${TRANSCODER_TARGET}
    ws2_32
)
endif()
