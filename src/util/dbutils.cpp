#include <exception>

#include "dbutils.h"

void checkDbSession(soci::session &sql)
{
    try
    {
        sql << "SELECT 1";
        if (sql.got_data())
            return;

        throw std::runtime_error("Not ready connection");
    }
    catch (const std::exception &e)
    {
        // Try reconnect
        sql.reconnect();
    }
}

