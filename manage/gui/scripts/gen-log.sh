#!/bin/sh

git whatchanged "$@" --pretty=format:"%ai  %cn  <%ce>%n%n%s%n%b" |
	gawk '
		BEGIN {
			head  = "";
			files = "";
			logs  = "";
		}
		function ltrim(s)
		{
		  sub(/^[ \t]+/, "", s);
		  return s
		}
		
		{
			if(/^[0-9]+-[0-9]+-[0-9]+ [0-9]+:[0-9]+:[0-9]/)
			{
				if (head !~ /^$/)
				{
					printf("%s\n\n\\t* %s\n\n%s\n", head, files, logs);
					head="";
					file="";
					logs="";
				}
				head=$0;
			} 
			else if (/^:/)
			{
				files = files $6 ", ";
			}
			else if (!/^$/)
			{
				logs = logs "\t"  ltrim($0)  "\n";
			}
		}
		
		END {
		  if (head !~ /^$/)
		  {
		    printf("%s\n\n\t* %s\n\n%s\n", head, files, logs);
		  }
		}
		'
