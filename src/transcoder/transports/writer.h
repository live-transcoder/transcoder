#ifndef TRANSCODER_TRANSPORT_WRITER_H
#define TRANSCODER_TRANSPORT_WRITER_H

#include <boost/shared_ptr.hpp>
#include <boost/weak_ptr.hpp>

#include "av/container.h"

#include "pointed.hpp"

namespace transcoder {
namespace transport {

class Writer : public av::AbstractWriteFunctor
{
public:
    enum
    {
        OK = 0,
        FAIL = -1,             /// General Fail
        NEED_RESTART = -1000   /// Report to Muxer that it must be restarted
    } Codes;
};

DECLARE_PTR_TYPE(Writer);

} // namespace transport
} // namespace transcoder

#endif // TRANSCODER_TRANSPORT_WRITER_H
