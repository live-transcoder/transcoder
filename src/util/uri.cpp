#include <stdlib.h>

#include <string>
#include <iostream>
#include <sstream>

#include <boost/regex.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string.hpp>

#include "uri.h"

using namespace std;

Uri::Uri()
{
    init();
}

Uri::Uri(const string &str)
{
    init();
    setUri(str);
}

const string &Uri::getTransport() const
{
    return transport;
}

const string &Uri::getHost() const
{
    return host;
}

int Uri::getPort() const
{
    return port;
}

const string &Uri::getPath() const
{
    return path;
}

const UriRequest &Uri::getRequest() const
{
    return request;
}

const string &Uri::getUser() const
{
    return user;
}

const string &Uri::getPassword() const
{
    return password;
}

const string &Uri::getLabel() const
{
    return label;
}

string Uri::getUri() const
{
    ostringstream os;

    if (!transport.empty())
        os << transport << ':';

    if (!transport.empty() || !user.empty())
        os << "//";

    if (!user.empty())
    {
        os << user;

        if (!password.empty())
        {
            os << ':' << password;
        }

        os << '@';
    }

    os << host;

    if (port >= 0)
    {
        os << ':' << port;
    }

    os << path;

    if (!request.empty())
    {
        os << '?' << request.getRequest();
    }

    if (!label.empty())
    {
        os << '#' << label;
    }

    return os.str();
}

Uri &Uri::setTransport(const string &transport)
{
    this->transport = transport;
    return *this;
}

Uri &Uri::setHost(const string &host)
{
    this->host = host;
    return *this;
}

Uri &Uri::setPort(int port)
{
    this->port = port;
    return *this;
}

Uri &Uri::setPath(const string &path)
{
    this->path = path;
    return *this;
}

Uri &Uri::setRequest(const string &request)
{
    this->request = UriRequest(request);
    return *this;
}

Uri &Uri::setRequest(const UriRequest &request)
{
    this->request = request;
    return *this;
}

Uri &Uri::setUser(const string &user)
{
    this->user = user;
    return *this;
}

Uri &Uri::setPasword(const string &password)
{
    this->password = password;
    return *this;
}

Uri &Uri::setLabel(const string &label)
{
    this->label = label;
    return *this;
}

Uri &Uri::setUri(const string &uri)
{
    boost::regex parser("^(([^:/?#]+):)?(//((([^/?#:@]*)?(:([^/?#:@]*))?@)?([^/?#:@]*)(:([^/?#:@]*))?))?([^?#]*)(\\?([^#]*))?(#(.*))?",
                        boost::regex::perl|boost::regex::icase);

    boost::cmatch result;
    if (boost::regex_match(uri.c_str(), result, parser))
    {
        string request;

        transport.assign(result[2].first, result[2].second);
        user.assign(result[6].first, result[6].second);
        password.assign(result[8].first, result[8].second);
        host.assign(result[9].first, result[9].second);
        port = std::atoi(result[11].first);
        path.assign(result[12].first, result[12].second);
        request.assign(result[14].first, result[14].second);
        label.assign(result[16].first, result[16].second);

        if (!request.empty())
        {
            this->request = UriRequest(request);
        }
        else
        {
            this->request.clear();
        }

        if (port == 0)
        {
            port = -1;
        }
    }

    return *this;
}

void Uri::init()
{
    port = -1;
}


UriRequest::UriRequest()
{
}

UriRequest::UriRequest(const string &request)
{
    setRequest(request);
}

UriRequest::UriRequest(const UriRequest::RequestParams &params)
    : params(params)
{
}

bool UriRequest::empty() const
{
    return params.empty();
}

void UriRequest::clear()
{
    params.clear();
}

const UriRequest::RequestParams &UriRequest::getRequestParams() const
{
    return params;
}

string UriRequest::getRequest() const
{
    string result;
    RequestParams::const_iterator it;
    for (it = params.begin();
         it != params.end();
         ++it)
    {
        if (!result.empty())
            result.append("&"); // separator char, need to support to use semicolon also

        result.append(it->first);
        result.append("=");
        result.append(escape(it->second));
    }

    return result;
}

UriRequest &UriRequest::setRequestParams(const UriRequest::RequestParams &params)
{
    this->params = params;
    return *this;
}

UriRequest &UriRequest::setRequest(const string &request)
{
    // TODO
    RequestParams result;

    vector<string> requestSplit;
    boost::algorithm::split(requestSplit, request, boost::algorithm::is_any_of(";&"));

    vector<string>::iterator it;
    for (it = requestSplit.begin();
         it != requestSplit.end();
         ++it)
    {
        vector<string> pair;
        boost::algorithm::split(pair, *it, boost::algorithm::is_any_of("="));

        if (pair.size() >= 2)
        {
            result[pair[0]] = unescape(pair[1]);
        }
        else
        {
            result[pair[0]] = "";
        }
    }

    params.swap(result);

    return *this;
}

UriRequest &UriRequest::setParam(const string &key, const string &value)
{
    params[key] = value;
}


string UriRequest::escape(const string &stringToEscape) const
{
    // TODO: implement this functionality
    return stringToEscape;
}

string UriRequest::unescape(const string &valueToUnescape) const
{
    // TODO: implement this functionality
    return valueToUnescape;
}
