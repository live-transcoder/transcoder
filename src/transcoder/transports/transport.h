#ifndef TRANSCODER_TRANSPORT_TRANSPORT_H
#define TRANSCODER_TRANSPORT_TRANSPORT_H

#include <set>
#include <map>
#include <string>

#include <boost/noncopyable.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/weak_ptr.hpp>
#include <boost/thread/mutex.hpp>

#include "pointed.hpp"
#include "producer.h"
#include "muxer.h"
#include "writer.h"

namespace transcoder {
namespace transport {

class BaseTransport;
DECLARE_PTR_TYPE(BaseTransport);

class BaseTransport : private boost::noncopyable
{
public:
    typedef std::map<std::string, std::string>     Options;
    typedef std::pair<bool, MuxerPtr>              ResourceRegistrationStatus;

    static bool registerResource(const std::string& transport,
                                 const std::string& resource,
                                 const ProducerPtr &producer,
                                 const Options& options = Options());

    static bool unregisterResource(const std::string& transport,
                                   const string &resource);

    virtual ResourceRegistrationStatus registerResource(const std::string& resource,
                                                        const ProducerPtr &producer,
                                                        const Options& options = Options()) = 0;

    virtual bool unregisterResource(const std::string& resource) = 0;

    virtual const std::string getTransportName() = 0;

    virtual void init(const Options& options = Options());
    virtual void deinit();

protected:
    static bool registerTransport(BaseTransportPtr &transport);

protected:
    virtual ~BaseTransport();

private:
    static boost::mutex  transportsMutex;
    static std::set<BaseTransportPtr> transports;
};


template<typename T>
class Transport : public BaseTransport
{
public:
    static bool registerTransport()
    {
        static BaseTransportPtr instance;
        if (!instance)
        {
            instance = BaseTransportPtr(new T);
            return BaseTransport::registerTransport(instance);
        }

        return false;
    }

private:
};

} // namespace transport
} // namespace transcoder

#endif // TRANSCODER_TRANSPORT_TRANSPORT_H
