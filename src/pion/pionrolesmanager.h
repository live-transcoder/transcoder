#ifndef PIONROLESMANAGER_H
#define PIONROLESMANAGER_H

#include <string>
#include <map>
#include <set>

#include <boost/noncopyable.hpp>
#include <boost/smart_ptr.hpp>

namespace pion {
namespace net {

using namespace std;

class PionRolesManager : private boost::noncopyable
{
public:
    PionRolesManager() {}
    virtual ~PionRolesManager() {}

    virtual bool empty() { return userRoles.empty(); }

    virtual bool addUser(const string &role, const string &user);
    virtual bool removeUser(const string &role, const string &user);
    virtual bool clearUsers(const string &role);

    virtual bool removeRole(const string &role);
    virtual void clearAll();

    virtual bool        isUserInRole(const string &role, const string &user);
    virtual bool        isUserInRole(const set<string> &roles, const string &userRoles);
    virtual set<string> getUserRoles(const string &user);

private:
    map< string, set<string> > userRoles;

};

typedef boost::shared_ptr<PionRolesManager> PionRolesManagerPtr;

}} // ::pion::net

#endif // PIONROLESMANAGER_H
