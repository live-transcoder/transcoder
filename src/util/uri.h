#ifndef URI_H
#define URI_H

#include <iostream>
#include <map>

#include <boost/lexical_cast.hpp>


class UriRequest
{
public:
    typedef std::map<std::string, std::string> RequestParams;

    UriRequest();
    UriRequest(const std::string& request);
    UriRequest(const RequestParams& params);

    bool empty() const;
    void clear();

    const RequestParams& getRequestParams() const;
    std::string          getRequest() const;

    template <typename T>
    T getParam(const std::string& key) const
    {
        return boost::lexical_cast<T>(params.at(key));
    }

    UriRequest& setRequestParams(const RequestParams& params);
    UriRequest& setRequest(const std::string& request);
    UriRequest& setParam(const std::string &key, const std::string &value);

    template <typename T>
    UriRequest& setParam(const std::string& key, const T& value)
    {
        std::string valueStr = boost::lexical_cast<std::string>(value);
        setParam(key, valueStr);
        return *this;
    }

private:
    void init();
    std::string escape(const std::string& stringToEscape) const;
    std::string unescape(const std::string& valueToUnescape) const;

private:
    RequestParams params;
    bool          semicolonUse; // TODO
};



class Uri
{
public:
    Uri();
    Uri(const std::string &str);

    const std::string& getTransport() const;
    const std::string& getHost()      const;
    int                getPort()      const;
    const std::string& getPath()      const;
    const UriRequest&  getRequest()   const;
    const std::string& getUser()      const;
    const std::string& getPassword()  const;
    const std::string& getLabel()     const;
    std::string        getUri()       const;

    Uri& setTransport(const std::string& transport);
    Uri& setHost(const std::string& host);
    Uri& setPort(int port = -1);
    Uri& setPath(const std::string& path);
    Uri& setRequest(const std::string& request);
    Uri& setRequest(const UriRequest& request);
    Uri& setUser(const std::string& user);
    Uri& setPasword(const std::string& password);
    Uri& setLabel(const std::string& label);
    Uri& setUri(const std::string& uri);

private:
    void init();

private:
    std::string transport;
    std::string host;
    int         port;
    std::string path;
    UriRequest  request;
    std::string user;
    std::string password;
    std::string label;
};

#endif // URI_H
