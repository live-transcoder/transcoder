#-------------------------------------------------
#
# Project created by QtCreator 2012-06-20T14:43:38
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = transcoder-manager
TEMPLATE = app

CONFIG += link_pkgconfig
PKGCONFIG += QJson

SOURCES += main.cpp\
        mainwindow.cpp \
    formatdata.cpp \
    addeditdialog.cpp \
    previewdialog.cpp \
    addserverdialog.cpp \
    transcodersmodel.cpp

HEADERS  += mainwindow.h \
    formatdata.h \
    addeditdialog.h \
    previewdialog.h \
    addserverdialog.h \
    transcodersmodel.h

FORMS    += mainwindow.ui \
    addeditdialog.ui \
    previewdialog.ui \
    addserverdialog.ui

RESOURCES += \
    main.qrc
