#include <soci/soci.h>

#include "dbconnectionpool.h"
#include "util/mutex.hpp"

using namespace soci;

struct DbConnectionPool::connection_pool_impl
{
    bool find_free(std::size_t & pos)
    {
        for (std::size_t i = 0; i != sessions_.size(); ++i)
        {
            if (sessions_[i].first)
            {
                pos = i;
                return true;
            }
        }

        return false;
    }

    // by convention, first == true means the entry is free (not used)
    std::vector<std::pair<bool, session *> > sessions_;
    Mutex             mtx_;
    ConditionVariable cond_;
};




DbConnectionPool::DbConnectionPool(std::size_t size)
    : connection_pool(size),
      initialSize(size),
      maxSize(size),
      currentSize(size),
      increaseStep(0)
{
}

DbConnectionPool::DbConnectionPool(std::size_t initialSize, std::size_t maxSize, std::size_t increaseStep)
    : connection_pool(1),
      initialSize(initialSize),
      maxSize(maxSize),
      currentSize(initialSize),
      increaseStep(increaseStep)
{
    if (initialSize == 0)
    {
        throw soci_error("Invalid pool size");
    }

    if (increaseStep == 0)
    {
        throw soci_error("Increase step for connection pool is zero");
    }

    if (initialSize > maxSize)
    {
        maxSize = initialSize;
    }

    pimpl_ = new connection_pool_impl();
    pimpl_->sessions_.resize(initialSize);
    for (std::size_t i = 0; i != initialSize; ++i)
    {
        pimpl_->sessions_[i] = std::make_pair(true, new session());
    }
}

DbConnectionPool::~DbConnectionPool()
{
    for (std::size_t i = 0; i != pimpl_->sessions_.size(); ++i)
    {
        delete pimpl_->sessions_[i].second;
    }

    delete pimpl_;
}

session &DbConnectionPool::at(std::size_t pos)
{
    if (initialSize == maxSize && increaseStep == 0)
    {
        return connection_pool::at(pos);
    }
    else
    {
        if (pos >= pimpl_->sessions_.size())
        {
            if (pos < maxSize)
            {
                // Increase pool
            }
            else
            {
                throw soci_error("Invalid pool position");
            }
        }
    }
}

std::size_t DbConnectionPool::lease()
{
}

bool DbConnectionPool::try_lease(std::size_t &pos, int timeout)
{
}

void DbConnectionPool::give_back(std::size_t pos)
{
}



