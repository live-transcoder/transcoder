#ifndef _LOG_OSTREAM_H
#define _LOG_OSTREAM_H 1

#include <iostream>
#include <sstream>
#include <cstdio>

#include <boost/smart_ptr.hpp>

namespace logstream {

#ifndef LOG_BUFFER_SIZE
#define LOG_BUFFER_SIZE 4096
#endif

namespace level {
enum level {
    silent,
    fatal,
    error,
    warning,
    info,
    extra,
    debug,
    trace
};
} // ::level


namespace backend {

/**
 * Abstract logging backend
 */
class abstract_backend
{
public:
    virtual ~abstract_backend() {}
    virtual void push_message(level::level priority, level::level threshold, const std::string& message) = 0;
};

typedef boost::shared_ptr<abstract_backend> backend_ptr;

/**
 * Logging backend that push messages to STDERR/STDOUT, using by default.
 */
class console_backend : public abstract_backend
{
public:
    virtual void push_message(level::level priority, level::level threshold, const std::string &message);
};


/**
 * Logging backend that store messages to string
 */
class string_backend : public abstract_backend
{
public:
    string_backend();

    virtual void push_message(level::level priority, level::level threshold, const std::string &message);

    std::string str();
    void        clear();
    void        set_threshold_ignoring(bool do_ignore);

private:
    std::stringstream ss;
    bool              ignore_threshold;
};

} // ::backend


/// Forward declaration for logger helper class
class logger_helper;

/**
 * Base class for basic_logbuf and basic_stream that provide public API to logger helper class.
 */
class logger_meta
{
public:

    /**
     * Global settings
     */
    //@{
    void set_threshold(level::level threshold);
    level::level get_threshold() const;

    void set_logging_backend(const backend::backend_ptr& backend);
    backend::backend_ptr get_logging_backend() const;

    void set_global_logger_using(bool use);
    bool is_global_logger_used() const;
    //@}

    /**
     * Thread-specific settings
     *
     */
    //@{
    void set_priority(level::level priority);
    level::level get_priority() const;

    void set_thread_logging_backend(const backend::backend_ptr& backend);
    backend::backend_ptr get_thread_logging_backend();

    void set_thread_logger_using(bool use);
    bool is_thread_logger_used() const;
    //@}

protected:
    logger_meta(logger_helper *helper);
    void push_message(level::level priority, const std::string &message);

private:
    logger_meta();

private:
    logger_helper *helper;
};


/**
 * Stream buffer implementation for logging
 */
template<typename C, typename T = std::char_traits<C> >
class basic_logbuf : public std::basic_streambuf<C, T>, public logger_meta
{
    typedef typename T::int_type int_type;
    typedef typename T::pos_type pos_type;

public:
    inline basic_logbuf(logger_helper *helper)
        : logger_meta(helper)
    {
        buffer = new C[LOG_BUFFER_SIZE];
        this->setp(buffer, buffer + ((LOG_BUFFER_SIZE) - 1));
    }

    inline ~basic_logbuf()
    {
        delete [] buffer;
    }

protected:
    virtual int_type overflow(int_type c)
    {
        if ( c != EOF )
        {
            *(this->pptr()) = c;
            this->pbump(1) ;
        }

        sync();

        return c;
    }

    virtual int_type sync()
    {
        const std::streamsize count = this->pptr() - this->pbase();
        if (count > 0)
        {
            std::basic_string<C,T> msg (this->pbase(), this->pptr ());
            if (is_global_logger_used())
            {
                push_message(get_priority(), msg);
            }

            if (is_thread_logger_used() && get_thread_logging_backend())
            {
                get_thread_logging_backend()->push_message(get_priority(), get_threshold(), msg);
            }
        }

        this->pbump(-count);
        return count;
    }

private:
    basic_logbuf (const basic_logbuf&);
    basic_logbuf& operator=(const basic_logbuf&);

private:
    C *buffer;
};


/**
 * Output stream for logging implementation
 */
template<typename C, typename T = std::char_traits<C> >
class basic_logstream : public std::basic_ostream<C, T>, public logger_meta
{
public:
    basic_logstream(logger_helper *helper)
        : std::basic_ios<C, T>(&buffer),
          std::basic_ostream<C, T>(&buffer),
          logger_meta(helper),
          buffer(helper)
    {
        this->init(&buffer);
    }

private:
    basic_logbuf<C, T> buffer;
};


typedef basic_logstream<char> logstream;

/// Get stream logger, thread-safe
logstream& logger(level::level priority);

/// Get stream logger, without changing previous log priority
logstream& logger();

/// Pretty logger wrapper
#define LOG(pri) logger(pri) << __PRETTY_FUNCTION__ << ":" << __LINE__ << ":"
}

#endif //!_LOG_OSTREAM_H
