/** =============================================================================================
  
    This file is a part of "%ProjectName%" project
    http://htrd.su
    
    @date   2012-4-7
    @brief
    
    Copyright (C) 2002-2012 by Alexander 'hatred' Drozdov <adrozdoff@gmail.com>
    
    This program is free software; you can redistribute it and/or modify
    it under the terms of the version 2 of GNU General Public License as
    published by the Free Software Foundation.
    
    For more information see LICENSE and LICENSE.ru files
    
    Based on:
    http://www.justsoftwaresolutions.co.uk/threading/implementing-a-thread-safe-queue-using-condition-variables.html

   ============================================================================================== */

#ifndef BLOCKEDRRQUEUE_H
#define BLOCKEDRRQUEUE_H

#include <queue>

#include <boost/thread.hpp>

template< typename T >
class ConcurentQueue
{
public:
    explicit ConcurentQueue(int size = -1)
        : queueSize(size),
          triStateDataFlag(-1)
    {
    }


    void setMaxQueueSize(int size)
    {
        boost::mutex::scoped_lock lock(mutex);
        queueSize = size;
    }


    void waitForData() const
    {
        boost::mutex::scoped_lock lock(mutex);
        while (triStateDataFlag < 0)
        {
            dataAvailCond.wait(lock);
        }
    }


    void waitForSpace() const
    {
        boost::mutex::scoped_lock lock(mutex);
        while (triStateDataFlag > 0)
        {
            spaceAvailCond.wait(lock);
        }
    }


    bool waitAndPop(T& value)
    {
        boost::mutex::scoped_lock lock(mutex);
        while (triStateDataFlag < 0)
        {
            dataAvailCond.wait(lock);
        }

        if (!data.empty())
        {
            internalPop(value, lock);
            return true;
        }

        return false;
    }


    bool waitAndPush(const T& value)
    {
        boost::mutex::scoped_lock lock(mutex);
        while (triStateDataFlag > 0)
        {
            spaceAvailCond.wait(lock);
        }

        if (data.size() < queueSize)
        {
            internalPush(value, lock);
            return true;
        }

        return false;
    }


    void cancelWait()
    {
        boost::mutex::scoped_lock lock(mutex);
        triStateDataFlag = 0;
        dataAvailCond.notify_one();
        spaceAvailCond.notify_one();
    }


    int getMaxQueueSize() const
    {
        boost::mutex::scoped_lock lock(mutex);
        return queueSize;
    }


    bool empty() const
    {
        boost::mutex::scoped_lock lock(mutex);
        return data.empty();
    }


    bool pop(T& value)
    {
        boost::mutex::scoped_lock lock(mutex);
        if (data.empty())
        {
            return false;
        }

        internalPop(value, lock);

        return true;
    }


    bool push(const T& x)
    {
        boost::mutex::scoped_lock lock(mutex);

        if (queueSize > 0 && data.size() >= queueSize)
        {
            return false;
        }

        internalPush(x, lock);

        return true;
    }


    int size() const
    {
        boost::mutex::scoped_lock lock(mutex);
        return data.size();
    }


    void clear()
    {
        boost::mutex::scoped_lock lock(mutex);

        std::queue<T> empty;
        std::swap(data, empty);
        updateDataState();
    }


private:
    void internalPush(const T& value, boost::mutex::scoped_lock& lock)
    {
        const bool wasEmpty = data.empty();

        data.push(value);

        updateDataState();

        lock.unlock();
        if (wasEmpty)
        {
            dataAvailCond.notify_one();
        }
    }


    void internalPop(T& value, boost::mutex::scoped_lock& lock)
    {
        const bool wasFull = (data.size() >= queueSize);

        value = data.front();
        data.pop();

        updateDataState();

        lock.unlock();
        if (wasFull)
        {
            spaceAvailCond.notify_one();
        }
    }


    void updateDataState()
    {
        if (data.empty())
        {
            triStateDataFlag = -1;
        }
        else if (queueSize > 0 && data.size() >= queueSize)
        {
            triStateDataFlag = 1;
        }
        else
        {
            triStateDataFlag = 0;
        }
    }

private:
    mutable boost::mutex              mutex;
    mutable boost::condition_variable dataAvailCond;
    mutable boost::condition_variable spaceAvailCond;
    char                      triStateDataFlag; // 0 - ok; <0 - empty; >0 - full

    int                       queueSize;
    std::queue<T>             data;
};

#endif // BLOCKEDRRQUEUE_H
