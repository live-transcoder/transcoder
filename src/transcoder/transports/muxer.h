#ifndef TRANSCODER_TRANSPORT_MUXER_H
#define TRANSCODER_TRANSPORT_MUXER_H

#include <map>

#include <boost/shared_ptr.hpp>
#include <boost/weak_ptr.hpp>

#include "av/packet.h"
#include "av/avutils.h"

#include "boost/atomic.hpp"

#include "../baseencodecontext.h"

#include "consumer.h"
#include "concurentqueue.hpp"
#include "thread.hpp"
#include "pointed.hpp"

#include "writer.h"

namespace transcoder {
namespace transport {

using namespace av;

class Muxer :
        public transcoder::encode::BaseEncodeContext,
        public Thread<Muxer>,
        public Consumer
{
public:
    typedef boost::function<int (const PacketPtr&, const WriterPtr&)> PacketWriteHandler;

    Muxer();
    Muxer(const WriterPtr& writer);
    Muxer(const FormatDescription &format, const WriterPtr& writer);
    virtual ~Muxer();

    virtual void notify(const Producer *producer);
    virtual void run();

    void setBeforePacketWriteHandler(const PacketWriteHandler& handler);
    void setAfterPacketWriteHandler(const PacketWriteHandler& handler);

    void setTimeSynchronizeWrite(bool flag);

    void setQueueSize(uint32_t queueSize);
    uint32_t getQueueSize() const;

    // override Thread start
    void start();

    void stop();

protected:
    virtual int writePacket(const PacketPtr &packet);

private:
    void commonCtor();
    void waitReady();

protected:
    bool isHeaderWriten;

private:
    boost::atomic_bool doWork;

    WriterPtr writer;
    ConcurentQueue<PacketPtr> packetQueue;

    std::map<int, av::PacketPtsRecalculator> ptsRecalc;

    bool    isFirstRecvPacket;
    bool    doTimeSynchronizeWrite;

    PacketWriteHandler beforePacketWrite;
    PacketWriteHandler afterPacketWrite;

    Condition          readyCond;

    // Temporary
    time_t prevTime;
};

DECLARE_PTR_TYPE(Muxer);

} // namespace transport
} // namespace transcoder

#endif // TRANSCODER_TRANSPORT_MUXER_H
