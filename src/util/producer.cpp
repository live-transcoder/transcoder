#include <iostream>

#include "producer.h"
#include "consumer.h"

#include "boost/atomic.hpp"
#include "log/logstream.h"

using namespace std;
using namespace logstream;


struct ResponseHandler
{
    ResponseHandler(const ResponseHandlerProc& proc)
        : proc(proc)
    {}

    void operator()()
    {
        if (!proc.empty())
            proc();
    }

    ResponseHandlerProc proc;
};


class NotifyThread
{
public:
    NotifyThread(Producer *producer, const boost::weak_ptr<Consumer> &consumer)
        : producer(producer),
          consumer(consumer),
          isActive(false)
    {
    }


    ~NotifyThread()
    {
        boost::thread::id id = thread.get_id();
        logger(level::extra) << "Begin to destroy NotifyThread: " << id << endl;
        stop();
        join();
        logger(level::extra) << "Finish to destroy NotifyThread: " << id << endl;
    }


    void start()
    {
        boost::lock_guard<boost::mutex> lock(mutex);
        if (!isActive)
        {
            isActive = true;
            thread = boost::thread(boost::bind(&NotifyThread::run, this));
            logger(level::extra) << "Start NotifyThread: " << thread.get_id() << endl;
        }
    }


    void stop()
    {
        boost::lock_guard<boost::mutex> guard(mutex);
        if (isActive)
        {
            isActive = false;
            thread.interrupt();
            newDataCond.wakeUpOne();
        }
    }


    void join()
    {
        thread.join();
    }


    bool isRun()
    {
        return isActive;
    }


    const boost::weak_ptr<Consumer> &getConsumer() const
    {
        return this->consumer;
    }


    // Only notify about new data
    void notify(const ResponseHandlerWPtr& responseHandler)
    {
        boost::lock_guard<boost::mutex> lock(mutex);
        this->responseHandler = responseHandler;
        newDataCond.wakeUpOne();
    }


protected:
    void run()
    {
        while (isActive)
        {
            newDataCond.wait();

            ResponseHandlerWPtr responseHandler;

            // make local copy
            {
                boost::lock_guard<boost::mutex> guard(mutex);
                responseHandler = this->responseHandler;
            }

            if (isActive)
            {
                boost::shared_ptr<Consumer> ptr(consumer.lock());
                if (ptr)
                {
                    ptr->notify(producer);
                }
                else
                {
                    isActive = false;
                }

                ResponseHandlerPtr handler = responseHandler.lock();
                if (handler)
                {
                    (*handler)();
                }
            }
        }
    }

private:
    Producer                  *producer;
    boost::weak_ptr<Consumer>  consumer;

    boost::atomic_bool        isActive;
    boost::thread             thread;

    boost::mutex              mutex;
    Condition                 newDataCond;
    ResponseHandlerWPtr       responseHandler;
};




struct EqPredicate
{
    const boost::weak_ptr<Consumer>& theItem;

    EqPredicate(const boost::weak_ptr<Consumer>& item)
        : theItem(item)
    {
    }


    bool operator () (const boost::weak_ptr<Consumer>& p) const
    {
        return p.lock() == theItem.lock();
    }


    bool operator () (const boost::shared_ptr<NotifyThread>& t) const
    {
        return t->getConsumer().lock() == theItem.lock();
    }

};


struct ExpiredPredicate
{
    bool operator () (const boost::weak_ptr<Consumer>& p) const
    {
        return p.expired();
    }

    bool operator () (const boost::shared_ptr<NotifyThread>& t) const
    {
        return t->getConsumer().expired();
    }
};


struct EqualOrExpiredPredicate
{
    EqPredicate      eqPred;
    ExpiredPredicate expPred;

    EqualOrExpiredPredicate(const boost::weak_ptr<Consumer>& item)
        : eqPred(item)
    {}

    template <typename U>
    bool operator() (const U& p) const
    {
        if (eqPred.operator ()(p))
            return true;
        else if (expPred.operator ()(p))
            return true;
        else
            return false;
    }
};



Producer::Producer()
    : cachedNotifyListSize(0)
{
}

Producer::~Producer()
{
    notifyList.clear();
}

void Producer::subscribe(const boost::weak_ptr<Consumer> &consumer)
{
    boost::lock_guard<boost::mutex> lock(consumersMutex);

    boost::shared_ptr<NotifyThread> n(new NotifyThread(this, consumer));
    n->start();

    notifyList.push_back(n);

    cachedNotifyListSize = notifyList.size();
}

void Producer::unsubscribe(const boost::weak_ptr<Consumer> &consumer)
{
    boost::lock_guard<boost::mutex> lock(consumersMutex);
    notifyList.remove_if(EqualOrExpiredPredicate(consumer));

    cachedNotifyListSize = notifyList.size();
}

uint Producer::subscribersCount() const
{
    boost::lock_guard<boost::mutex> guard(consumersMutex);
    return cachedNotifyListSize;
}

void Producer::notifyAll(void *data)
{
    std::size_t size = 0;
    NotifyList tmp;

    boost::lock_guard<boost::mutex> lock(consumersMutex);

    NotifyList::iterator it = notifyList.begin();
    while (it != notifyList.end())
    {
        if ((*it)->isRun())
        {
            size++;
            tmp.push_back(*it);
        }
        else
        {
            notifyList.erase(it++);
            continue;
        }

        ++it;
    }

    if (size != cachedNotifyListSize)
    {
        cachedNotifyListSize = size;
    }

    responseReset(size);
    for (it = tmp.begin(); it != tmp.end(); ++it)
    {
        // Notify with new response handler
        (*it)->notify(ResponseHandlerWPtr(responseHandlerPrt));
    }
}


#define TIME_DIFF_MSEC(a, b) \
    ((a.sec - b.sec) * 1000 + (a.nsec - b.nsec) / 1000 / 1000)

void Producer::waitSubscribers(int64_t waitTimeMsec)
{
    if (waitTimeMsec == 0) // do not wait
    {
        return;
    }

    boost::shared_lock<boost::shared_mutex> lock(responseCompleteMutex);
    boost::xtime time;
    boost::xtime_get(&time, 1); // TODO must be replaced with boost::chrono, but currently target
                                // does not have modern BOOST library
    while (!responseCompleteFlag)
    {
         responseCompleteCond.timed_wait(lock, boost::posix_time::milliseconds(1000));

        boost::xtime currentTime;
        boost::xtime_get(&currentTime, 1);
        if (waitTimeMsec > 0 && TIME_DIFF_MSEC(currentTime, time) >= waitTimeMsec)
        {
            break;
        }
    }
}


void Producer::responseHandler()
{
    boost::lock_guard<boost::shared_mutex> guard(responseCompleteMutex);
    if (++responseCount == responseNum)
    {
        responseCompleteFlag = true;
        responseCompleteCond.notify_one();
    }
}


void Producer::responseReset(std::size_t cout)
{
    {
        boost::lock_guard<boost::shared_mutex> guard(responseCompleteMutex);
        responseCompleteFlag = false;
        responseNum          = cout;
        responseCount        = 0;
    }

    // Invalidate previous pointer and recreate it.
    // Now, previous notify calls will not be report.
    // It valid, if we don't want wait for it finish, like wait timeout expired.
    responseHandlerPrt = ResponseHandlerPtr(new ResponseHandler(boost::bind(&Producer::responseHandler, this)));
}
