#include <sys/stat.h>
#include <fcntl.h>

#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/filesystem.hpp>

#include "encodethread.h"
#include "applicationcontext.h"
#include "log/logstream.h"
#include "util/uri.h"
#include "util/utils.h"

#include "file.h"

using namespace logstream;

namespace transcoder {
namespace transport {


struct FileWriter : public Writer
{
    FileWriter(const std::string& resource)
        : resource(resource),
          fd(-1),
          index(0),
          chunkSize(0),
          chunkDuration(0)
    {
        logger(level::debug) << "Begin file writing: " << resource << endl;
        uri.setUri(resource);

        if (uri.getRequest().getRequestParams().count("file_size"))
        {
            chunkSize = uri.getRequest().getParam<uint64_t>("file_size");
        }

        if (uri.getRequest().getRequestParams().count("file_duration"))
        {
            chunkDuration = uri.getRequest().getParam<uint64_t>("file_duration");
        }

        this->open();
    }

    ~FileWriter()
    {
        logger(level::debug) << "End file writing: " << resource << endl;
        this->close();
    }

    int open()
    {
        logger(level::debug) << "Open file: " << resource << std::endl;
        if (resource.empty())
        {
            return -1;
        }

        // create new file name
        currentName = uri.getPath();
        map<string, string> replaceMap;
        fillReplaceMap(replaceMap);
        placeHoldersReplace(currentName, replaceMap);

        // make file path
        try
        {
            boost::filesystem::path filePath(currentName);
            boost::filesystem::path fileDir = boost::filesystem::absolute(filePath).parent_path();
            if (!boost::filesystem::exists(fileDir))
            {
                if (!boost::filesystem::create_directories(fileDir))
                {
                    fd = -1;
                    return -1;
                }
            }
        }
        catch (...)
        {
            fd = -1;
            return -1;
        }

        openTime = boost::posix_time::second_clock::local_time();

        fd = ::open(currentName.c_str(), O_RDWR | O_CREAT, 0664);
        logger(level::debug) << "File opened: " << resource << ", fd = " << fd << ", error: " << strerror(errno) <<  std::endl;
        return fd;
    }

    int close()
    {
        return ::close(fd);
    }

    int reopen()
    {
        close();
        return open();
    }

    void fillReplaceMap(map<string, string>& replaceMap)
    {
        replaceMap["%%"] = "%";
        replaceMap["%i"] = boost::lexical_cast<string>(index++);

        // Date based
        boost::gregorian::date currentDate = boost::gregorian::day_clock::local_day();
        replaceMap["%y"] = boost::lexical_cast<string>(currentDate.year());
        replaceMap["%m"] = boost::lexical_cast<string>(static_cast<int>(currentDate.month()));
        if (currentDate.month() < 10)
        {
            replaceMap["%m"] = "0" + replaceMap["%m"];
        }

        replaceMap["%d"] = boost::lexical_cast<string>(currentDate.day());
        if (currentDate.day() < 10)
        {
            replaceMap["%d"] = "0" + replaceMap["%d"];
        }

        // Time based
        boost::posix_time::ptime currentTime = boost::posix_time::second_clock::local_time();
        boost::posix_time::time_duration td = currentTime.time_of_day();
        replaceMap["%H"] = boost::lexical_cast<string>(td.hours());
        if (td.hours() < 10)
        {
            replaceMap["%H"] = "0" + replaceMap["%H"];
        }

        replaceMap["%M"] = boost::lexical_cast<string>(td.minutes());
        if (td.minutes() < 10)
        {
            replaceMap["%M"] = "0" + replaceMap["%M"];
        }

        replaceMap["%S"] = boost::lexical_cast<string>(td.seconds());
        if (td.seconds() < 10)
        {
            replaceMap["%S"] = "0" + replaceMap["%S"];
        }
    }



    int operator ()(uint8_t *buf, int size)
    {
        bool doReopen = false;

        boost::posix_time::time_duration td
                = boost::posix_time::second_clock::local_time() - openTime;
        if (chunkDuration > 0 && td.total_seconds() >= chunkDuration)
        {
            doReopen = true;
        }

        uint64_t currentSize = boost::filesystem::file_size(boost::filesystem::path(currentName));
        if (chunkSize > 0 && currentSize >= chunkSize)
        {
            doReopen = true;
        }

        if (fd == -1 || doReopen)
        {
            reopen();
        }

        return ::write(fd, buf, size);
    }

    const char *name() const
    {
        return resource.c_str();
    }


    string resource;
    int fd;

    Uri      uri;
    uint64_t index;
    string   currentName;

    uint64_t chunkSize;
    uint64_t chunkDuration;

    boost::posix_time::ptime openTime;
};

DECLARE_PTR_TYPE(FileWriter);

BaseTransport::ResourceRegistrationStatus
File::registerResource(const string &resource,
                       const ProducerPtr &producer,
                       const BaseTransport::Options &options)
{
    boost::mutex::scoped_lock lock(muxersMutex);

    FileWriterPtr writer(new FileWriter(resource));
    MuxerPtr      muxer(new Muxer(writer));

    // Set send buffer size
    muxer->setQueueSize(ApplicationContext::instance->sendBufferSize);

    muxers[resource] = muxer;

    return std::make_pair(true, muxer);
}

bool File::unregisterResource(const string &resource)
{
    boost::mutex::scoped_lock lock(muxersMutex);
    if (muxers.find(resource) != muxers.end())
    {
        muxers.erase(resource);
    }
}

const string File::getTransportName()
{
    return "file";
}

} // namespace transport
} // namespace transcoder
