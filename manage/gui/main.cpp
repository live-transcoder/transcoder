#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QCoreApplication::setOrganizationName("HatredsLogPlace");
    QCoreApplication::setApplicationName("transcoder-manager");

    MainWindow w;
    w.show();
    
    return a.exec();
}
