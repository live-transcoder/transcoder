#ifndef TRANSCODERDAO_H
#define TRANSCODERDAO_H

#include <string>
#include <list>
#include <set>

#include <boost/property_tree/ptree.hpp>

// Data base lib
#include <soci/soci.h>

#include "transcoder.h"

namespace transcoder {

using namespace std;


bool ptreeToFormatDescription(const boost::property_tree::ptree &node, FormatDescription &desc);
bool formatDescriptionToPtree(const FormatDescription &desc, boost::property_tree::ptree &dest);



class TranscoderDAO
{
public:
    explicit TranscoderDAO(soci::connection_pool *pool);
    virtual ~TranscoderDAO();

    bool isTranscoderExists(const string &name);

    bool putTranscoder(const TranscoderPtr &trancoder);
    TranscoderPtr getTranscoder(const string& name);

    list<TranscoderPtr> getTranscoders(const string &transcoderName = string());
    bool putTranscoders(const list<TranscoderPtr> transcoders);

    bool delTranscoder(const string &name);
    bool delTranscoders(const set<string> &names);

private:
    soci::connection_pool *dbpool;
};


} // ::transocoder

#endif // TRANSCODERDAO_H
