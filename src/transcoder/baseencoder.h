#ifndef BASEENCODER_H
#define BASEENCODER_H

#include <list>
#include <vector>
#include <map>

#include <boost/function.hpp>
#include <boost/bind.hpp>

#include "av/videoframe.h"
#include "av/audiosamples.h"
#include "av/avutils.h"
#include "av/filtergraph.h"

#include "baseencodecontext.h"
#include "formatdescription.h"

namespace transcoder {
namespace encode {

typedef boost::function<void (const av::PacketPtr &packet)> EncodedPacketHandler;

class FrameEncoder : public BaseEncodeContext
{
public:

    FrameEncoder();
    explicit FrameEncoder(const FormatDescription &description);
    virtual ~FrameEncoder() {}

    virtual bool isCodersOpened();
    virtual bool openCoders();

    virtual bool canEncode(const av::FramePtr& inFrame);

    int getNewFrameStreamIndex(const av::FramePtr& inFrame);

    virtual std::list<av::PacketPtr> encode(const av::FramePtr& inFrame, const EncodedPacketHandler &onPacketHandler = EncodedPacketHandler());

protected:
    /**
     * Encode video using filters (can produce more packets for one frame)
     *
     * @param inFrame
     * @param onPacketHandler
     * @return
     */
    virtual std::list<av::PacketPtr> encodeVideo(const av::VideoFramePtr &inFrame, const EncodedPacketHandler &onPacketHandler = EncodedPacketHandler());

    /**
     * Encode audio using filters (can produce more packets for one frame)
     *
     * @param inFrame
     * @param onPacketHandler
     * @return
     */
    virtual std::list<av::PacketPtr> encodeAudio(const av::AudioSamplesPtr &inFrame, const EncodedPacketHandler &onPacketHandler = EncodedPacketHandler());

private:
    //av::FramePtsRecalculator ptsRecalc;

    std::map<int, av::FramePtsRecalculator>   ptsRecalculators;
    std::map<int, av::VideoResamplerPtr>      videoResamplers;
    std::map<int, av::AudioResamplerPtr>      audioResamplers;
    std::map<int, av::FilterGraphPtr>         audioFilters;
};


}} // ::transcoder::encode

#endif // BASEENCODER_H
