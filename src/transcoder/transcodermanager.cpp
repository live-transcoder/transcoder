#include <boost/thread.hpp>

#include "transcodermanager.h"
#include "transcoderdao.h"
#include "applicationcontext.h"

#include "log/logstream.h"

using namespace logstream;

namespace transcoder {

struct TranscoderExists
{
    TranscoderExists(const string &n)
        : name(n)
    {}

    bool operator() (const TranscoderPtr &t)
    {
        if (name == t->getName())
        {
            return true;
        }

        return false;
    }

private:
    string name;
};


TranscoderManager::TranscoderManager(boost::restricted)
{
}


bool TranscoderManager::load()
{
    transcoders.clear();
    TranscoderDAO dao(ApplicationContext::instance->pool);
    transcoders = dao.getTranscoders();

    if (transcoders.empty())
    {
        logger(level::warning) << "Empty transcoders list loaded" << endl;
    }

    return !transcoders.empty();
}

bool TranscoderManager::load(const string &name)
{
    logger(level::error) << "Method '" << __PRETTY_FUNCTION__ << "' does not implemented yet" << endl;
    return false;
}

bool TranscoderManager::save()
{
    TranscoderDAO dao(ApplicationContext::instance->pool);

    bool result = true;
    if (!dao.delTranscoders(deletedTranscoders))
    {
        logger(level::warning) << "Can't remote transcoders marked as 'deleted' from database" << endl;
        result = false;
    }

    if (!dao.putTranscoders(transcoders))
    {
        logger(level::warning) << "Can't save changed transcoders to database" << endl;
        result = false;
    }

    return result;
}

bool TranscoderManager::save(const string &name)
{
    TranscoderPtr transcoder = getTranscoder(name);
    TranscoderDAO dao(ApplicationContext::instance->pool);
    if (!transcoder)
    {
        return false;
    }

    if (!dao.putTranscoder(transcoder))
    {
        logger(level::warning) << "Can't save transcoder with name '" << name << "' to database" << endl;
        return false;
    }

    return true;
}

void TranscoderManager::startAll()
{
    Transcoders::iterator it;
    for (it = transcoders.begin(); it != transcoders.end(); ++it)
    {
        if ((*it)->isEnabled())
            (*it)->start();
    }
}

void TranscoderManager::stopAll()
{
    boost::thread_group threads;

    Transcoders::iterator it;
    for (it = transcoders.begin(); it != transcoders.end(); ++it)
    {
        threads.create_thread(boost::bind(&Transcoder::stop, *it));
    }

    threads.join_all();
}

void TranscoderManager::restartAll()
{
    stopAll();
    startAll();
}

bool TranscoderManager::addTranscoder(const TranscoderPtr &transcoder)
{
    if (!transcoder)
    {
        logger(level::warning) << "Nullable transcoder" << endl;
        return false;
    }

    Transcoders::iterator it = findTranscoder(transcoder->getName());
    if (it != transcoders.end())
    {
        logger(level::warning) << "Transcoder with name '" << transcoder->getName() << "' already exists" << endl;
        return false;
    }

    transcoders.push_back(transcoder);
    if (deletedTranscoders.find(transcoder->getName()) != deletedTranscoders.end())
    {
        deletedTranscoders.erase(transcoder->getName());
    }

    return true;
}

bool TranscoderManager::removeTranscoder(const string &name)
{
    Transcoders::iterator it = findTranscoder(name);
    if (it == transcoders.end())
    {
        return false;
    }

    deletedTranscoders.insert(name);
    transcoders.erase(it);
    return true;
}

TranscoderPtr TranscoderManager::getTranscoder(const string &name)
{
    Transcoders::iterator it = findTranscoder(name);
    if (it == transcoders.end())
    {
        return TranscoderPtr();
    }

    return (*it);
}

const TranscoderManager::Transcoders &TranscoderManager::getTranscoders() const
{
    return transcoders;
}

TranscoderManager::Transcoders::iterator TranscoderManager::findTranscoder(const string &name)
{
    Transcoders::iterator it;
    it = std::find_if(transcoders.begin(),
                      transcoders.end(),
                      TranscoderExists(name));

    if (it == transcoders.end())
    {
        logger(level::warning) << "Transcoder with name '" << name << "' does not present" << endl;
    }

    return it;
}

} // ::transcoder
