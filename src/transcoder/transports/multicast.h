#ifndef TRANSCODER_TRANSPORT_MULTICAST_H
#define TRANSCODER_TRANSPORT_MULTICAST_H

#include <boost/thread/mutex.hpp>

#include "transport.h"
#include "muxer.h"

namespace transcoder {
namespace transport {

class Multicast : public Transport<Multicast>
{
public:
    virtual BaseTransport::ResourceRegistrationStatus
    registerResource(const string &resource,
                     const ProducerPtr &producer,
                     const Options &options);

    virtual bool unregisterResource(const string &resource);

    virtual const std::string getTransportName();

private:
    boost::mutex                    muxersMutex;
    std::map<std::string, MuxerPtr> muxers;
};

} // namespace transport
} // namespace transcoder

#endif // TRANSCODER_TRANSPORT_MULTICAST_H
