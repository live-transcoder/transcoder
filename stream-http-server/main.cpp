/** =============================================================================================
  
    This file is a part of "%ProjectName%" project
    http://htrd.su
    
    @date   2012-4-13
    @brief
    
    Copyright (C) 2002-2012 by Alexander 'hatred' Drozdov <adrozdoff@gmail.com>
    
    This program is free software; you can redistribute it and/or modify
    it under the terms of the version 2 of GNU General Public License as
    published by the Free Software Foundation.
    
    For more information see LICENSE and LICENSE.ru files
    
   ============================================================================================== */

#include <stdint.h>

#include <iostream>

#include <boost/function.hpp>
#include <boost/thread.hpp>

#include <pion/net/HTTPServer.hpp>
#include <pion/net/HTTPTypes.hpp>
#include <pion/net/HTTPRequest.hpp>
#include <pion/net/HTTPResponse.hpp>

using namespace std;
using namespace pion;
using namespace pion::net;

class HttpStreamServer
{
public:
    void start(uint16_t port);
    void stop();

private:
    void requestHandler(HTTPRequestPtr &httpRequest, TCPConnectionPtr &tcpCon);
    void errorHandler(HTTPRequestPtr &httpRequest, TCPConnectionPtr &tcpCon, const string &message);

private:
    HTTPServerPtr httpServer;
};


///
/// Entry point
///
int main(int argc, char *argv[])
{
    HttpStreamServer server;

    server.start(8080);
    cin.get();
    server.stop();

    return 0;
}



void HttpStreamServer::start(uint16_t port)
{
    httpServer = HTTPServerPtr(new HTTPServer(port));
    httpServer->addResource("/", boost::bind(&HttpStreamServer::requestHandler, this, _1, _2));
    httpServer->setBadRequestHandler(boost::bind(&HttpStreamServer::requestHandler, this, _1, _2));
    httpServer->setServerErrorHandler(boost::bind(&HttpStreamServer::errorHandler, this, _1, _2, _3));
    httpServer->start();
}

void HttpStreamServer::stop()
{
    if (httpServer.get() != 0)
    {
        httpServer->stop();
    }
}

void HttpStreamServer::requestHandler(HTTPRequestPtr &httpRequest, TCPConnectionPtr &tcpCon)
{
    HTTPResponsePtr response = HTTPResponsePtr(new HTTPResponse(*(httpRequest.get())));
    boost::system::error_code ec;
    response->setDoNotSendContentLength();
    response->setContentType("video/x-flv");

    response->send(*(tcpCon.get()), ec, true);           // send only headers

    cout << "New request: " << httpRequest->getMethod() << endl
         << "first line:  " << httpRequest->getFirstLine() << endl;

    string helloStr = "Hello, man!\n";
    while (!ec)
    {
        tcpCon->write(boost::asio::buffer(helloStr), ec);
        cout << helloStr;
        sleep(1);
    }
}

void HttpStreamServer::errorHandler(HTTPRequestPtr &httpRequest, TCPConnectionPtr &tcpCon, const string &message)
{
    cout << "Error: " << message << endl;
}
