#include "http_server/server.hpp"
#include "http_server/auth/auth.hpp"
#include "applicationcontext.h"
#include "encodethread.h"
#include "log/logstream.h"
#include "utils.h"

#include "http.h"

namespace transcoder {
namespace transport {

using namespace std;
using namespace ::http::server;
using namespace logstream;

////////////////////////////////////////////////////////////////////////////////////////////////////

const static string MULTIPART_BOUNDARY="--htrd-boundary";

////////////////////////////////////////////////////////////////////////////////////////////////////

struct HttpWriter : public Writer
{
    HttpWriter(request_ptr &request, reply_ptr reply, const string& format)
        : request(request),
          reply(reply),
          isHeadersSended(false),
          firstPacket(true),
          format(format)
    {
        destination = request->get_resource();

        // Set output buffer size
        //boost::system::error_code ec;
        reply->connection()->socket().set_option(boost::asio::socket_base::send_buffer_size(10000));
        reply->connection()->socket().set_option(boost::asio::socket_base::receive_buffer_size(10000));
        reply->connection()->socket().set_option(boost::asio::ip::tcp::no_delay(true));
        reply->connection()->socket().set_option(boost::asio::socket_base::reuse_address(true));
        //rep->connection()->socket().non_blocking(false);
    }


    int beforePacketWrite(const PacketPtr& packet,
                          const WriterPtr& writer,
                          bool             isMultipart,
                          const string&    partMime)
    {
        if (format == "image2pipe")
        {
            reply->set_header(header::HEADER_CONTENT_LENGTH, boost::lexical_cast<string>(packet->getSize()));
        }

        if (!isHeadersSended)
        {
            // send headers only
            boost::system::error_code ec;
            reply->send(ec, true);

            // TODO Handler writer error
            if (ec)
            {
            }

            isHeadersSended = true;
        }

        if (isMultipart)
        {
            string mime("application/octet-stream");
            size_t size = packet->getSize();

            if (!partMime.empty())
            {
                mime = partMime;
            }

            ostringstream ss;
            if (!firstPacket)
            {
                ss << "\r\n";
            }

            firstPacket = false;

            ss << MULTIPART_BOUNDARY << "\r\n"
               << "Content-Type: " << mime << "\r\n"
               << "Content-Length: " << size << "\r\n"
               << "\r\n";

            return (*writer)((uint8_t*)ss.str().data(), ss.str().size());
        }

        return 0;
    }


    int afterPacketWrite(const PacketPtr& packet,
                         const WriterPtr& writer)
    {
        if (format == "image2pipe")
        {
            return -1; // end writing
        }
        return 0;
    }


    int operator ()(uint8_t *buf, int size)
    {
        //PrettyScopedTimer3("HttpSending", false);
        if (size <= 0)
        {
            return size;
        }

        int writen = 0;
        boost::system::error_code ec;

        //writen = tcpCon->write(boost::asio::buffer(buf, size), ec);
        writen = reply->send(buf, size, ec);
        //writen = tcpCon->getSocket().send(boost::asio::buffer(buf, size), 0, ec);

        if (ec.value() != 0)
        {
            level::level l = level::error;

            if (ec.value() == 32)
                l = level::debug; // Broken Pipe

            logger(l) << "Socket write error: " << ec.value() << " / " << ec.message() << endl;
            return -1;
        }

        return writen;
    }

    const char *name() const
    {
        return destination.c_str();
    }


    string      destination;
    request_ptr request;
    reply_ptr   reply;
    bool        firstPacket;
    bool        isHeadersSended;
    string      format;
};

DECLARE_PTR_TYPE(HttpWriter);

////////////////////////////////////////////////////////////////////////////////////////////////////

class ResourceHandler
{
public:
    ResourceHandler()
    {}

    ResourceHandler(const string &resource,
                    const ProducerPtr &producer,
                    const BaseTransport::Options &options)
        : resource(resource),
          producer(producer),
          options(options)
    {
    }

    void handler(request_ptr &req, reply_ptr &rep)
    {
        string resource = req->get_resource();

        logger(level::info) << "Requested resource: " << resource << endl;

        ProducerPtr producer = this->producer.lock();
        EncodeThreadPtr encoder = boost::dynamic_pointer_cast<EncodeThread>(producer);
        if (!producer ||
            !encoder  ||
            encoder->getFormatDescription().format.empty())
        {
            server::handle_not_found(req, rep);
            return;
        }

        logger(level::debug) << "Start Playing HTTP resource: " << resource << endl;

        // Create muxer and writer
        HttpWriterPtr writer(new HttpWriter(req, rep, encoder->getFormatDescription().format));
        MuxerPtr      muxer(new Muxer(encoder->getFormatDescription(), writer));

        // Detect mime type
        string mime;
        string partMime;
        bool   isMultipart = false;

        if (encoder->getFormatDescription().format == "mjpeg" ||
            encoder->getFormatDescription().format == "mjpg")
        {
            mime = "multipart/x-mixed-replace; boundary=" + MULTIPART_BOUNDARY;
            isMultipart = true;
            partMime = "image/jpeg";
        }
        else
        {
            mime = muxer->getMimeType();
        }

        // Wait between packet send
        muxer->setTimeSynchronizeWrite(true);

        // Set send buffer size
        muxer->setQueueSize(ApplicationContext::instance->sendBufferSize);

        // Register before packet handler for correct handle multipart messages (like MJPEG)
        muxer->setBeforePacketWriteHandler(boost::bind(&HttpWriter::beforePacketWrite,
                                                       writer,
                                                       _1, _2, isMultipart, partMime));
        // Register after packet handler
        muxer->setAfterPacketWriteHandler(boost::bind(&HttpWriter::afterPacketWrite,
                                                      writer,
                                                      _1, _2));
        muxer->setThreadName(this->resource + "mhttp");
        ::setThreadName(this->resource + "thttp");

        // Set headers
        rep->set_header(header::HEADER_CONTENT_TYPE, mime);
        rep->set_header("Pragma",        "no-cache");
        rep->set_header("Cache-Control", "no-cache");
        rep->set_header("Connection",    "close");

        // Start muxing
        muxer->start();
        producer->subscribe(muxer);

        // release poiters
        producer.reset();
        encoder.reset();

        bool joined;
        do
        {
            joined = muxer->timed_join(boost::posix_time::milliseconds(1000));
        }
        while (!this->producer.expired() && !joined);

        if (!joined)
        {
            muxer->stop();
            muxer->join();
        }

        producer = this->producer.lock();
        if (producer)
        {
            producer->unsubscribe(muxer);
        }

        logger(level::info) << "Finished Playing HTTP source: " << resource << endl;
    }

private:
    const string                 resource;
    const ProducerWPtr           producer;
    const BaseTransport::Options options;
};

////////////////////////////////////////////////////////////////////////////////////////////////////

const string Http::getTransportName()
{
    return "http";
}

string Http::makeServerResource(const string &resource)
{
    string resourcePrefix = "/media"; // TODO make configurable
    string serverResource = resourcePrefix;
    if (resource[0] == '/')
    {
        serverResource += resource;
    }
    else
    {
        serverResource += '/' + resource;
    }

    return serverResource;
}

BaseTransport::ResourceRegistrationStatus
Http::registerResource(const string &resource,
                       const ProducerPtr &producer,
                       const BaseTransport::Options &options)
{
    boost::mutex::scoped_lock lock(handlersMutex);
    string cleanResource = server::strip_trailing_slash(resource);

    if (handlers.find(cleanResource) != handlers.end())
    {
        return make_pair(false, MuxerPtr());
    }

    // Create handler
    ResourceHandlerPtr handler(new ResourceHandler(cleanResource, producer, options));

    // Register handler on http server
    {
        ApplicationContext::lease ctx;

        string serverResource = makeServerResource(cleanResource);
        ctx->httpServer->addHandler(serverResource, boost::bind(&ResourceHandler::handler,
                                                                handler,
                                                                _1, _2));
        logger(level::info) << "Resource '" << resource << "' registered as '" << serverResource << "'" << endl;
    }

    // Store handler
    handlers[cleanResource] = handler;

    return make_pair(true, MuxerPtr());
}

bool Http::unregisterResource(const string &resource)
{
    boost::mutex::scoped_lock lock(handlersMutex);

    string cleanResource = server::strip_trailing_slash(resource);
    string serverResource = makeServerResource(cleanResource);

    if (handlers.find(cleanResource) == handlers.end())
    {
        return false;
    }

    // Unregister handler on http server
    {
        ApplicationContext::lease ctx;
        ctx->httpServer->removeHandler(serverResource);
        logger(level::info) << "Resource '" << resource << "' unregistered as '" << serverResource << "'" << endl;
    }

    // Remove handler
    handlers.erase(cleanResource);
    return true;
}

} // namespace transport
} // namespace transcoder
