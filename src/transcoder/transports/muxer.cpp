#include "log/logstream.h"
#include "encodethread.h"

#include "muxer.h"
#include "utils.h"
#include "av/avutils.h"

namespace transcoder {
namespace transport {

using namespace std;
using namespace logstream;

Muxer::Muxer()
    : isHeaderWriten(false)
{
    commonCtor();
}

Muxer::Muxer(const WriterPtr &writer)
    : writer(writer),
      isHeaderWriten(false)
{
    commonCtor();
}

Muxer::Muxer(const FormatDescription &format, const WriterPtr &writer)
    : BaseEncodeContext(format),
      writer(writer),
      isHeaderWriten(false)
{
    commonCtor();
}

void Muxer::commonCtor()
{
    // Temporary
    prevTime = time(0);

    packetQueue.setMaxQueueSize(100);

    isFirstRecvPacket      = true;
    doTimeSynchronizeWrite = false;
}

void Muxer::waitReady()
{
    readyCond.wait();
}

Muxer::~Muxer()
{
    stop();
}


void Muxer::run()
{
    PacketTimeSynchronizer packetSync;

    readyCond.wakeUpAll();

    // HACK: wait buffer filling
    while (packetQueue.size() < packetQueue.getMaxQueueSize() / 2)
        boost::this_thread::yield();

    bool firstPacket = true;
    while (doWork)
    {
        PacketPtr packet;

        if (!packetQueue.waitAndPop(packet))
        {
            continue;
        }

        if (firstPacket)
        {
            firstPacket = false;
            packetSync.reset();
        }

        {

        if (doTimeSynchronizeWrite)
        {
            packetSync.doTimeSync(packet);
            //clog << "SleepDelta: " << sleepTime << endl;
        }

        {
            int stat = writePacket(packet);
            if (stat < 0)
            {
                logger(level::debug) << "Write with error: " << stat << std::endl;
                break;
            }
        }
        }
    }
}

void Muxer::setBeforePacketWriteHandler(const PacketWriteHandler &handler)
{
    beforePacketWrite = handler;
}

void Muxer::setAfterPacketWriteHandler(const PacketWriteHandler &handler)
{
    afterPacketWrite = handler;
}

void Muxer::setTimeSynchronizeWrite(bool flag)
{
    doTimeSynchronizeWrite = flag;
}

void Muxer::setQueueSize(uint32_t queueSize)
{
    if (queueSize > 0)
        packetQueue.setMaxQueueSize(queueSize);
}

uint32_t Muxer::getQueueSize() const
{
    return packetQueue.getMaxQueueSize();
}

void Muxer::start()
{
    doWork = true;
    Thread::start();
    waitReady();
}

void Muxer::stop()
{
    doWork = false;
    packetQueue.cancelWait();
    readyCond.wakeUpAll();

    if (!timed_join(boost::posix_time::seconds(3)))
    {
        kill();
        join();
    }
}


void Muxer::notify(const Producer *producer)
{
    const EncodeThread *source = dynamic_cast<const EncodeThread*>(producer);
    if (!source)
    {
        return;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    //PrettyScopedTimer3("MuxerNotify", false);

    if (!isValid())
    {
        init(source->getFormatDescription());
    }

    // Be thread-safe, make our own copy of packet
    PacketPtr packet = PacketPtr(new Packet(*source->lastEncodedPacket()));

    if (packet->getPts() != AV_NOPTS_VALUE || packet->getFakePts() != AV_NOPTS_VALUE)
    {
        av::PacketPtsRecalculator &recalculator = ptsRecalc[packet->getStreamIndex()];

        recalculator.useOffset(true);
        if (recalculator.recalc(packet) == false)
        {
            // Skip same PTS
            logger(level::warning) << "Skip packet with PTS same to previous (" << packet->getStreamIndex() << "): " << recalculator.getLastPts() << std::endl;
            return;
        }

        packet->setDts(packet->getPts());
    }

    //packet->setDts(AV_NOPTS_VALUE);


    // currently simple drop new frames if queue is full
    if (!packetQueue.push(packet))
    {
        logger(level::trace) << "Error: can't add packet to queue: " << source->getFormatDescription().resource << std::endl;
    }

    isFirstRecvPacket = false;

    // Temporary
    {
        time_t t = time(0);
        if (t - prevTime > 10)
        {
            logger(level::info)  << "Send buffer size: " << packetQueue.size()
                                 << " - "
                                 << source->getFormatDescription().resource
                                 << std::endl;
            prevTime = t;
        }
    }

//    if (string(format->getOutputFormatName()) == string("flv"))
//    {
//        logger(level::info)
//                << "Packet: " << packet->getStreamIndex()
//                << ", pts = " << packet->getPts()
//                << ", dts = " << packet->getDts()
//                << ", fake pts = " << packet->getFakePts()
//                << ", tb = " << packet->getTimeBase()
//                << ", time = " << (packet->getPts() != AV_NOPTS_VALUE ?
//                                                           packet->getPts() * packet->getTimeBase().getDouble() :
//                                                           packet->getFakePts() * packet->getTimeBase().getDouble())
//                << std::endl;
//    }

}


int Muxer::writePacket(const PacketPtr &packet)
{
    if (!isValid())
    {
        return -128;
    }

    if (!container->isOpened())
    {
        if (!writer)
        {
            return -129;
        }

        if (!container->openOutput(*writer))
        {
            return -130;
        }

        container->dump();
    }

    if (!isHeaderWriten)
    {
        container->writeHeader();
        isHeaderWriten = true;
    }

    int writen = 0;
    int result = -1;
    if (beforePacketWrite)
    {
        result = beforePacketWrite(packet, writer);
        if (result < 0)
        {
            return result;
        }
        writen += result;
    }

//    clog << "Output packet time " << packet->getStreamIndex()
//         << ", "           << packet->getTimeBase().getDouble() * packet->getPts()
//         << ", fake pts: " << packet->getTimeBase().getDouble() * packet->getFakePts()
//         << endl;

    result = container->writePacket(packet, false);
    if (result < 0)
    {
        return result;
    }
    writen += result;


    if (afterPacketWrite)
    {
        result = afterPacketWrite(packet, writer);
        if (result < 0)
        {
            return result;
        }
        writen += result;
    }

    return writen;
}


} // namespace transport
} // namespace transcoder
