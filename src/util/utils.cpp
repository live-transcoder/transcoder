#include <iostream>

#include <boost/thread.hpp>
#include <boost/date_time.hpp>

#include "utils.h"

using namespace std;

// Sleep for a millisecond
void sleep_msec(int64_t interval)
{
    boost::this_thread::disable_interruption di;
#if BOOST_VERSION >= 105000
    boost::this_thread::sleep_for(boost::chrono::milliseconds(interval));
#else
    boost::this_thread::sleep(boost::posix_time::milliseconds(interval));
#endif
}


// Sleep for a nanosecond
void sleep_usec(uint64_t interval)
{
    boost::this_thread::disable_interruption di;
#if BOOST_VERSION >= 105000
    boost::this_thread::sleep_for(boost::chrono::microseconds(interval));
#else
    boost::this_thread::sleep(boost::posix_time::microseconds(interval));
#endif
}


/**
 * Replace place holders look like %N (N is any valid latin1 symbol) with values from replace map.
 * Unknown place holders stay as is.
 *
 * @param str        string where place holders must be reseted
 * @param replaceMap
 */
void placeHoldersReplace(string& str, const map<std::string, std::string>& replaceMap)
{
    string::size_type pos = 0;

    while ((pos = str.find('%', pos)) != string::npos)
    {
        if ((pos + 1) != str.length())
        {
            const string key = str.substr(pos, 2);

            if (replaceMap.count(key))
            {
                str.replace(pos, 2, replaceMap.at(key));
            }

            // skip next character
            ++pos;
        }

        // next search from next character
        ++pos;
    }
}


ScopedTimer::TimingAvarageData ScopedTimer::average;
boost::shared_mutex            ScopedTimer::averageMutex;

ScopedTimer::ScopedTimer(const std::string &name, bool printStuf)
    : name(name),
      startClock(av::gettime()),
      printStuf(printStuf)
{
}


ScopedTimer::~ScopedTimer()
{
    time_t currentClock = av::gettime();
    ScopedTimer::process(name, startClock, currentClock);
}


void ScopedTimer::dump(std::ostream *ost)
{
    boost::shared_lock<boost::shared_mutex> lock(averageMutex);

    TimingAvarageData::iterator it;
    for (it = average.begin(); it != average.end(); ++it)
    {
        boost::thread::id threadId = it->first;
        ThreadData::iterator j;
        for (j = it->second.begin(); j != it->second.end(); ++j)
        {
            const std::string  &name   = j->first;
            const TimingValues &values = j->second;

            (*ost) << name
                   << ": avg: " << values.timeSum / values.callsCount
                   << ", min: " << values.minTime
                   << ", max: " << values.maxTime
                   << ", btw: " << values.timeBetweenCallSum / (values.callsCount - 1)
                   << ", cnt: " << values.callsCount
                   << std::endl;
        }
    }
}

void ScopedTimer::process(const std::string &name, ScopedTimer::time_t startClock, ScopedTimer::time_t currentClock)
{
    time_t clockDelta = currentClock - startClock;
    double sec = (double)clockDelta / 1000.0;

    boost::thread::id currentThreadId = boost::this_thread::get_id();

    ThreadData *threadData = 0;
    // take item
    {
        boost::shared_lock<boost::shared_mutex> lock(averageMutex);
        if (average.find(currentThreadId) != average.end())
        {
            ThreadData &data = average[currentThreadId];
            threadData = &data;
        }
    }

    // create new item
    if (threadData == 0)
    {
        boost::unique_lock<boost::shared_mutex> lock(averageMutex);
        ThreadData data;
        average[currentThreadId] = data;

        ThreadData &dataRef = average[currentThreadId];
        threadData = &dataRef;
    }

    if (threadData->find(name) == threadData->end())
    {
        (*threadData)[name] = TimingValues();
        (*threadData)[name].minTime  = sec;
        (*threadData)[name].maxTime  = sec;
    }

    threadData->at(name).timeSum += sec;
    threadData->at(name).callsCount  += 1;
    if (threadData->at(name).lastCall > 0.0)
        threadData->at(name).timeBetweenCallSum += (startClock / 1000.0 - threadData->at(name).lastCall);
    threadData->at(name).lastCall = startClock / 1000.0;

    double avg = threadData->at(name).timeSum / threadData->at(name).callsCount;

    if (sec > threadData->at(name).maxTime)
    {
        threadData->at(name).maxTime = sec;
    }
    else if (sec < threadData->at(name).minTime)
    {
        threadData->at(name).minTime = sec;
    }

    if (printStuf)
    {
        std::clog << name
                  << ":\t cur: " << sec
                  << ", avg: " << avg
                  << ", min: " << threadData->at(name).minTime
                  << ", max: " << threadData->at(name).maxTime
                  << std::endl;
    }
}


