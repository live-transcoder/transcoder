#include <iostream>
#include <algorithm>
#include <sstream>
#include <utility>
#include <list>
#include <vector>

#include "baseencodecontext.h"
#include "av/ffmpeg.h"
#include "log/logstream.h"

using namespace logstream;
using namespace std;
using namespace av;

namespace transcoder {
namespace encode {

static int stringToInt(const string &str)
{
    int result = 0;
    istringstream stream(str);
    stream >> result;
    return result;
}

static vector<string> splitString(const string &str, const string &delim)
{
    vector<string> temp;
    if ( delim.empty() )
    {
        temp.push_back( str );
        return temp;
    }

    typedef string::const_iterator iter;
    const iter::difference_type f_size( distance( delim.begin(), delim.end() ) );
    iter i( str.begin() );
    for ( iter pos; ( pos = search( i , str.end(), delim.begin(), delim.end() ) ) != str.end(); )
    {
        temp.push_back( string( i, pos ) );
        advance( pos, f_size );
        i = pos;
    }
    temp.push_back( string( i, str.end() ) );
    return temp;
}


static Rational stringToRational(const string &str)
{
    vector<string> tokens = splitString(str, "/");
    Rational result;

    result.setNumerator(stringToInt(tokens.at(0)));
    if (tokens.size() > 1)
        result.setDenominator(stringToInt(tokens.at(1)));

    return result;
}




BaseEncodeContext::BaseEncodeContext()
    : isValidFlag(false)
{
}

BaseEncodeContext::BaseEncodeContext(const FormatDescription &description)
    : isValidFlag(false)
{
    init(description);
}

BaseEncodeContext::~BaseEncodeContext()
{
}

bool BaseEncodeContext::init(const FormatDescription &description)
{
    if (description.format.empty() || description.streams.empty())
    {
        // Invalid format description
        logger(level::error) << "Invalid format description" << std::endl;
        return false;
    }

    if (this->isValidFlag)
    {
        coders.clear();
        streams.clear();
        codecs.clear();
        container.reset();
        format.reset();
    }

    string formatName = description.format;

    format = ContainerFormatPtr(new ContainerFormat());
    format->setOutputFormat(formatName.c_str(), 0, 0);

    container = ContainerPtr(new Container());
    container->setFormat(format);

    ///////// STREAM SPECIFIC ///////////
    for (vector<StreamDescription>::const_iterator it = description.streams.begin();
         it != description.streams.end();
         ++it)
    {
        const StreamDescription &streamDesc = *it;

        //int mapFrom = it->first;
        //int mapTo   = streamDesc.mapToIndex;

        CodecPtr       codec;
        StreamPtr      stream;
        StreamCoderPtr coder;

        if (!streamDesc.codec.empty())
        {
            codec = Codec::findEncodingCodec(streamDesc.codec.c_str());
            if (!codec)
            {
                logger(level::warning) << "Can't find user specific codec: " << streamDesc.codec << endl;
            }
        }

        if (!codec)
        {
            logger(level::info) << "Fallback to default codec" << endl;
            if (streamDesc.getMediaType() == AVMEDIA_TYPE_AUDIO)
            {
                codec = Codec::findEncodingCodec(format->getOutputDefaultAudioCodec());
            }
            else if (streamDesc.getMediaType() == AVMEDIA_TYPE_VIDEO)
            {
                codec = Codec::findEncodingCodec(format->getOutputDefaultVideoCodec());
            }
            else
            {
                logger(level::warning) << "Unsupported media type: " << streamDesc.getMediaType() << endl;
            }
        }

        if (!codec)
        {
            logger(level::info) << "Can't found codec for stream, skip stream" << std::endl;
            continue;
        }

        stream = container->addNewStream(codec);
        coder  = StreamCoderPtr(new StreamCoder(stream));

        // Workaround: must be fixed
        coder->setCodec(codec);

        int defaultBitRate = 64000;
        AVMediaType mediaType = streamDesc.getMediaType();
        switch (mediaType)
        {
            case AVMEDIA_TYPE_VIDEO:
            {
                int width               = streamDesc.width;
                int height              = streamDesc.height;
                PixelFormat pixelFormat = coder->getPixelFormat();

                coder->setWidth(width);
                coder->setHeight(height);

                if (formatName == "mjpeg" ||
                    formatName == "mpjpeg" ||
                    formatName == "jpeg" ||
                    formatName == "jpg")
                {
                    pixelFormat = PIX_FMT_YUVJ420P;
                }

                coder->setPixelFormat(pixelFormat);

                if (streamDesc.videoGopSize > 0)
                {
                    coder->setGopSize(streamDesc.videoGopSize);
                }


                ////// FRAME RATES AND TIME BASES //////
                // TODO: check for supported time-bases and frame-rates

                Rational frameRate = Rational();
                if (streamDesc.videoFrameRate == Rational())
                {
                    if (codec->getAVCodec()->supported_framerates)
                    {
                        std::list<Rational> list = codec->getSupportedFramerates();
                        frameRate = *(list.begin());
                    }
                }
                else
                {
                    frameRate = streamDesc.videoFrameRate;
                }

                if (frameRate == Rational())
                {
                    frameRate = Rational(25, 1);
                }

                coder->setTimeBase(Rational(1, 1000));
                stream->setTimeBase(Rational(1, 1000));
                coder->setFrameRate(frameRate);

                if (formatName == "mpegts")
                {
                    coder->setTimeBase(Rational(1001, 30000));
                    stream->setTimeBase(Rational(1001, 30000));
                }

                logger(level::info) << "Frame rate: " << frameRate << endl;
                ////// FRAME RATES AND TIME BASES //////

                // TODO: set strict
                if (streamDesc.videoStrict)
                {}

                if (streamDesc.videoBitRateTolerance != -1)
                {
                    coder->setBitRateTolerance(streamDesc.videoBitRateTolerance);
                }

                defaultBitRate = 100000;              
                if (formatName == "image2pipe")
                {
                    coder->getAVCodecContext()->global_quality = 100;
                    defaultBitRate = 500000;
                }

                break;
            }

            case AVMEDIA_TYPE_AUDIO:
            {
                if (streamDesc.audioChannels > 0)
                {
                    coder->setChannels(streamDesc.audioChannels);
                }
                else
                {
                    coder->setChannels(2);
                }

                if (streamDesc.audioSampleRate > 0)
                {
                    coder->setSampleRate(streamDesc.audioSampleRate);
                }
                else
                {
                    coder->setSampleRate(22050);
                }

                if (streamDesc.audioSampleFormat != AV_SAMPLE_FMT_NONE)
                {
                    coder->setSampleFormat(streamDesc.audioSampleFormat);
                }

                // WORKAROUND: FLV container support only 44100, 22050 and 11025 Hz sample rates
                // and 16000, 8000, 5512 Hz in some cases (non MP3 codec)
                if (formatName == "flv")
                {
                    int flvSampleRates[] = {44100, 22050, 11025, 0};
                    int sr = guessValue(coder->getSampleRate(), flvSampleRates, EqualComparator<int>(0));
                    coder->setSampleRate(sr);
                }

                //coder->setSampleFormat();

                defaultBitRate = 64000;
                break;
            }

            case AVMEDIA_TYPE_DATA:
            case AVMEDIA_TYPE_SUBTITLE:
            case AVMEDIA_TYPE_ATTACHMENT:
            case AVMEDIA_TYPE_NB:
            case AVMEDIA_TYPE_UNKNOWN:
                break;
        }

        // Common - ???
        if (streamDesc.bitRate > 0)
        {
            coder->setBitRate(streamDesc.bitRate);
        }
        else if (streamDesc.bitRateMin > 0 && streamDesc.bitRateMax > 0)
        {
            if (streamDesc.bitRateMin == streamDesc.bitRateMax)
            {
                coder->setBitRate(streamDesc.bitRateMin);
            }
            else
            {
                coder->setBitRateRange(pair<int,int>(streamDesc.bitRateMin, streamDesc.bitRateMax));
            }
        }
        else
        {
            coder->setBitRate(defaultBitRate);
        }

        if (format->getOutputFlags() & AVFMT_GLOBALHEADER)
        {
            coder->addFlags(CODEC_FLAG_GLOBAL_HEADER);
        }
        //


        codecs.push_back(codec);
        streams.push_back(stream);
        coders.push_back(coder);
    }
    ///////// STREAM SPECIFIC ///////////



    //initFilters("fps=fps=25");
    //initFilters("fps=fps=1");
    //initFilters("negate");
    //initFilters("null");

    container->dump();
    this->isValidFlag = true;
    this->description = description;

    return true;
}


string BaseEncodeContext::getMimeType() const
{
    if (isValidFlag)
    {
        if (format->getOutputFormatMimeType())
        {
            return string(format->getOutputFormatMimeType());
        }
        else
        {
            // TODO more clean way for mime type detecting
            if (format->getOutputFormat()->video_codec == CODEC_ID_MJPEG)
            {
                return string("image/jpeg");
            }
            else if (format->getOutputFormat()->video_codec == CODEC_ID_GIF)
            {
                return string("image/gif");
            }
            else if (format->getOutputFormat()->video_codec == CODEC_ID_PNG)
            {
                return string("image/png");
            }
        }
    }
    return string("application/octet-stream");
}

// TODO ugly current name, rename predicate
struct OriginalStreamIndexPredicate
{
    OriginalStreamIndexPredicate(int index)
        : index(index)
    {}

    bool operator()(const StreamDescription& desc)
    {
        if (index == desc.mapFromIndex)
            return true;
        else
            return false;
    }

    int index;
};

int BaseEncodeContext::mappedStreamIndex(int inputStreamIndex)
{
    int result = -1;

    vector<StreamDescription>::iterator it =
            std::find_if(description.streams.begin(),
                         description.streams.end(),
                         OriginalStreamIndexPredicate(inputStreamIndex));

    if (it != description.streams.end())
    {
        result = it->mapToIndex;
    }

    return result;
}


void BaseEncodeContext::initFilters(const char *filtersDesc)
{
#if 0
    char args[255];
    int ret;

    AVFilter *bufferSrc = avfilter_get_by_name("buffer");
    AVFilter *bufferSink = avfilter_get_by_name("buffersink");

    AVFilterInOut *outputs = avfilter_inout_alloc();
    AVFilterInOut *inputs  = avfilter_inout_alloc();

    PixelFormat pixFmts[] = {videoCoder->getPixelFormat(), PIX_FMT_NONE};

    filterGraph = avfilter_graph_alloc();

    snprintf(args, sizeof(args), "%d:%d:%d:%d:%d:%d:%d",
             videoCoder->getWidth(), videoCoder->getHeight(), videoCoder->getPixelFormat(),
             videoCoder->getTimeBase().getNumerator(), videoCoder->getTimeBase().getDenominator(),
             videoCoder->getAVCodecContext()->sample_aspect_ratio.num,
             videoCoder->getAVCodecContext()->sample_aspect_ratio.den);

    ret = avfilter_graph_create_filter(&bufferSrcCtx, bufferSrc, "in", args, 0, filterGraph);
    if (ret < 0)
    {
        logger(level::fatal) << "Cannot create buffer source" << endl;
        exit(1);
    }


    ret = avfilter_graph_create_filter(&bufferSinkCtx, bufferSink, "out", 0, pixFmts, filterGraph);
    if (ret < 0)
    {
        logger(level::fatal) << "Cannot create buffer sink" << endl;
        exit(1);
    }

    outputs->name = av_strdup("in");
    outputs->filter_ctx = bufferSrcCtx;
    outputs->pad_idx = 0;
    outputs->next = 0;

    inputs->name = av_strdup("out");
    inputs->filter_ctx = bufferSinkCtx;
    inputs->pad_idx = 0;
    inputs->next = 0;

    ret = avfilter_graph_parse(filterGraph, filtersDesc, &inputs, &outputs, 0);
    if (ret < 0)
    {
        logger(level::fatal) << "Cannot parse filters" << endl;
        exit(1);
    }

    ret = avfilter_graph_config(filterGraph, 0);
    if (ret < 0)
    {
        logger(level::fatal) << "Cannot config graph" << endl;
        exit(1);
    }
#endif
}




}} // ::transcoder::encode
