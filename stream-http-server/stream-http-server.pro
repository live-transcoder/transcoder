#-------------------------------------------------
#
# Project created by QtCreator 2012-04-13T23:39:01
#
#-------------------------------------------------

TARGET = stream-http-server
TEMPLATE = app

CONFIG += link_pkgconfig

PKGCONFIG += pion-net

DEFINES += __STDC_CONSTANT_MACROS

SOURCES += main.cpp
