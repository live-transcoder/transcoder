#include "addserverdialog.h"

AddServerDialog::AddServerDialog(QWidget *parent) :
    QDialog(parent)
{
    setupUi(this);
}


QString AddServerDialog::getHostAndPort() const
{
    return hostAndPortLineEdit->text();
}


QString AddServerDialog::getLogin() const
{
    return loginLineEdit->text();
}


QString AddServerDialog::getPassword() const
{
    return passwordLineEdit->text();
}

QString AddServerDialog::getTitle() const
{
    return titleLineEdit->text();
}

void AddServerDialog::setHostAndPort(const QString &hostPort)
{
    hostAndPortLineEdit->setText(hostPort);
}

void AddServerDialog::setLogin(const QString &login)
{
    loginLineEdit->setText(login);
}

void AddServerDialog::setPassword(const QString &password)
{
    passwordLineEdit->setText(password);
}

void AddServerDialog::setTitle(const QString &title)
{
    titleLineEdit->setText(title);
}


void AddServerDialog::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
        case QEvent::LanguageChange:
            retranslateUi(this);
            break;
        default:
            break;
    }
}
