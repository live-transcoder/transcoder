#include <sstream>
#include <locale>

#include <boost/regex.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/foreach.hpp>
#include <boost/optional.hpp>

#include <pion/net/HTTPServer.hpp>

#include "adminrest.h"
#include "transcodermanager.h"
#include "transcoderdao.h"

#include "log/logstream.h"

using namespace logstream;

//
// Make UTF8 chars valid in JSON output (non escape it)
// see: http://stackoverflow.com/questions/10260688/boostproperty-treejson-parser-and-two-byte-wide-characters
//
namespace boost { namespace property_tree { namespace json_parser
{
    // Create necessary escape sequences from illegal characters
    template<>
    std::basic_string<char> create_escapes(const std::basic_string<char> &s)
    {
        std::basic_string<char> result;
        std::basic_string<char>::const_iterator b = s.begin();
        std::basic_string<char>::const_iterator e = s.end();
        while (b != e)
        {
            // This assumes an ASCII superset. But so does everything in PTree.
            // We escape everything outside ASCII, because this code can't
            // handle high unicode characters.
            if (*b == 0x20 || *b == 0x21 || (*b >= 0x23 && *b <= 0x2E) ||
                (*b >= 0x30 && *b <= 0x5B) || (*b >= 0x5D && *b <= 0xFF)  //it fails here because char are signed
                || (*b >= -0x80 && *b < 0 ) ) // this will pass UTF-8 signed chars
                result += *b;
            else if (*b == char('\b')) result += char('\\'), result += char('b');
            else if (*b == char('\f')) result += char('\\'), result += char('f');
            else if (*b == char('\n')) result += char('\\'), result += char('n');
            else if (*b == char('\r')) result += char('\\'), result += char('r');
            else if (*b == char('/')) result += char('\\'), result += char('/');
            else if (*b == char('"'))  result += char('\\'), result += char('"');
            else if (*b == char('\\')) result += char('\\'), result += char('\\');
            else
            {
                const char *hexdigits = "0123456789ABCDEF";
                typedef make_unsigned<char>::type UCh;
                unsigned long u = (std::min)(static_cast<unsigned long>(
                                                 static_cast<UCh>(*b)),
                                             0xFFFFul);
                int d1 = u / 4096; u -= d1 * 4096;
                int d2 = u / 256; u -= d2 * 256;
                int d3 = u / 16; u -= d3 * 16;
                int d4 = u;
                result += char('\\'); result += char('u');
                result += char(hexdigits[d1]); result += char(hexdigits[d2]);
                result += char(hexdigits[d3]); result += char(hexdigits[d4]);
            }
            ++b;
        }
        return result;
    }
} } }




namespace transcoder {
namespace admin {
namespace rest {

#define CONTENT_TYPE_JSON "application/json"

static inline
string makeContentType(const string &contentType, const string &charset = "UTF-8")
{
    string result = contentType;
    if (!charset.empty())
    {
        result += "; charset=" + charset;
    }
    return result;
}

struct StringReplacer
{
    StringReplacer(string& source)
        : source(source)
    {}

    StringReplacer& replace(const string& from, const string& to)
    {
        boost::regex expression(from);
        source = boost::regex_replace(source, expression, to, boost::match_default | boost::format_all);
        return *this;
    }

private:
    string& source;
};

static inline
string makeJsonString(string text)
{
    string source = text;
    StringReplacer(text).replace("(\\n)"     "|(')"      "|(\")"      "|(\\r)"    "|(&)"      "|(\\\\)",
                                 "(?1\\\\n)" "(?2\\\\')" "(?3\\\\\")" "(?4\\\\r)" "(?5\\\\&)" "(?6\\\\\\\\)");

    return text;
}


AdminRest::AdminRest()
{
#define BIND(resource, method) addResource(resource, boost::bind(&AdminRest::method, this, _1, _2))

    BIND("/admin/transcoder/add",    transcoderAdd);
    BIND("/admin/transcoder/delete", transcoderDelete);
    BIND("/admin/transcoder/list",   transcoderList);
    BIND("/admin/transcoder/modify", transcoderModify);

    BIND("/admin/transcoder/start",   transcoderStart);
    BIND("/admin/transcoder/stop",    transcoderStop);
    BIND("/admin/transcoder/restart", transcoderRestart);
    BIND("/admin/transcoder/restart-all", transcoderRestartAll);

    BIND("/admin/transcoder/save",    transcoderSave);
    BIND("/admin/transcoder/save-all",transcoderSaveAll);

#undef BIND
}

bool AdminRest::handle(http::server::request_ptr &req, http::server::reply_ptr &rep)
{
    // handle
    InternalRequestHandler handler;
    if (findResource(req->get_resource(), handler))
    {
        if (!handler.empty())
        {
            rep->set_header(http::server::header::HEADER_CONTENT_TYPE, makeContentType(CONTENT_TYPE_JSON));

            boost::shared_ptr<backend::string_backend> st(new backend::string_backend);
            st->set_threshold_ignoring(true);
            logger().set_thread_logging_backend(st);

            handler(req, rep);

            logger().set_thread_logging_backend(backend::backend_ptr());

            boost::system::error_code ec;
            rep->send(ec);

            return true;
        }
    }

    return false;
}

void AdminRest::addResource(const string &resource, const AdminRest::InternalRequestHandler &handler)
{
    const string cleanResource(http::server::server::strip_trailing_slash(resource));
    resources[cleanResource] = handler;
}

bool AdminRest::findResource(const string &resource, AdminRest::InternalRequestHandler &handler) const
{
    const string cleanResource(http::server::server::strip_trailing_slash(resource));
    if (resources.find(cleanResource) != resources.end())
    {
        handler = resources.at(cleanResource);
        return true;
    }

    return false;
}

void AdminRest::requestValidate(http::server::request_ptr &req,
                                http::server::reply_ptr &rep,
                                const string &method,
                                const string &mime)
{
    if (req->get_method() != method)
    {
        rep->set_status(http::server::reply::not_allowed);
        throw logic_error("Given method does not allowed: " + req->get_method()  + "<br />\n");
    }

    if (!mime.empty())
    {
        string contentType;
        req->get_header(http::server::header::HEADER_CONTENT_TYPE, contentType);

        boost::regex expression("^" + mime + ".*");
        boost::cmatch match;
        if (!boost::regex_match(contentType.c_str(), match, expression))
        {
            rep->set_status(http::server::reply::bad_request);

            throw logic_error("Given mime type does not allowed: " + contentType + "<br />\n");
        }
    }
}


static inline
void genericResponse(http::server::reply_ptr& rep,
                     int returnStatus = 0,
                     const exception* e = 0,
                     const string& message = string())
{
    string error;
    string text   = message;
    string status = boost::lexical_cast<string>(returnStatus);

    if (returnStatus > 0)
    {
        boost::shared_ptr<backend::string_backend> st =
                boost::dynamic_pointer_cast<backend::string_backend>(logger().get_thread_logging_backend());

        if (!text.empty())
        {
            text += '\n';
        }

        if (e)
        {
            text += string(e->what());
        }

        if (st)
        {
            text += "\nLog:\n" + st->str();
        }
    }

    error = "{\"error_code\" : " + status + ", \"error_text\" : \"" + makeJsonString(text) + "\"}";

    rep->set_content(error);
}


void AdminRest::transcoderList(http::server::request_ptr &req, http::server::reply_ptr &rep)
{
    logger(level::debug) << "REST: transcoder list" << endl;

    try
    {
        requestValidate(req, rep, "GET", "");

        list<TranscoderPtr> transcoders = TranscoderManager::instance->getTranscoders();
        list<TranscoderPtr>::iterator it = transcoders.begin();

        boost::property_tree::ptree root;
        boost::property_tree::ptree items;
        for (; it != transcoders.end(); ++it)
        {
            boost::property_tree::ptree item;
            item.put("name", (*it)->getName());
            item.put("description", (*it)->getDescription());
            item.put("enabled", (*it)->isEnabled());
            item.put("active", (*it)->isActive());
            item.put("started", (*it)->isRunning());

            // source
            FormatDescription source;
            source.uri = (*it)->getInput();
            source.format = (*it)->getInputFormat();
            boost::property_tree::ptree sourceNode;
            formatDescriptionToPtree(source, sourceNode);
            item.put_child("source", sourceNode);

            // destination
            boost::property_tree::ptree destinationNode;
            vector<FormatDescription> destinations = (*it)->getEncoders();
            vector<FormatDescription>::iterator j = destinations.begin();
            for (; j != destinations.end(); ++j)
            {
                FormatDescription &desc = *j;
                boost::property_tree::ptree node;
                formatDescriptionToPtree(desc, node);
                destinationNode.push_back(make_pair("", node));
            }
            item.add_child("destination", destinationNode);

            items.push_back(make_pair("", item));
        }

        root.put("error_code", 0);
        root.put("error_text", "");
        root.add_child("transcoders", items);

        // Serialize to JSON and write
        {
            stringstream ss;
            boost::property_tree::json_parser::write_json(ss, root);

            rep->set_content(ss.str());
        }
    }
    catch (const exception &e)
    {
        genericResponse(rep, 1, &e);
        return;
    }
}


static inline
void fillTranscoder(TranscoderPtr &transcoder, const boost::property_tree::ptree &node, bool doUpdate = false)
{
    string name = node.get<string>("name");
    boost::algorithm::trim(name);
    if (name.empty())
    {
        throw logic_error("Empty transcoder name");
    }

    string description = node.get<string>("description", string());
    try
    {
        bool enabled = node.get<bool>("enabled");
        transcoder->setEnabled(enabled);
    }
    catch (const exception &e)
    {
        if (doUpdate == false)
            transcoder->setEnabled(false);
    }

    transcoder->setName(name);
    if (description != string())
        transcoder->setDescription(description);

    try
    {
        boost::property_tree::ptree sourceNode = node.get_child("source");
        FormatDescription sourceFormat;
        ptreeToFormatDescription(sourceNode, sourceFormat);
        transcoder->setInput(sourceFormat.uri, sourceFormat.format);
    }
    catch (const exception &e)
    {
        if (doUpdate == false)
            throw;
    }

    boost::optional<const boost::property_tree::ptree&> destinations = node.get_child_optional("destination");
    if (destinations.is_initialized())
    {
        transcoder->removeEncoders();
        BOOST_FOREACH(const boost::property_tree::ptree::value_type &child,
                      destinations.get().get_child(""))
        {
            FormatDescription desc;
            if (ptreeToFormatDescription(child.second, desc))
            {
                if (!transcoder->addEncoder(desc))
                {
                    transcoder->removeEncoder(desc);
                    if (!transcoder->addEncoder(desc))
                    {
                        // FIXME: process this case, where we can't add some amount of output formats
                    }
                }
            }
        }
    }
}


void AdminRest::transcoderAdd(http::server::request_ptr &req, http::server::reply_ptr &rep)
{
    logger(level::debug) << "REST: transcoder add" << endl;

    try
    {
        requestValidate(req, rep, "POST", CONTENT_TYPE_JSON);

        // take string data from request
        string data(reinterpret_cast<const char *>(req->get_data().data()), req->get_data().size());

        stringstream ss;
        boost::property_tree::ptree root;
        ss << data;

        boost::property_tree::json_parser::read_json(ss, root);

        boost::property_tree::ptree transcoderNode;
        transcoderNode = root.get_child("transcoder");
        TranscoderPtr transcoder(new Transcoder());

        fillTranscoder(transcoder, transcoderNode);

        if (!TranscoderManager::instance->addTranscoder(transcoder))
        {
            throw logic_error("Can't add transcoder. See logs for additional info.");
        }
    }
    catch (const exception &e)
    {
        genericResponse(rep, 1, &e);
        return;
    }

    genericResponse(rep);
}

void AdminRest::transcoderModify(http::server::request_ptr &req, http::server::reply_ptr &rep)
{
    logger(level::debug) << "REST: transcoder modify" << endl;

    try
    {
        requestValidate(req, rep, "POST", CONTENT_TYPE_JSON);

        // take string data from request
        string data(reinterpret_cast<const char *>(req->get_data().data()), req->get_data().size());

        stringstream ss;
        boost::property_tree::ptree root;
        ss << data;

        boost::property_tree::json_parser::read_json(ss, root);

        boost::property_tree::ptree transcoderNode;
        transcoderNode = root.get_child("transcoder");

        string name = transcoderNode.get<string>("name");
        boost::algorithm::trim(name);
        if (name.empty())
        {
            throw logic_error("Empty transcoder name");
        }

        TranscoderPtr transcoder = TranscoderManager::instance->getTranscoder(name);
        if (!transcoder)
        {
            throw logic_error("Transcoder with name: '" + name + "' does not exists");
        }

        ScopedTryLock<Transcoder> lock(transcoder.get(), true, "Can't lock transcoder to modify");
        fillTranscoder(transcoder, transcoderNode, true);

        logger(level::debug) << "Description: " << transcoder->getDescription() << endl;
    }
    catch (const exception &e)
    {
        genericResponse(rep, 1, &e);
        return;
    }

    genericResponse(rep);
}

void AdminRest::transcoderDelete(http::server::request_ptr &req, http::server::reply_ptr &rep)
{
    logger(level::debug) << "REST: transcoder delete" << endl;
    try
    {
        requestValidate(req, rep, "POST", CONTENT_TYPE_JSON);

        // take string data from request
        string data(reinterpret_cast<const char *>(req->get_data().data()), req->get_data().size());

        stringstream ss;
        boost::property_tree::ptree root;
        ss << data;

        boost::property_tree::json_parser::read_json(ss, root);

        boost::property_tree::ptree transcoderNode;
        transcoderNode = root.get_child("transcoder");

        string name = transcoderNode.get<string>("name");
        boost::algorithm::trim(name);
        if (name.empty())
        {
            throw logic_error("Empty transcoder name");
        }

        bool result = TranscoderManager::instance->removeTranscoder(name);
        if (!result)
        {
            throw runtime_error("Errors on transcoder removing is occured: " + name + ". See logs for additional info.");
        }
    }
    catch (const exception &e)
    {
        genericResponse(rep, 1, &e);
        return;
    }

    genericResponse(rep);
}

void AdminRest::transcoderStart(http::server::request_ptr &req, http::server::reply_ptr &rep)
{
    logger(level::debug) << "REST: transcoder start" << endl;
    try
    {
        requestValidate(req, rep, "POST", CONTENT_TYPE_JSON);

        // take string data from request
        string data(reinterpret_cast<const char *>(req->get_data().data()), req->get_data().size());

        stringstream ss;
        boost::property_tree::ptree root;
        ss << data;

        boost::property_tree::json_parser::read_json(ss, root);

        boost::property_tree::ptree transcoderNode;
        transcoderNode = root.get_child("transcoder");

        string name = transcoderNode.get<string>("name");
        boost::algorithm::trim(name);
        if (name.empty())
        {
            throw logic_error("Empty transcoder name");
        }

        TranscoderPtr transcoder = TranscoderManager::instance->getTranscoder(name);
        if (!transcoder)
        {
            throw logic_error("Transcoder '" + name + "' does not exists");
        }

        ScopedTryLock<Transcoder> lock(transcoder.get(), true, "Can't lock transcoder to modify");
        if (!transcoder->start())
        {
            throw logic_error("Can't start transcoder. See server logs for additional info. Run flag: " + (int)transcoder->isRunning());
        }
    }
    catch (const exception &e)
    {
        genericResponse(rep, 1, &e);
        return;
    }

    genericResponse(rep);
}

void AdminRest::transcoderStop(http::server::request_ptr &req, http::server::reply_ptr &rep)
{
    logger(level::debug) << "REST: transcoder stop" << endl;
    try
    {
        requestValidate(req, rep, "POST", CONTENT_TYPE_JSON);

        // take string data from request
        string data(reinterpret_cast<const char *>(req->get_data().data()), req->get_data().size());

        stringstream ss;
        boost::property_tree::ptree root;
        ss << data;

        boost::property_tree::json_parser::read_json(ss, root);

        boost::property_tree::ptree transcoderNode;
        transcoderNode = root.get_child("transcoder");

        string name = transcoderNode.get<string>("name");
        boost::algorithm::trim(name);
        if (name.empty())
        {
            throw logic_error("Empty transcoder name");
        }

        TranscoderPtr transcoder = TranscoderManager::instance->getTranscoder(name);
        if (!transcoder)
        {
            throw logic_error("Transcoder '" + name + "' does not exists");
        }

        ScopedTryLock<Transcoder> lock(transcoder.get(), true, "Can't lock transcoder to modify");
        if (!transcoder->stop())
        {
            throw logic_error("Can't stop transcoder. See server logs for additional info. Run flag: " + (int)transcoder->isRunning());
        }
    }
    catch (const exception &e)
    {
        genericResponse(rep, 1, &e);
        return;
    }

    genericResponse(rep);
}

void AdminRest::transcoderRestart(http::server::request_ptr &req, http::server::reply_ptr &rep)
{
    logger(level::debug) << "REST: transcoder restart" << endl;
    try
    {
        requestValidate(req, rep, "POST", CONTENT_TYPE_JSON);

        // take string data from request
        string data(reinterpret_cast<const char *>(req->get_data().data()), req->get_data().size());

        stringstream ss;
        boost::property_tree::ptree root;
        ss << data;

        boost::property_tree::json_parser::read_json(ss, root);

        boost::property_tree::ptree transcoderNode;
        transcoderNode = root.get_child("transcoder");

        string name = transcoderNode.get<string>("name");
        boost::algorithm::trim(name);
        if (name.empty())
        {
            throw logic_error("Empty transcoder name");
        }

        TranscoderPtr transcoder = TranscoderManager::instance->getTranscoder(name);
        if (!transcoder)
        {
            throw logic_error("Transcoder '" + name + "' does not exists");
        }

        ScopedTryLock<Transcoder> lock(transcoder.get(), true, "Can't lock transcoder to modify");
        if (!transcoder->restart())
        {
            throw logic_error("Can't restart transcoder. See server logs for additional info. Run flag: " + (int)transcoder->isRunning());
        }
    }
    catch (const exception &e)
    {
        genericResponse(rep, 1, &e);
        return;
    }

    genericResponse(rep);
}

void AdminRest::transcoderRestartAll(http::server::request_ptr &req, http::server::reply_ptr &rep)
{
    logger(level::debug) << "REST: transcoder restart all" << endl;
    try
    {
        requestValidate(req, rep, "GET", "");
        TranscoderManager::instance->restartAll();
    }
    catch (const exception &e)
    {
        genericResponse(rep, 1, &e);
        return;
    }

    genericResponse(rep);
}

void AdminRest::transcoderSave(http::server::request_ptr &req, http::server::reply_ptr &rep)
{
    logger(level::debug) << "REST: transcoder save" << endl;
    try
    {
        requestValidate(req, rep, "POST", CONTENT_TYPE_JSON);

        // take string data from request
        string data(reinterpret_cast<const char *>(req->get_data().data()), req->get_data().size());

        stringstream ss;
        boost::property_tree::ptree root;
        ss << data;

        boost::property_tree::json_parser::read_json(ss, root);

        boost::property_tree::ptree transcoderNode;
        transcoderNode = root.get_child("transcoder");

        string name = transcoderNode.get<string>("name");
        boost::algorithm::trim(name);
        if (name.empty())
        {
            throw logic_error("Empty transcoder name");
        }

        if (!TranscoderManager::instance->save(name))
        {
            throw logic_error("Error on transcoders saving is occured. See server logs for additional info.");
        }
    }
    catch (const exception &e)
    {
        genericResponse(rep, 1, &e);
        return;
    }

    genericResponse(rep);
}

void AdminRest::transcoderSaveAll(http::server::request_ptr &req, http::server::reply_ptr &rep)
{
    logger(level::debug) << "REST: transcoder save all" << endl;
    try
    {
        requestValidate(req, rep, "GET", "");
        if (!TranscoderManager::instance->save())
        {
            throw logic_error("Error on transcoders saving is occured. See server logs for additional info.");
        }
    }
    catch (const exception &e)
    {
        genericResponse(rep, 1, &e);
        return;
    }

    genericResponse(rep);

}


}}} // ::transcoder::admin::rest
