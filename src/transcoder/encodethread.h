#ifndef ENCODETHREAD_H
#define ENCODETHREAD_H

#include <iostream>
#include <vector>

#include <boost/smart_ptr.hpp>
#include <boost/thread.hpp>

#include "boost/atomic.hpp"
#include "thread.hpp"
#include "concurentqueue.hpp"

#include "av/ffmpeg.h"
#include "av/container.h"
#include "av/packet.h"
#include "av/frame.h"
#include "av/videoresampler.h"

#include "consumer.h"
#include "producer.h"
#include "baseencoder.h"
#include "formatdescription.h"
#include "concurentqueue.hpp"
#include "util/mutex.hpp"

namespace transcoder {

class EncodeThread : public Thread<EncodeThread>, public Consumer, public Producer
{

public:
    explicit EncodeThread(const FormatDescription &description);
    virtual ~EncodeThread();

    virtual void run();
    virtual void notify(const Producer *producer);

    /**
     * @brief open Open video codec and return result
     * @return true if codec opened sucsessfuly, false - if problem occured
     */
    bool open();
    void stop();

    av::PacketPtr            lastEncodedPacket() const;

    const FormatDescription& getFormatDescription() const { return encoder.getFormatDescription(); }

    void setQueueSize(uint32_t queueSize);
    uint32_t getQueueSize() const;

private:
    void pushData(const av::PacketPtr &packet);

    void encodeSync(const av::FramePtr& frame);
    void encodeAsync(const av::FramePtr& frame);

private:
    boost::atomic_bool              runFlag;

    encode::FrameEncoder            encoder;
    FormatDescription               description;

    mutable boost::shared_mutex     packetMutex;
    av::PacketPtr                   lastPacket;

    int64_t                         previousPts;

    ConcurentQueue<av::FramePtr>    frameQueue;

    // Temporary
    time_t prevTime;
};

DECLARE_PTR_TYPE(EncodeThread);

}

#endif // ENCODETHREAD_H
