#include <iostream>

#include "dbpionusermanager.h"
#include "util/dbutils.h"

namespace pion {
namespace net {


DbPionUserManager::DbPionUserManager(soci::connection_pool *pool)
    : dbpool(pool)
{
}


const pair<bool, string> &DbPionUserManager::getDatabaseError() const
{
    return databaseError;
}

void DbPionUserManager::cleanError()
{
    databaseError = make_pair(false, string());
}

void DbPionUserManager::setError(const string &text)
{
    std::cerr << "Database error: " << text << '\n';
    databaseError = make_pair(true, text);
}

bool DbPionUserManager::empty()
{
    bool result = true;

    if (!dbpool)
    {
        return true;
    }

    try
    {
        int count = 0;
        soci::session sql(*dbpool);
        checkDbSession(sql);

        sql << "SELECT COUNT(*) FROM users", soci::into(count);
        if (sql.got_data() && count > 0)
        {
            result = false;
        }
    }
    catch (const exception &e)
    {
        setError(e.what());
    }

    return result;
}

PionUserPtr DbPionUserManager::getUser(const string &username)
{
    if (!dbpool)
    {
        return PionUserPtr();
    }

    try
    {
        soci::session sql(*dbpool);
        checkDbSession(sql);

        string user;
        string password;
        sql << "SELECT name, password FROM users WHERE name = :user",
                soci::into(user),
                soci::into(password),
                soci::use(username, "user");
        if (sql.got_data())
        {
            return PionUserPtr(new PionUser(user, password));
        }
    }
    catch (const exception &e)
    {
        setError(e.what());
    }

    return PionUserPtr();

}


PionUserPtr DbPionUserManager::getUser(const string &username, const string &password)
{
    if (!dbpool)
    {
        return PionUserPtr();
    }

    try
    {
        soci::session sql(*dbpool);
        checkDbSession(sql);

        string user;
        string pass;
        sql << "SELECT name, password FROM users WHERE name = :user AND password = :password",
                soci::into(user),
                soci::into(pass),
                soci::use(username, "user"),
                soci::use(password, "password");
        if (sql.got_data())
        {
            return PionUserPtr(new PionUser(user, pass));
        }
    }
    catch (const exception &e)
    {
        setError(e.what());
    }

    return PionUserPtr();
}


}} // ::pion::net

