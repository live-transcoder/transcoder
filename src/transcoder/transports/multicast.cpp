#include <sys/stat.h>
#include <fcntl.h>

#include "encodethread.h"
#include "applicationcontext.h"
#include "log/logstream.h"

#include "multicast.h"
#include "muxerproxy.h"

using namespace logstream;

namespace transcoder {
namespace transport {

struct MulticastWriter : public Writer
{
    MulticastWriter(const std::string& destination)
        : destination(destination)
    {
        logger(level::debug) << "Begin multicast writing: " << destination << endl;
        this->open();
        ioService.run();
    }


    ~MulticastWriter()
    {
        logger(level::debug) << "End multicast writing: " << destination << endl;
        this->close();
    }


    int open()
    {
        // Like: [ip][@ip[:port]]

        std::string multicastParams = destination;

        std::string bindAddress = "0.0.0.0";
        std::string multicastAddressStr;
        int         multicastPort = 1234;
        int         ttl           = 1;

        size_t dogPlace =  multicastParams.find('@');
        size_t colonPlace = multicastParams.find(':');

        if (dogPlace == -1)
        {
            //throw std::runtime_error("Error: incorrect multicast address, must be: [bind_address][@multicast_address[:port]]");
            return -1;
        }

        if (dogPlace > 0 && dogPlace != std::string::npos)
        {
            bindAddress = multicastParams.substr(0, dogPlace);
        }

        if (colonPlace != std::string::npos)
        {
            multicastAddressStr = multicastParams.substr(dogPlace + 1, colonPlace - dogPlace - 1);
            multicastPort       = atoi(multicastParams.substr(colonPlace + 1).c_str());
        }
        else
        {
            multicastAddressStr = multicastParams.substr(dogPlace + 1);
        }

        boost::asio::ip::address multicastAddress =
                boost::asio::ip::address::from_string(multicastAddressStr);

        udpEndpoint = boost::asio::ip::udp::endpoint(multicastAddress, multicastPort);
        udpSocket   = new boost::asio::ip::udp::socket(ioService, udpEndpoint.protocol());;

        // reuse address
        boost::asio::socket_base::reuse_address reuseAddr(true);
        udpSocket->set_option(reuseAddr);

        // make socket non-blocked
        //udpSocket->non_blocking(true);

        // Set receive buffer size to ~2.0MB
        boost::asio::socket_base::receive_buffer_size receiveBufferSize((2000000 / 1316) * 1316);
        udpSocket->set_option(receiveBufferSize);

        // Set send buffer size to ~4.0MB
        //boost::asio::socket_base::send_buffer_size    sendBufferSize((sendingBufferSize * 1024 * 1024 / 1316) * 1316);
        //udpSocket->set_option(sendBufferSize);

#ifndef WIN32
        // Set multicast TTL
        boost::asio::ip::multicast::hops multicastTtl(ttl);
        udpSocket->set_option(multicastTtl);
#endif

        // Set multicast outbound interface
        boost::asio::ip::address_v4 localInterface =
                boost::asio::ip::address_v4::from_string(bindAddress);
        boost::asio::ip::multicast::outbound_interface outboundInterface(localInterface);
        udpSocket->set_option(outboundInterface);

        std::cout << "Bind: " << bindAddress << "@" << multicastAddressStr << ":" << multicastPort << std::endl;
    }


    int close()
    {
        udpSocket->close();
        return 0;
    }


    int operator ()(uint8_t *buf, int size)
    {
        boost::system::error_code code;
        int writen;
        writen = udpSocket->send_to(boost::asio::buffer(buf, size),
                                    udpEndpoint,
                                    0,
                                    code);

        std::clog << "MCast write: " << writen << std::endl;

        if (code)
        {
            return -1;
        }

        return writen;
    }


    const char *name() const
    {
        return destination.c_str();
    }


    std::string destination;

    boost::asio::io_service         ioService;
    boost::asio::ip::udp::endpoint  udpEndpoint;
    boost::asio::ip::udp::socket   *udpSocket;
};

DECLARE_PTR_TYPE(MulticastWriter);

BaseTransport::ResourceRegistrationStatus
Multicast::registerResource(const string &resource,
                            const ProducerPtr &producer,
                            const BaseTransport::Options &options)
{
    boost::mutex::scoped_lock lock(muxersMutex);

    std::string multicastResource = "udp://" + resource;


    if (multicastResource.find('?') == std::string::npos)
    {
        multicastResource += "?pkt_size=1316";
    }
    else if (multicastResource.find("pkt_size=") == std::string::npos)
    {
        // set MPEGTS compatible buffer size
        multicastResource += "&pkt_size=1316";
    }

    MuxerProxyPtr        muxer(new MuxerProxy(multicastResource));

    // Set send buffer size
    muxer->setQueueSize(ApplicationContext::instance->sendBufferSize);
    muxer->setTimeSynchronizeWrite(true);
    muxer->setThreadName(resource + "udp");

    muxers[resource] = muxer;

    return std::make_pair(true, muxer);
}

bool Multicast::unregisterResource(const string &resource)
{
    boost::mutex::scoped_lock lock(muxersMutex);
    if (muxers.find(resource) != muxers.end())
    {
        muxers.erase(resource);
    }
}

const string Multicast::getTransportName()
{
    return "multicast";
}

} // namespace transport
} // namespace transcoder
