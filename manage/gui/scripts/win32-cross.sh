#!/bin/bash

source project.conf

cd ..
tmpf1=`mktemp`
tmpf2=`mktemp`
cat $project.pro | grep '^CONFIG' | tee $tmpf1 | sed 's/debug/release/' > $tmpf2

line1=`cat $tmpf1`
line2=`cat $tmpf2`

cat $project.pro | sed "s/$line1/$line2/" > $project-win32.pro
mv $project-win32.pro $project.pro

qmake -spec win32-g++-cross $project.pro
#i486-mingw32-qmake $project.pro


echo "*** Now you can type 'cd .. && make' for build"
