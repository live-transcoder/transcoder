#ifndef ADDEDITDIALOG_H
#define ADDEDITDIALOG_H

#include "ui_addeditdialog.h"
#include "formatdata.h"

class AddEditDialog : public QDialog, private Ui::AddEditDialog
{
    Q_OBJECT
    
public:
    explicit AddEditDialog(QWidget *parent = 0);
    
    void setData(const TranscoderData &data);
    TranscoderData getData() const;

    void setNameFieldEnabled(bool isEnabled);

protected:
    void changeEvent(QEvent *e);

    TranscoderData data;
    QList<FormatData> formats;

private:
    void updateRow(int row, const FormatData &format);

private slots:
    void on_destTableWidget_currentCellChanged(int currentRow, int currentColumn, int previousRow, int previousColumn);
    void on_updatePushButton_clicked();
    void on_addPushButton_clicked();
    void on_deletePushButton_clicked();
    void on_nameLineEdit_textEdited(const QString &arg1);
};

#endif // ADDEDITDIALOG_H
