/** =============================================================================================
  
    This file is a part of "%ProjectName%" project
    http://htrd.su
    
    @date   2012-4-7
    @brief
    
    Copyright (C) 2002-2012 by Alexander 'hatred' Drozdov <adrozdoff@gmail.com>
    
    This program is free software; you can redistribute it and/or modify
    it under the terms of the version 2 of GNU General Public License as
    published by the Free Software Foundation.
    
    For more information see LICENSE and LICENSE.ru files
    
   ============================================================================================== */

#include <stdint.h>

#include <cstdlib>
#include <cstddef>
#include <iostream>

#include <boost/thread.hpp>
#include <boost/date_time.hpp>
//#include <boost/date_time/posix_time/posix_time.hpp>

#include "decoderthread.h"
#include "../util/utils.h"

#include "av/codec.h"
#include "av/streamcoder.h"
#include "av/stream.h"
#include "av/audiosamples.h"
#include "av/container.h"
#include "av/avutils.h"
#include "log/logstream.h"


using namespace logstream;


DecodeThread::DecodeThread()
    : runFlag(false),
      activeFlag(false),
      encodersWaitTime(-1l)
{
}

DecodeThread::~DecodeThread()
{
}


static int handle_jpeg(enum PixelFormat &format)
{
    switch (format)
    {
        case PIX_FMT_YUVJ420P:
            format = PIX_FMT_YUV420P;
            return 1;
        case PIX_FMT_YUVJ422P:
            format = PIX_FMT_YUV422P;
            return 1;
        case PIX_FMT_YUVJ444P:
            format = PIX_FMT_YUV444P;
            return 1;
        case PIX_FMT_YUVJ440P:
            format = PIX_FMT_YUV440P;
            return 1;
        default:
            return 0;
    }
}


/**
 * Can be used for built-in types only (like int, char, long and so on)
 */
template <typename T, T initialValue = 0>
struct BuiltInTypeWrapper
{
    BuiltInTypeWrapper()
        : value(initialValue)
    {}
    BuiltInTypeWrapper(T v)
        : value(v)
    {}

    bool isValid() const
    {
        return (value != initialValue);
    }

    void reset()
    {
        value = initialValue;
    }

    operator T&()
    {
        return value;
    }

    operator T const&() const
    {
        return value;
    }

    BuiltInTypeWrapper& operator=(T v)
    {
        value = v;
        return *this;
    }

    T value;
};

/**
 * Simple wrapper for time stamp values
 */
typedef BuiltInTypeWrapper<int64_t, AV_NOPTS_VALUE> TsWrapper;


void DecodeThread::run()
{
    runFlag = true;
    while (runFlag)
    {
        ContainerPtr inp(new Container());

        inp->setReadingTimeout(15); // set reading timeout to 60 sec
        map<int, int64_t> lastPts;

        activeFlag = false;

        ContainerFormatPtr forcedInputFormat = ContainerFormatPtr();
        if (!inputFormatName.empty())
        {
            forcedInputFormat = ContainerFormatPtr(new ContainerFormat());
            forcedInputFormat->setInputFormat(inputFormatName.c_str());
        }


        if (!inp->openInput(streamUri.c_str(), forcedInputFormat))
        {
            logger(level::warning) << "Can't open input stream: " << streamUri << std::endl;
            sleep(5);
            continue;
        }

        //
        // Stream mapping helper
        //
        map<int, int> mappingFrom;

        vector<StreamPtr>      streams;
        vector<StreamCoderPtr> decoders;
        for (int i = 0, j = 0; i < inp->getStreamsCount(); ++i)
        {
            StreamPtr st = inp->getStream(i);
            if (st->getMediaType() == AVMEDIA_TYPE_VIDEO ||
                st->getMediaType() == AVMEDIA_TYPE_AUDIO)
            {
                streams.push_back(st);

                // Stream mapping
                mappingFrom[st->getIndex()] = j++;

                StreamCoderPtr decoder(new StreamCoder(st));
                if (!decoder || !decoder->open())
                {
                    logger(level::warning) << "Can't find or open codec for stream: " << st->getIndex() << ", " << st->getAVStream()->codec->codec_type << ", " << streamUri << std::endl;
                    break;
                }

                decoders.push_back(decoder);
                lastPts[i] = AV_NOPTS_VALUE;
            }
        }

        if (streams.empty())
        {
            logger(level::warning) << "Can't find any video/audio streams: " << streamUri << std::endl;
            sleep(5);
            continue;
        }

        if (streams.size() != decoders.size())
        {
            // Do not print log message because it already printed.
            sleep(5);
            continue;
        }

        //
        // Fill format description
        //
        formatDescription.reset();
        formatDescription.uri    = streamUri;
        formatDescription.format = inp->getFormat()->getInputFormat()->name;

        for (int i = 0; i < streams.size(); ++i)
        {
            StreamPtr      &stream  = streams[i];
            StreamCoderPtr &decoder = decoders[i];

            StreamDescription desc;

            desc.codec      = decoder->getAVCodecContext()->codec->name;
            desc.mediaType  = decoder->getAVCodecContext()->codec_type;
            desc.timeBase   = decoder->getTimeBase();
            desc.bitRate    = decoder->getBitRate();
            desc.bitRateMin = decoder->getBitRateRange().first;
            desc.bitRateMax = decoder->getBitRateRange().second;

            // Mapping
            desc.mapFromIndex = stream->getStreamIndex();
            desc.mapToIndex   = i;

            if (desc.bitRateMin == 0 || desc.bitRateMax == 0)
            {
                desc.bitRateMin = desc.bitRateMax = desc.bitRate;
            }

            switch (stream->getAVStream()->codec->codec_type)
            {
                case AVMEDIA_TYPE_VIDEO:
                    desc.width                 = decoder->getWidth();
                    desc.height                = decoder->getHeight();
                    desc.videoGopSize          = decoder->getGopSize();
                    desc.videoBitRateTolerance = decoder->getBitRateTolerance();
                    desc.videoStrict           = decoder->getStrict();
                    desc.pixFormat             = decoder->getPixelFormat();
                    desc.sampleAspectRatio     = decoder->getAVCodecContext()->sample_aspect_ratio;
                    desc.videoFrameRate        = stream->getFrameRate();

                    break;

                case AVMEDIA_TYPE_AUDIO:
                    desc.audioChannels         = decoder->getChannels();
                    desc.audioChannelsLayout   = decoder->getChannelLayout();
                    desc.audioSampleRate       = decoder->getSampleRate();
                    desc.audioSampleFormat     = decoder->getSampleFormat();

                    break;


                case AVMEDIA_TYPE_DATA:
                    break;
                case AVMEDIA_TYPE_SUBTITLE:
                    break;
                case AVMEDIA_TYPE_ATTACHMENT:
                    break;
                case AVMEDIA_TYPE_NB:
                    break;
                case AVMEDIA_TYPE_UNKNOWN:
                    break;
            }

            formatDescription.streams.push_back(desc);
        }

        std::cerr << "Streams: " << formatDescription.streams.size() << std::endl;
        std::cerr << "Codec:   " << formatDescription.streams[0].codec << std::endl;


        PacketPtr pkt;
        pkt = PacketPtr(new Packet());

        map<int, pair<TsWrapper, TsWrapper> > tsFix;
        map<int, TsWrapper>                   lastPktPts;

        FrameTimeSynchronizer frameSync;
        while (inp->readNextPacket(pkt) >= 0 && runFlag)
        {
            activeFlag = true;
            if (mappingFrom.find(pkt->getStreamIndex()) == mappingFrom.end())
            {
                continue;
            }

            int index = mappingFrom[pkt->getStreamIndex()];
            StreamCoderPtr &decoder = decoders[index];

            int stat = -1;
            FramePtr frame;

            //clog << "Packet time (0): 0:" << pkt->getStreamIndex() << " - " << pkt->getPts() << ", " << pkt->getDts() << endl;

            //
            // Fix packet pts
            // TODO we can do it with filter 'pts'
            //

            if (pkt->getPts() == AV_NOPTS_VALUE)
            {
                pkt->setPts(pkt->getDts());
            }

            if (pkt->getPts() != AV_NOPTS_VALUE)
            {
                if (tsFix[pkt->getStreamIndex()].first == AV_NOPTS_VALUE)
                    tsFix[pkt->getStreamIndex()].first = pkt->getPts();

                pkt->setPts(pkt->getPts() - tsFix[pkt->getStreamIndex()].first);
            }

            if (pkt->getDts() != AV_NOPTS_VALUE)
            {
                if (tsFix[pkt->getStreamIndex()].second == AV_NOPTS_VALUE)
                    tsFix[pkt->getStreamIndex()].second = pkt->getDts();

                pkt->setDts(pkt->getDts() - tsFix[pkt->getStreamIndex()].second);
            }

            if (pkt->getPts() < 0)
            {
                int idx = pkt->getStreamIndex();
                if (lastPktPts[idx].isValid() && lastPktPts[idx].value < 0)
                {
                    break;
                }
            }

            lastPktPts[pkt->getStreamIndex()] = pkt->getPts();

            //clog << "Input packet time " << pkt->getStreamIndex() << ", " << pkt->getTimeBase().getDouble() * pkt->getPts() << endl;
            //clog << "Packet time (1): 0:" << pkt->getStreamIndex() << " - " << pkt->getPts() << ", " << pkt->getDts() << endl;

            switch (decoder->getCodecType())
            {
                case AVMEDIA_TYPE_VIDEO:
                {
                    frame = VideoFramePtr(new VideoFrame());
                    stat  = decoder->decodeVideo(frame, pkt);

                    VideoFramePtr videoFrame = boost::dynamic_pointer_cast<VideoFrame>(frame);

                    // Hack? Some encoders does not support B-frames, so override type to I-frame (eg. key-frame)
                    if (videoFrame && videoFrame->getPictureType() != AV_PICTURE_TYPE_I)
                    {
                        videoFrame->setPictureType(AV_PICTURE_TYPE_I);
                    }

                    break;
                }

                case AVMEDIA_TYPE_AUDIO:
                {
                    frame = AudioSamplesPtr(new AudioSamples());
                    stat  = decoder->decodeAudio(frame, pkt);

                    //std::clog << "SAMPLES:  " << frame->getAVFrame()->nb_samples << std::endl;
                    //std::clog << "FRAME SZ: " << decoder->getAudioFrameSize() << std::endl;

                    break;
                }

                case AVMEDIA_TYPE_DATA:
                    break;
                case AVMEDIA_TYPE_SUBTITLE:
                    break;
                case AVMEDIA_TYPE_ATTACHMENT:
                    break;
                case AVMEDIA_TYPE_NB:
                    break;
                case AVMEDIA_TYPE_UNKNOWN:
                    break;
            }


            if (stat >= 0 && frame && frame->isComplete() && frame->isValid())
            {
                PrettyScopedTimer3(streamUri, false);

                if (decoder->getCodecType() == AVMEDIA_TYPE_VIDEO)
                {
                    if (lastPts[frame->getStreamIndex()] != AV_NOPTS_VALUE &&
                        lastPts[frame->getStreamIndex()] == frame->getPts())
                    {
                        // Skip duplicated pts
                        logger(level::warning)
                                << streamUri << ": st = " << index
                                << ", skip duplicate pts = " << frame->getPts()
                                << endl;
                        continue;
                    }

                    if (lastPts[frame->getStreamIndex()] != AV_NOPTS_VALUE &&
                        lastPts[frame->getStreamIndex()] >  frame->getPts())
                    {                       
                        int64_t pts = frame->getPts();
                        logger(level::warning)
                                << streamUri << ": "
                                << "Wrong PTS: st = " << index
                                << ", pkt_pts = " << pkt->getPts()
                                << ", frame_pts = " << pts
                                << ", last_pts = " << lastPts[frame->getStreamIndex()]
                                << ", best effort ts = " << frame->getBestEffortTimestamp()
                                << endl;
                        // Skip frame with same PTS
                        continue;
                    }

                    lastPts[frame->getStreamIndex()] = frame->getPts();
                }

                lastFrameTime = boost::get_xtime(boost::get_system_time());

                pushFrame(frame);

                int64_t s = frameSync.doTimeSync(frame, true);
                //clog << "Sync time, st = " << index << ", time (ms) = " << s << endl;
            }
            else
            {
                std::clog << "INVALID FRAME: "
                          << "st: " << index
                          << ", stat: " << stat
                          << ", isComplete: " << (frame ? (frame->isComplete() ? "yes" : "no" ) : "unknown" )
                          << ", isValid: " << (frame ? (frame->isValid() ? "yes" : "no" ) : "unknown" )
                          << std::endl;
            }
        }
    }

    activeFlag = false;
}

bool DecodeThread::isRunning()
{
    return runFlag;
}

bool DecodeThread::isActive()
{
    return activeFlag;
}

void DecodeThread::stop()
{
    runFlag = false;
}

const std::string &DecodeThread::getStreamUri()
{
    return streamUri;
}

void DecodeThread::setStreamUri(const std::string &uri)
{
    streamUri = uri;
}

void DecodeThread::forceInputFormat(const string &format)
{
    inputFormatName = format;
}


FramePtr DecodeThread::getLastFrame() const
{
    boost::shared_lock<boost::shared_mutex> lock(frameMutex);
    return lastFrame;
}

const boost::xtime &DecodeThread::getLastFrameTime() const
{
    return lastFrameTime;
}

void DecodeThread::setEncodersWaitTime(int64_t time)
{
    //encodersWaitTime = boost::atomic_int64_t(time);
    encodersWaitTime.store(time);
}

int64_t DecodeThread::getEncodersWaitTime() const
{
    return encodersWaitTime;
}

void DecodeThread::pushFrame(const FramePtr &frame)
{
    {
        boost::unique_lock<boost::shared_mutex> lock(frameMutex);
        lastFrame = frame;
    }

    // уведомления подписчикам
    notifyAll();
    waitSubscribers(encodersWaitTime);
}
