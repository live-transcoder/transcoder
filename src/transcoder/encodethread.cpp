#include <algorithm>
#include <iterator>
#include <vector>

#include <boost/date_time.hpp>

#include "encodethread.h"
#include "decoderthread.h"
#include "../util/utils.h"
#include "log/logstream.h"

#include "av/rational.h"

using namespace logstream;
using namespace std;
using namespace av;
using namespace transcoder::encode;

namespace transcoder {


EncodeThread::EncodeThread(const FormatDescription &description)
    : runFlag(false),
      description(description),
      previousPts(AV_NOPTS_VALUE)
{
    //encoder = BaseEncoder(description);
    frameQueue.setMaxQueueSize(5);

    // Temporary
    prevTime = time(0);
}

EncodeThread::~EncodeThread()
{
    stop();
    if (!timed_join(boost::posix_time::seconds(3)))
    {
        kill();
        join();
    }
}


void EncodeThread::run()
{
    runFlag = true;
    while (runFlag)
    {
        FramePtr frame;
        if (frameQueue.waitAndPop(frame))
        {
            encodeSync(frame);
        }
    }
}


static inline
bool IsToStreamIndexLess(const StreamDescription& first, const StreamDescription& second)
{
    return first.mapToIndex < second.mapToIndex;
}

static inline
bool IsFromStreamIndexLess(const StreamDescription& first, const StreamDescription& second)
{
    return first.mapFromIndex < second.mapFromIndex;
}


struct FindByOriginalStreamId
{
    FindByOriginalStreamId(int streamId)
        : streamId(streamId)
    {}

    bool operator()(const StreamDescription& stream)
    {
        return stream.mapFromIndex == streamId;
    }

    int streamId;
};


void EncodeThread::notify(const Producer *producer)
{
    const DecodeThread *source = dynamic_cast<const DecodeThread*>(producer);
    if (source == 0)
    {
        return;
    }

    PrettyScopedTimer3(encoder.getFormatDescription().format + ":" + encoder.getFormatDescription().format, false);

    if (!encoder.isValid())
    {
        const FormatDescription &decoderDesc = source->getFormatDescription();
        FormatDescription desc = description;

        ////
        // Merge format and encoders settings, make stream mapping
        ////

        // check user-given streams for present in input
        for (int i = 0; i < desc.streams.size(); ++i)
        {
            bool isFound = false;
            for (int j = 0; j < decoderDesc.streams.size(); ++j)
            {
                if (desc.streams[i].mapFromIndex == decoderDesc.streams[j].mapFromIndex)
                {
                    isFound = true;
                    break;
                }
            }

            // if not found - remove it
            if (!isFound)
            {
                desc.streams.erase(desc.streams.begin() + i);
                --i;
            }
        }

        bool isStreamsCopiedFromInput = false;
        if (desc.streams.empty())
        {
            desc.streams = decoderDesc.streams;
            isStreamsCopiedFromInput = true;
        }

        // Sort streams by to index
        std::sort(desc.streams.begin(), desc.streams.end(), IsToStreamIndexLess);

        // Fix toIndex mappig for continious numeration from 0 to count-1
        for (int i = 0; i < desc.streams.size(); ++i)
        {
            StreamDescription &st = desc.streams[i];

            if (isStreamsCopiedFromInput)
            {
                //
                // Reset some params
                //

                // Reset codec to default for output container
                st.codec = string();
            }
            else
            {
                //
                // Fill some fields
                //

                // TODO: setup default mapping by media type
                if (st.mapFromIndex < 0)
                {
                    st.mapFromIndex = i;
                }

                // Find original stream (mapped from)
                vector<StreamDescription>::const_iterator it =
                        std::find_if(decoderDesc.streams.begin(),
                                     decoderDesc.streams.end(),
                                     FindByOriginalStreamId(st.mapFromIndex));

                // TODO: select more intelligent way for setting up media types
                if (st.mediaType == AVMEDIA_TYPE_UNKNOWN)
                {
                    st.mediaType = it->mediaType;
                }


                st.mapToIndex = i;
                if (st.codec == "copy")
                {
                    // Copy codec name from input stream
                    st.codec = it->codec;
                }

                if (st.timeBase == Rational())
                {
                    st.timeBase = it->timeBase;
                }

                if (st.bitRateMin <= 0 || st.bitRateMax <= 0 || st.bitRate <= 0)
                {
                    st.bitRate = it->bitRate;
                    st.bitRateMin = it->bitRateMin;
                    st.bitRateMax = it->bitRateMax;
                }

                switch (st.mediaType)
                {
                    case AVMEDIA_TYPE_VIDEO:
                        if (st.width == -1 || st.height == -1)
                        {
                            st.width = it->width;
                            st.height = it->height;
                        }

                        if (st.videoFrameRate == Rational())
                        {
                            st.videoFrameRate = it->videoFrameRate;
                        }

                        break;

                    case AVMEDIA_TYPE_AUDIO:
                        if (st.audioChannels <= 0)
                        {
                            st.audioChannels = it->audioChannels;
                        }

                        if (st.audioSampleRate <= 0)
                        {
                            st.audioSampleRate = it->audioSampleRate;
                        }

                        // TODO: add channels layout

                        break;

                    case AVMEDIA_TYPE_DATA:
                        break;
                    case AVMEDIA_TYPE_SUBTITLE:
                        break;
                    case AVMEDIA_TYPE_ATTACHMENT:
                        break;
                    case AVMEDIA_TYPE_NB:
                        break;
                    case AVMEDIA_TYPE_UNKNOWN:
                        break;
                }
            }
        }

        encoder = FrameEncoder(desc);
    }

    // Process frame only if at least one subscriber exists
    if (subscribersCount())
    {
        FramePtr frame = source->getLastFrame();
        encodeAsync(frame);
    }
    else
    {
        if (frameQueue.size())
        {
            frameQueue.clear();
        }
    }
}


bool EncodeThread::open()
{
    return false;
}


void EncodeThread::stop()
{
    runFlag = false;
    frameQueue.cancelWait();
}



PacketPtr EncodeThread::lastEncodedPacket() const
{
    boost::shared_lock<boost::shared_mutex> lock(packetMutex);
    return lastPacket;
}

void EncodeThread::setQueueSize(uint32_t queueSize)
{
    if (queueSize > 0)
        frameQueue.setMaxQueueSize(queueSize);
}

uint32_t EncodeThread::getQueueSize() const
{
    return frameQueue.getMaxQueueSize();
}


void EncodeThread::pushData(const PacketPtr &packet)
{
    {
        boost::unique_lock<boost::shared_mutex> lock(packetMutex);
        lastPacket = packet;
    }

    notifyAll();
}


void EncodeThread::encodeSync(const FramePtr &frame)
{
    //PrettyScopedTimer3(encoder.getFormatDescription().format, false);
    FramePtr workFrame;

    if (!frame)
    {
        return;
    }

    // Duplicate frame, skip it
    if (previousPts != AV_NOPTS_VALUE && previousPts == frame->getPts())
    {
        return;
    }

    workFrame = frame->clone();

    if (!workFrame)
    {
        return;
    }

    previousPts = workFrame->getPts();

    {
        PrettyScopedTimer3(encoder.getFormatDescription().format, false);
        encoder.encode(workFrame, boost::bind(&EncodeThread::pushData, this, _1));
    }

}

void EncodeThread::encodeAsync(const FramePtr &frame)
{
    if (!encoder.canEncode(frame))
    {
        return;
    }

    frameQueue.waitAndPush(frame);

    // Temporary
    {
        time_t t = time(0);
        if (t - prevTime > 10)
        {
            logger(level::debug) << "Encode buffer size: " << frameQueue.size()
                                 << " - "
                                 << encoder.getFormatDescription().resource
                                 << std::endl;
            prevTime = t;
        }
    }
}

} // ::transcoder
