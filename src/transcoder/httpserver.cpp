#include <boost/bind.hpp>
#include <boost/lexical_cast.hpp>

#include "uri.h"
#include "httpserver.h"
#include "rest/adminrest.h"
#include "log/logstream.h"

using namespace logstream;

namespace transcoder {
namespace transport {
namespace http {

HttpServer::HttpServer()
    : maxConnectionsCount(-1)
{
}

void HttpServer::start(uint16_t port)
{
    httpServer = server_ptr(new server("0.0.0.0", boost::lexical_cast<std::string>(port)));
    if (httpAuth)
    {
        //httpServer->
        httpServer->set_auth(httpAuth);
    }

    httpServer->set_maximum_simultaneous_connections(maxConnectionsCount);

    // Admin requests handler
    httpServer->add_resource("/admin", boost::bind(&HttpServer::adminHandler, this, _1, _2));

    // Generic bad request handler
    //httpServer->setBadRequestHandler(boost::bind(&HttpServer::badRequestHandler, this, _1, _2));

    // Error handler
    //httpServer->setServerErrorHandler(boost::bind(&HttpServer::errorHandler, this, _1, _2, _3));
    httpServer->start();

    adminConnections    = 0;
    maxAdminConnections = 10;

    logger(level::info) << "Http Server start on port: " << port << endl;
}


void HttpServer::stop()
{
    if (httpServer.get() != 0)
    {
        httpServer->stop();
    }
}

void HttpServer::setAuthentication(const auth_ptr &auth)
{
    httpAuth = auth;
    if (httpServer)
    {
        httpServer->set_auth(httpAuth);
    }
}

void HttpServer::setMaxAdminConnections(int count)
{
    maxAdminConnections = count;
}


void HttpServer::setMaxConnections(int connectionsCount)
{
    maxConnectionsCount = connectionsCount;
    if (httpServer.get())
    {
        httpServer->set_maximum_simultaneous_connections(connectionsCount);
    }
}


int HttpServer::getMaxConnections()
{
    if (httpServer.get())
    {
        return httpServer->get_maximum_simultaneous_connections();
    }

    return maxConnectionsCount;
}

void HttpServer::addHandler(const string &resource, const request_handler &handler)
{
    httpServer->add_resource(resource, handler);
}

void HttpServer::removeHandler(const string &resource)
{
    httpServer->remove_resource(resource);
}


void HttpServer::adminHandler(request_ptr &req, reply_ptr &rep)
{
    logger(level::debug) << "====> admin handler start" << endl;

    {
        boost::lock_guard<Mutex> lock(adminConnectionsMutex);
        ++adminConnections;
        if (adminConnections >= maxAdminConnections)
        {
            string error = "{\"error_code\" : 1, \"error_text\" : \"Maximum admin connections occured\"}";
            boost::system::error_code ec;

            rep->set_header(header::HEADER_CONTENT_TYPE, "application/json; charset=UTF-8");
            rep->set_content(error);
            rep->send(ec);

            --adminConnections;
            return;
        }
    }

    admin::rest::AdminRest adminHandler;
    if (!adminHandler.handle(req, rep))
    {
        server::handle_not_found(req, rep);
    }

    {
        boost::lock_guard<Mutex> lock(adminConnectionsMutex);
        --adminConnections;
    }

    logger(level::debug) << "====> admin handler finished" << endl;
}

void HttpServer::badRequestHandler(request_ptr &req, reply_ptr &rep)
{
}


void HttpServer::errorHandler(request_ptr &req, reply_ptr &rep, const string &message)
{
    logger(level::warning) << "Handler error: " << message << endl;
}



}}} // transcoder::transport::http
