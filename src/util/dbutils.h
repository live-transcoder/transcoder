#ifndef DBUTILS_H
#define DBUTILS_H

#include <soci/soci.h>

// Check sql connection and try reconnect if it fail
void checkDbSession(soci::session &sql);

#endif // DBUTILS_H
