#include "transport.h"
#include "log/logstream.h"

namespace transcoder {
namespace transport {

using namespace logstream;

boost::mutex BaseTransport::transportsMutex;
std::set<BaseTransportPtr> BaseTransport::transports;

struct FindTransportByName
{
    FindTransportByName(const std::string& name)
        : name(name)
    {}

    bool operator()(const BaseTransportPtr& transport)
    {
        if (transport)
        {
            return (transport->getTransportName() == name);
        }

        return false;
    }

    std::string name;
};


static void muxerMonitorThread(MuxerWPtr wmuxer, ProducerWPtr wproducer)
{
    MuxerPtr muxer = wmuxer.lock();
    if (!muxer)
        return;

    bool joined;
    do
    {
        joined = muxer->timed_join(boost::posix_time::milliseconds(1000));
    }
    while (!wproducer.expired() && !joined);

    if (!joined)
    {
        muxer->stop();
        muxer->join();
    }

    ProducerPtr producer = wproducer.lock();
    if (producer)
    {
        producer->unsubscribe(muxer);
    }
}

bool BaseTransport::registerResource(const string &transport,
                                     const std::string &resource,
                                     const ProducerPtr &producer,
                                     const Options     &options)
{
    std::set<BaseTransportPtr>::iterator it;

    {
        boost::mutex::scoped_lock lock(transportsMutex);
        it = std::find_if(transports.begin(), transports.end(), FindTransportByName(transport));
    }

    if (it == transports.end())
    {
        logger(level::error) << "Transport not found: " << transport << std::endl;
        return false;
    }

    logger(level::debug) << "Transport found: " << (*it)->getTransportName() << std::endl;

    ResourceRegistrationStatus regStatus = (*it)->registerResource(resource, producer, options);

    if (regStatus.first && regStatus.second)
    {
        MuxerPtr muxer = regStatus.second;
        // Start muxer and subscribe it
        muxer->start();
        producer->subscribe(muxer);

        // Create muxer monitor
        boost::thread monitor(boost::bind(muxerMonitorThread, MuxerWPtr(muxer), ProducerWPtr(producer)));
        monitor.detach();

        logger(level::info) << transport << ":" << "resource registered: " << resource << std::endl;
    }

    return regStatus.first;
}


bool BaseTransport::unregisterResource(const string &transport, const string &resource)
{
    std::set<BaseTransportPtr>::iterator it;
    {
        boost::mutex::scoped_lock lock(transportsMutex);
        it = std::find_if(transports.begin(), transports.end(), FindTransportByName(transport));
    }

    if (it == transports.end())
    {
        logger(level::error) << "Transport not found: " << transport << std::endl;
        return false;
    }

    logger(level::debug) << "Transport found: " << (*it)->getTransportName() << std::endl;

    return (*it)->unregisterResource(resource);
}

BaseTransport::~BaseTransport()
{
}

void BaseTransport::init(const BaseTransport::Options &/*options*/)
{
}

void BaseTransport::deinit()
{
}

bool BaseTransport::registerTransport(BaseTransportPtr &transport)
{
    boost::mutex::scoped_lock lock(transportsMutex);
    transports.insert(transport);
    return true;
}

} // namespace transport
} // namespace transcoder
