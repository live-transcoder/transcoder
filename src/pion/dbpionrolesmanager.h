#ifndef DBPIONROLESMANAGER_H
#define DBPIONROLESMANAGER_H

#include <string>
#include <soci/soci.h>

#include "pionrolesmanager.h"

namespace pion {
namespace net {


class DbPionRolesManager : public PionRolesManager
{
public:
    explicit DbPionRolesManager(soci::connection_pool *pool);
    virtual ~DbPionRolesManager();

    virtual bool empty();

    virtual bool        isUserInRole(const string &role, const string &user);
    virtual bool        isUserInRole(const set<string> &roles, const string &user);
    virtual set<string> getUserRoles(const string &user);


    const pair<bool, string> &getDatabaseError() const;

protected:
    void cleanError();
    void setError(const string &text);


private:
    soci::connection_pool *dbpool;
    pair<bool, string> databaseError;
};

typedef boost::shared_ptr<DbPionRolesManager> DbPionRolesManagerPtr;

}} // ::pion::net

#endif // DBPIONROLESMANAGER_H
