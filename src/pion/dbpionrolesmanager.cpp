#include <iostream>
#include <boost/algorithm/string.hpp>

#include "dbpionrolesmanager.h"
#include "util/dbutils.h"


namespace pion {
namespace net {


DbPionRolesManager::DbPionRolesManager(soci::connection_pool *pool)
    : dbpool(pool)
{
}

DbPionRolesManager::~DbPionRolesManager()
{
}

const pair<bool, string> &DbPionRolesManager::getDatabaseError() const
{
    return databaseError;
}

void DbPionRolesManager::cleanError()
{
    databaseError = make_pair(false, string());
}

void DbPionRolesManager::setError(const string &text)
{
    std::cerr << "Database error: " << text << '\n';
    databaseError = make_pair(true, text);
}

bool DbPionRolesManager::empty()
{
    bool result = true;

    if (!dbpool)
    {
        return true;
    }

    try
    {
        int count = 0;
        soci::session sql(*dbpool);
        checkDbSession(sql);

        sql << "SELECT COUNT(*) FROM roles", soci::into(count);
        if (sql.got_data() && count > 0)
        {
            result = false;
        }
    }
    catch (const exception &e)
    {
        setError(e.what());
    }

    return result;
}

bool DbPionRolesManager::isUserInRole(const string &role, const string &user)
{
    if (!dbpool)
    {
        return false;
    }

    try
    {
        int count = 0;
        soci::session sql(*dbpool);
        sql << "SELECT COUNT(*) FROM roles WHERE name = :name AND role = :role",
                soci::use(user, "name"),
                soci::use(role, "role"),
                soci::into(count);
        if (sql.got_data() && count > 0)
        {
            return true;
        }
    }
    catch (const exception &e)
    {
        setError(e.what());
    }

    return false;
}

bool DbPionRolesManager::isUserInRole(const set<string> &roles, const string &user)
{
    if (!dbpool)
    {
        return false;
    }

    try
    {
        string rolesStr = "(\"" + boost::algorithm::join(roles, "\",\"") + "\")";

        soci::session sql(*dbpool);
        checkDbSession(sql);

        int count = 0;
        sql << "SELECT COUNT(*) FROM roles WHERE name = :name AND role IN " << rolesStr,
                soci::use(user, "name"),
                soci::into(count);
        if (sql.got_data() && count > 0)
        {
            return true;
        }
    }
    catch (const exception &e)
    {
        setError(e.what());
    }

    return false;
}

set<string> DbPionRolesManager::getUserRoles(const string &user)
{
    if (!dbpool)
    {
        return set<string>();
    }

    try
    {
        set<string> rolesSet;
        int offset = 0;
        int count  = 100;

        soci::session sql(*dbpool);
        checkDbSession(sql);

        while (true)
        {
            vector<string> roles(count);
            sql << "SELECT role FROM roles WHERE name = :name LIMIT " << offset << "," << count,
                    soci::use(user,   "name"),
                    soci::into(roles);


            if (sql.got_data())
            {
                rolesSet.insert(roles.begin(), roles.end());
            }
            else
            {
                break;
            }

            offset += count;
        }
        return rolesSet;
    }
    catch (const exception &e)
    {
        setError(e.what());
    }

    return set<string>();
}


}} // ::pion::net
