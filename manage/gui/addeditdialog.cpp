#include <QMessageBox>

#include "addeditdialog.h"

AddEditDialog::AddEditDialog(QWidget *parent) :
    QDialog(parent)
{
    setupUi(this);
    destTableWidget->horizontalHeader()->setResizeMode(2, QHeaderView::Stretch);
}

void AddEditDialog::setData(const TranscoderData &data)
{
    this->data = data;

    nameLineEdit->setText(data.name);
    sourceLineEdit->setText(data.source);
    forcedFormatLineEdit->setText(data.sourceFormat);
    enabledCheckBox->setChecked(data.enabled);
    descriptionTextEdit->setText(data.description);

    formats = data.destinations;
    destTableWidget->setRowCount(0);
    for (int i = 0; i < data.destinations.size(); ++i)
    {
        const FormatData &format = data.destinations[i];
        destTableWidget->setRowCount(i + 1);
        updateRow(i, format);
    }
}

TranscoderData AddEditDialog::getData() const
{
    TranscoderData result = this->data;
    result.destinations = formats;

    result.name = nameLineEdit->text();
    result.source = sourceLineEdit->text();
    result.sourceFormat = forcedFormatLineEdit->text();
    result.enabled = enabledCheckBox->checkState() == Qt::Checked ? true : false;
    result.description = descriptionTextEdit->toPlainText();

    return result;
}

void AddEditDialog::setNameFieldEnabled(bool isEnabled)
{
    nameLineEdit->setReadOnly(!isEnabled);
}

void AddEditDialog::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
        case QEvent::LanguageChange:
            retranslateUi(this);
            break;
        default:
            break;
    }
}

void AddEditDialog::updateRow(int row, const FormatData &format)
{
    for (int i = 0; i < destTableWidget->columnCount(); ++i)
    {
        if (destTableWidget->item(row, i) == 0)
            destTableWidget->setItem(row, i, new QTableWidgetItem);
    }

    destTableWidget->item(row, 0)->setText(format.format);
    destTableWidget->item(row, 1)->setText(format.transport);
    destTableWidget->item(row, 2)->setText(format.resource);
}

void AddEditDialog::on_destTableWidget_currentCellChanged(int currentRow, int currentColumn, int previousRow, int previousColumn)
{
    if (currentRow == previousRow)
        return;

    if (currentRow < 0)
        return;

    FormatData &format = formats[currentRow];

    if (formatComboBox->findText(format.format) > -1)
        formatComboBox->setCurrentIndex(formatComboBox->findText(format.format));
    else
        QMessageBox::critical(this, "Unknown format", "Unknown format: " + format.format);

    if (transportComboBox->findText(format.transport) > -1)
        transportComboBox->setCurrentIndex(transportComboBox->findText(format.transport));
    else
        QMessageBox::critical(this, "Unknown transport", "Unknonwn transport: " + format.transport);

    resourceLineEdit->setText(format.resource);
    rolesLineEdit->setText(format.roles);
}

void AddEditDialog::on_updatePushButton_clicked()
{
    int currentRow = destTableWidget->currentRow();
    if (currentRow < 0)
        return;

    FormatData &format = formats[currentRow];

    format.format = formatComboBox->currentText();
    format.transport = transportComboBox->currentText();
    format.resource = resourceLineEdit->text();
    format.roles = rolesLineEdit->text();

    updateRow(currentRow, format);
}

void AddEditDialog::on_addPushButton_clicked()
{
    int currentRow = destTableWidget->rowCount();
    if (currentRow < 0)
        return;

    FormatData format;
    format.format = formatComboBox->currentText();
    format.transport = transportComboBox->currentText();
    format.resource = resourceLineEdit->text();
    format.roles = rolesLineEdit->text();

    destTableWidget->setRowCount(currentRow + 1);
    formats.append(format);

    updateRow(currentRow, format);
}

void AddEditDialog::on_deletePushButton_clicked()
{
    int currentRow = destTableWidget->currentRow();
    if (currentRow < 0)
        return;

    destTableWidget->removeRow(currentRow);
    formats.removeAt(currentRow);
}

void AddEditDialog::on_nameLineEdit_textEdited(const QString &text)
{
    resourceLineEdit->setText("/" + text + ".");
}
