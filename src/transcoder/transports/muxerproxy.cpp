#include "muxerproxy.h"

namespace transcoder {
namespace transport {

MuxerProxy::MuxerProxy()
{
}

MuxerProxy::MuxerProxy(const string &uri)
    : uri(uri)
{
}

MuxerProxy::MuxerProxy(const FormatDescription &format, const string &uri)
    : Muxer(),
      uri(uri)
{
    init(format);
}

MuxerProxy::~MuxerProxy()
{
}


int MuxerProxy::writePacket(const PacketPtr &packet)
{
    if (!isValid())
    {
        return -128;
    }

    if (!container->isOpened())
    {
        if (uri.empty())
        {
            return -129;
        }

        if (!container->openOutput(uri.c_str()))
        {
            return -130;
        }

        container->dump();
    }

    if (!isHeaderWriten)
    {
        container->writeHeader();
        isHeaderWriten = true;
    }

//    clog << "Output packet time " << packet->getStreamIndex()
//         << ", " << packet->getTimeBase().getDouble() * packet->getPts()
//         << ", fake pts: " << packet->getTimeBase().getDouble() * packet->getFakePts()
//         << endl;

    int64_t result = container->writePacket(packet);

    return result;
}

} // namespace transport
} // namespace transcoder
