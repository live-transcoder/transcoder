#ifndef TRANSCODERMANAGER_H
#define TRANSCODERMANAGER_H

#include <set>
#include <list>
#include <iostream>

#include "boost/utility/mutexed_singleton.hpp"

#include "transcoder.h"
#include "util/mutex.hpp"

namespace transcoder {

using namespace std;

class TranscoderManager : public boost::mutexed_singleton<TranscoderManager>
{
public:
    typedef list<TranscoderPtr> Transcoders;

public:
    TranscoderManager(boost::restricted);

    /**
     * @brief load transcoders from data base
     * @return true if all ok
     */
    bool load();
    bool load(const string &name);
    /**
     * @brief save transcoders to data base
     * @return
     */
    bool save();
    bool save(const string &name);

    void startAll();
    void stopAll();
    void restartAll();


    bool addTranscoder(const TranscoderPtr &transcoder);
    bool removeTranscoder(const string &name);
    TranscoderPtr getTranscoder(const string &name);
    const Transcoders &getTranscoders() const;

private:

    Transcoders::iterator findTranscoder(const string &name);

private:
    Transcoders transcoders;
    set<string> deletedTranscoders;
};

} // ::transcoder

#endif // TRANSCODERMANAGER_H
