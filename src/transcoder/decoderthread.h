/** =============================================================================================
  
    This file is a part of "%ProjectName%" project
    http://htrd.su
    
    @date   2012-4-7
    @brief
    
    Copyright (C) 2002-2012 by Alexander 'hatred' Drozdov <adrozdoff@gmail.com>
    
    This program is free software; you can redistribute it and/or modify
    it under the terms of the version 2 of GNU General Public License as
    published by the Free Software Foundation.
    
    For more information see LICENSE and LICENSE.ru files
    
   ============================================================================================== */

#ifndef DECODER_H
#define DECODER_H

#include <iostream>

#include <boost/thread.hpp>

#include "boost/atomic.hpp"
#include "thread.hpp"
#include "producer.h"
#include "formatdescription.h"

#include "av/ffmpeg.h"
#include "av/container.h"
#include "av/frame.h"
#include "av/rect.h"

using namespace std;
using namespace av;
using namespace transcoder;

class DecodeThread : public Thread<DecodeThread>, public Producer
{
public:
    DecodeThread();
    ~DecodeThread();

    virtual void run();

    bool isRunning();
    bool isActive();
    void stop();

    const std::string& getStreamUri();
    void               setStreamUri(const string &uri);

    void               forceInputFormat(const string &format);

    const FormatDescription &getFormatDescription() const { return formatDescription; }

    FramePtr getLastFrame() const;

    const boost::xtime& getLastFrameTime() const;

    void setEncodersWaitTime(int64_t time);
    int64_t getEncodersWaitTime() const;

private:
    void pushFrame(const FramePtr &frame);

private:
    boost::atomic_bool runFlag;
    boost::atomic_bool activeFlag;
    string streamUri;
    string inputFormatName;

    FormatDescription formatDescription;

    // Optimization for multiple readers and one writer
    mutable boost::shared_mutex frameMutex;
    FramePtr                    lastFrame;

    boost::xtime lastFrameTime;

    boost::atomic_int64_t encodersWaitTime;
};

#endif // DECODER_H
