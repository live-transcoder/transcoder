#include <cerrno>

#include "log/logstream.h"
#include "baseencoder.h"

using namespace std;
using namespace av;

namespace transcoder {
namespace encode {

using namespace logstream;

FrameEncoder::FrameEncoder()
    : BaseEncodeContext()
{
}

FrameEncoder::FrameEncoder(const FormatDescription &description)
    : BaseEncodeContext(description)
{
}

bool FrameEncoder::isCodersOpened()
{
    return false;
}

bool FrameEncoder::openCoders()
{
    return false;
}

bool FrameEncoder::canEncode(const FramePtr &inFrame)
{
    return getNewFrameStreamIndex(inFrame) >= 0;
}

struct MatchStreamByOriginalIndex
{
    MatchStreamByOriginalIndex(int index)
        : index(index)
    {}

    bool operator()(const StreamDescription& desc)
    {
        return desc.mapFromIndex == index;
    }

    int index;
};

int FrameEncoder::getNewFrameStreamIndex(const FramePtr &inFrame)
{
    if (!isValid())
    {
        return -1;
    }

    const FormatDescription &desc = getFormatDescription();

    if (desc.streams.empty())
    {
        return -1;
    }

    int originalIndex = inFrame->getStreamIndex();

    vector<StreamDescription>::const_iterator it =
            std::find_if(desc.streams.begin(),
                         desc.streams.end(),
                         MatchStreamByOriginalIndex(originalIndex));

    if (it == desc.streams.end())
    {
        return -1;
    }

    int newIndex = it->mapToIndex;

    if (newIndex >= streams.size())
    {
        newIndex = -1;
    }

    return newIndex;
}

std::list<PacketPtr> FrameEncoder::encode(const FramePtr &inFrame, const EncodedPacketHandler &onPacketHandler)
{
    list<PacketPtr> result;

    if (!inFrame)
    {
        return result;
    }

    VideoFramePtr   videoFrame  = boost::dynamic_pointer_cast<VideoFrame>(inFrame);
    AudioSamplesPtr audioSamples = boost::dynamic_pointer_cast<AudioSamples>(inFrame);

    int streamIndex = inFrame->getStreamIndex();
    /*int streamIndex = mappedStreamIndex(inFrame->getStreamIndex());
    if (streamIndex < 0)
    {
        return result;
    }*/

    if (ptsRecalculators.find(streamIndex) == ptsRecalculators.end())
    {
        ptsRecalculators[streamIndex] = FramePtsRecalculator();
    }

    if (videoFrame)
    {
        result = encodeVideo(videoFrame, onPacketHandler);
    }
    else if (audioSamples)
    {
        result = encodeAudio(audioSamples, onPacketHandler);
    }

    return result;
}


static inline
VideoResamplerPtr createVideoResampler(const StreamCoderPtr &outputCoder,
                                       const VideoFramePtr  &inputFrame)
{
    VideoResamplerPtr videoResampler =
            VideoResamplerPtr(new VideoResampler(outputCoder->getWidth(),
                                                 outputCoder->getHeight(),
                                                 outputCoder->getPixelFormat(),
                                                 inputFrame->getWidth(),
                                                 inputFrame->getHeight(),
                                                 inputFrame->getPixelFormat()));
    if (!videoResampler->isValid())
    {
        videoResampler.reset();
    }

    return videoResampler;
}


list<PacketPtr> FrameEncoder::encodeVideo(const VideoFramePtr &inFrame, const EncodedPacketHandler &onPacketHandler)
{
    list<PacketPtr> result;

    int streamIndex = inFrame->getStreamIndex();

    FramePtsRecalculator &ptsRecalc      = ptsRecalculators[streamIndex];
    StreamCoderPtr        videoCoder     = coders[streamIndex];
    VideoResamplerPtr     videoResampler;

    if (!videoCoder->isOpened())
    {
        videoCoder->open();
        if (!videoCoder->isOpened())
            return result;
    }

    if (videoResamplers.find(streamIndex) != videoResamplers.end())
    {
        videoResampler = videoResamplers[streamIndex];
    }

    if (!videoResampler)
    {
        if (videoCoder->getWidth() != inFrame->getWidth() ||
            videoCoder->getHeight() != inFrame->getHeight() ||
            videoCoder->getPixelFormat() != inFrame->getPixelFormat())
        {
            videoResampler = createVideoResampler(videoCoder, inFrame);
            if (!videoResampler)
                return result;

            // Update video resampler
            videoResamplers[streamIndex] = videoResampler;
        }
    }
    else
    {
        // Use case: input stream restarted and now input width, height and color space
        // equal output one. So resampler is not needed, reset it
        if (videoCoder->getWidth() == inFrame->getWidth() &&
            videoCoder->getHeight() == inFrame->getHeight() &&
            videoCoder->getPixelFormat() == inFrame->getPixelFormat())
        {
            videoResampler.reset();

            // Update video resampler
            videoResamplers.erase(streamIndex);
        }
        // Use case: input stream restarted and now input width, height or color space does not
        // equal source width, height or color space of resampler. So resampler must be recreated
        else if (videoResampler->getSrcWidth() != inFrame->getWidth() ||
                 videoResampler->getSrcHeight() != inFrame->getHeight() ||
                 videoResampler->getSrcPixelFormat() != inFrame->getPixelFormat())
        {
            videoResampler = createVideoResampler(videoCoder, inFrame);
            if (!videoResampler)
                return result;

            // Update video resampler
            videoResamplers[streamIndex] = videoResampler;
        }
    }

    int stat = -1;

    VideoFramePtr frame;
    if (videoResampler)
    {
        frame = VideoFramePtr(new VideoFrame(videoCoder->getPixelFormat(),
                                             videoCoder->getWidth(),
                                             videoCoder->getHeight()));

        stat = videoResampler->resample(frame, inFrame);
        if (stat < 0)
        {
            return result;
        }
    }
    else
    {
        frame = boost::dynamic_pointer_cast<VideoFrame>(inFrame->clone());
    }

#if 0
    int c = 0;

    if (filterGraph)
    {
        //av_vsrc_buffer_add_frame(bufferSrcCtx, frame->getAVFrame(), AV_VSRC_BUF_FLAG_OVERWRITE | AV_BUFFERSRC_FLAG_NO_CHECK_FORMAT);
        //av_buffersrc_add_frame(bufferSrcCtx, frame->getAVFrame(), AV_BUFFERSRC_FLAG_NO_CHECK_FORMAT);
        if (av_buffersrc_add_frame(bufferSrcCtx, frame->getAVFrame(), AV_BUFFERSRC_FLAG_NO_CHECK_FORMAT) < 0)
        {
            throw runtime_error("Failed to inject frame into filter network");
        }

        while (av_buffersink_poll_frame(bufferSinkCtx))
        {
            picref = 0;
            if (av_buffersink_get_buffer_ref(bufferSinkCtx, &picref, 0) < 0)
            {
                logger(level::error) << "AV Filter tols us it has a frame available but filed to output one" << endl;
                goto cont;
            }

            if (picref)
            {
                FramePtr filteredFrame(new Frame());
                PacketPtr packet(new Packet());
                avfilter_fill_frame_from_video_buffer_ref(filteredFrame->getAVFrame(), picref);

                filteredFrame->setComplete(true, picref->pts);
                stat = videoCoder->encodeVideo(packet, filteredFrame);

                if (stat >= 0)
                {
                    result.push_back(packet);
                    if (!onPacketHandler.empty())
                    {
                        onPacketHandler(packet);
                    }
                }
                else
                {
                    logger(level::error) << "Encoding fail: " << stat << endl;
                }
            }

            cont:
            avfilter_unref_buffer(picref);
        }
    }
    else
#endif
    {
        ////////////////////////////////////////////////////////////////////////////////////////////
        // Recalculate frame PTS
        ptsRecalc.recalc(frame);
        ////////////////////////////////////////////////////////////////////////////////////////////

        PacketPtr packet(new Packet());
        stat = videoCoder->encodeVideo(packet, frame, onPacketHandler);
        if (stat == 0) // push only non-empty packets
        {
            result.push_back(packet);
//            if (!onPacketHandler.empty())
//            {
//                onPacketHandler(packet);
//            }
        }
    }

    return result;
}


static inline
FilterGraphPtr createAudioFilter(const StreamCoderPtr&  outputCoder,
                                 const AudioSamplesPtr& inputSamples,
                                 const string&          graphDescription = string())
{
    list<int>            dstSampleRates;
    list<AVSampleFormat> dstSampleFormats;
    list<uint64_t>       dstChannelLayouts;

    dstSampleRates.push_back(outputCoder->getSampleRate());
    dstSampleFormats.push_back(outputCoder->getSampleFormat());
    dstChannelLayouts.push_back(outputCoder->getChannelLayout());

    FilterGraphPtr graph =
            FilterGraph::createSimpleAudioFilterGraph(inputSamples->getTimeBase(),
                                                      inputSamples->getSampleRate(),
                                                      inputSamples->getSampleFormat(),
                                                      inputSamples->getChannelsLayout(),
                                                      dstSampleRates,
                                                      dstSampleFormats,
                                                      dstChannelLayouts,
                                                      graphDescription);
    if (graph && graph->getSinkFilter())
    {
        graph->getSinkFilter()->setFrameSize(outputCoder->getFrameSize());
    }

    return graph;
}


std::list<PacketPtr> FrameEncoder::encodeAudio(const AudioSamplesPtr &inFrame, const EncodedPacketHandler &onPacketHandler)
{
    list<PacketPtr> result;

    int streamIndex = inFrame->getStreamIndex();

    FramePtsRecalculator &ptsRecalc      = ptsRecalculators[streamIndex];
    StreamCoderPtr        audioCoder     = coders[streamIndex];
    FilterGraphPtr        filterGraph;

    if (!audioCoder->isOpened())
    {
        std::clog << "PRE FRAME SIZE: " << audioCoder->getAudioFrameSize() << std::endl;
        audioCoder->open();
        if (!audioCoder->isOpened())
            return result;
        std::clog << "PST FRAME SIZE: " << audioCoder->getAudioFrameSize() << std::endl;
    }

    if (audioFilters.find(streamIndex) != audioFilters.end())
    {
        filterGraph = audioFilters[streamIndex];
    }

    if (!filterGraph)
    {
        filterGraph = createAudioFilter(audioCoder, inFrame);
        if (!filterGraph)
        {
            // TODO exception?
            clog << "Can't create filter graph" << endl;
            return result;
        }

        if (!filterGraph->getSrcFilter())
        {
            // TODO exception?
            clog << "Can't take src filter" << endl;
            return result;
        }

        if (!filterGraph->getSinkFilter())
        {
            // TODO exception?
            clog << "Can't take sink filter" << endl;
            return result;
        }

        // Update video resampler
        audioFilters[streamIndex] = filterGraph;
    }

    int stat = -1;

    stat = filterGraph->getSrcFilter()->addFrame(inFrame);
    if (stat < 0)
    {
        // TODO exception?
        char buf[256];
        av_strerror(stat, buf, sizeof(buf));
        clog << "Can't add frame to filter chain: " << stat << ", " << buf << endl;
        return result;
    }

    while (true)
    {
        FilterBufferRef samplesref;
        stat = filterGraph->getSinkFilter()->getBufferRef(samplesref, 0);
        if (stat == AVERROR(EAGAIN) || stat == AVERROR_EOF)
        {
            break;
        }

        if (stat < 0)
        {
            // TODO exception?
            return result;
        }

        if (samplesref.isValid())
        {
            FramePtr outFrame;
            samplesref.copyToFrame(outFrame);
            if (outFrame)
            {
                AudioSamplesPtr outSamples = boost::dynamic_pointer_cast<AudioSamples>(outFrame);

                outSamples->setTimeBase(inFrame->getTimeBase());
                outSamples->setFakePts(inFrame->getFakePts()); // FIXME: look like wrong: we can produce more than one output frame per one input
                outSamples->setStreamIndex(streamIndex);

                PacketPtr packet(new Packet);
                stat = audioCoder->encodeAudio(packet, outSamples, onPacketHandler);
                if (stat == 0)
                {
                    result.push_back(packet);
                }
                else
                {
                    // TODO exception?
                    return result;
                }
            }
        }
    }

    return result;
}


}} // ::transcoder::encode
