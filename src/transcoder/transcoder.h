#ifndef TRANSCODER_H
#define TRANSCODER_H

#include <list>
#include <vector>
#include <iostream>

#include <boost/smart_ptr.hpp>

#include "av/ffmpeg.h"
#include "decoderthread.h"
#include "encodethread.h"
#include "formatdescription.h"
#include "util/mutex.hpp"

namespace transcoder {

using namespace std;


class Transcoder;
typedef boost::shared_ptr<Transcoder> TranscoderPtr;
typedef boost::weak_ptr<Transcoder> TranscoderWPtr;


class Transcoder : public Lockable
{
public:
    typedef vector<FormatDescription> FormatDescriptions;

    Transcoder();
    Transcoder(const string &name, const string &description = string(), bool isEnabled = false);
    virtual ~Transcoder();

    bool start();
    bool stop();
    bool restart();
    bool isRunning();

    bool isEnabled() const { return isEnabledFlag; }
    void setEnabled(bool isEnabledFlag) { this->isEnabledFlag = isEnabledFlag; }

    void setName(const string &newName) { name = newName; }
    const string& getName() const { return name; }

    void setDescription(const string &newDescription) { description = newDescription; }
    const string& getDescription() const { return description; }

    void setInput(const string &uri, const string &inputFormat = string());
    const string &getInput() const;
    const string &getInputFormat() const;

    bool  isActive() const;

    bool addEncoder(const FormatDescription &desc);
    bool removeEncoder(const FormatDescription &desc);
    bool removeEncoder(uint32_t id);
    void removeEncoders();
    const vector<FormatDescription> &getEncoders() const;

private:
    FormatDescriptions::iterator findDescription(const FormatDescription &desc);

private:
    string  name;
    string  description;
    bool    isEnabledFlag;

    boost::shared_ptr<DecodeThread>         decoder;
    list< boost::shared_ptr<EncodeThread> > encoders;

    string                                  inputUri;
    string                                  inputFormat;
    mutable vector<FormatDescription>       encoderDesctiptions;
};


} // ::transcoder

#endif // TRANSCODER_H
