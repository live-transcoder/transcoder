#ifndef UTILS_H
#define UTILS_H

#include <stdint.h>
#include <time.h>

#include <cstddef>
#include <string>
#include <iostream>
#include <sstream>
#include <map>

#include <boost/thread.hpp>

#include "../av/avtime.h"

/**
 * Convert boost::xtime to microsecond representation
 */
#define GET_TIME_NSEC(tm) \
    ((tm.sec * 1000000000L) + tm.nsec)

/**
 * Difference between timeB and timeA in microseconds. timeA and timeB is boost::xtime
 */
#define GET_TIME_DIFF_NSEC(timeA, timeB) \
    (((timeA.sec * 1000000000L) + timeA.nsec) - ((timeB.sec * 1000000000L) + timeB.nsec))


/**
 * @brief sleep_msec  sleep for a given amount of milliseconds
 * @param interval    interval in milliseconds to sleep
 */
extern
void sleep_msec(int64_t interval);

/**
 * @brief sleep_usec  sleep for a given amount of microseconds
 * @param interval    interval to sleep in microseconds
 */
extern
void sleep_usec(uint64_t interval);

/**
 * Replace place holders look like %N (N is any valid latin1 symbol) with values from replace map.
 * Unknown place holders stay as is.
 *
 * @param str        string where place holders must be reseted
 * @param replaceMap
 */
extern
void placeHoldersReplace(std::string& str, const std::map<std::string, std::string>& replaceMap);



#define STR2(a) #a
#define STR(a) STR2(a)
#define CONCAT(a, b) a ## b
#define PrettyScopedTimer()                                                              \
    std::stringstream CONCAT(__ss, __LINE__);                                            \
    CONCAT(__ss, __LINE__) << boost::this_thread::get_id();                              \
    ScopedTimer CONCAT(__PrettyScopedTimer, __LINE__)(std::string(__PRETTY_FUNCTION__) + \
                                                      std::string(":") +                 \
                                                      std::string(STR(__LINE__)) +       \
                                                      std::string(":") +                 \
                                                      std::string(CONCAT(__ss, __LINE__).str()))
#define PrettyScopedTimer2(comment)                                                      \
    std::stringstream CONCAT(__ss, __LINE__);                                            \
    CONCAT(__ss, __LINE__) << boost::this_thread::get_id();                              \
    ScopedTimer CONCAT(__PrettyScopedTimer, __LINE__)(std::string(__PRETTY_FUNCTION__) + \
                                                      std::string(":") +                 \
                                                      std::string(STR(__LINE__)) +       \
                                                      std::string(":") +                 \
                                                      std::string(CONCAT(__ss, __LINE__).str()) + \
                                                      std::string(":") +                 \
                                                      std::string(comment))
#define PrettyScopedTimer3(comment, stuff)                                               \
    std::stringstream CONCAT(__ss, __LINE__);                                            \
    CONCAT(__ss, __LINE__) << boost::this_thread::get_id();                              \
    ScopedTimer CONCAT(__PrettyScopedTimer, __LINE__)(std::string(__PRETTY_FUNCTION__) + \
                                                      std::string(":") +                 \
                                                      std::string(STR(__LINE__)) +       \
                                                      std::string(":") +                 \
                                                      std::string(CONCAT(__ss, __LINE__).str()) + \
                                                      std::string(":") +                 \
                                                      std::string(comment), stuff)

class ScopedTimer
{
public:
    struct TimingValues
    {
        TimingValues()
            : timeSum(0.0),
              callsCount(0),
              minTime(0.0),
              maxTime(0.0),
              lastCall(0.0),
              timeBetweenCallSum(0.0)
        {}

        double timeSum;
        time_t callsCount;

        double minTime;
        double maxTime;

        double lastCall;
        double timeBetweenCallSum;
    };

    typedef int64_t time_t;
    typedef std::map<std::string, TimingValues>     ThreadData;
    typedef std::map<boost::thread::id, ThreadData> TimingAvarageData;

    /**
     * Main ctor
     *
     * @param name      name for scoped timer line "void foo()". If ScopedTimer created with
     *                  preprocessor directives PrettyScopedTimer or PrettyScopedTimer2 name
     *                  automaticaly cantain current function name, line of code and thread id
     * @param printStuf regulator for printing profile information, if value true (default) print
     *                  current profile information, if false - do not print
     */
    ScopedTimer(const std::string& name, bool printStuf = true);
    ~ScopedTimer();

    /**
     * Dump aggregated data to given ostream
     * @param ost output stream, by default std::clog
     */
    static void dump(std::ostream *ost = &std::clog);

private:
    void process(const std::string& name, time_t startClock, time_t currentClock);

private:
    std::string name;
    time_t      startClock;
    bool        printStuf;

    static TimingAvarageData average;
    static boost::shared_mutex averageMutex;
};

#endif // UTILS_H
