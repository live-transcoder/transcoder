/** =============================================================================================
  
    This file is a part of "%ProjectName%" project
    http://htrd.su
    
    @date   2012-4-7
    @brief
    
    Copyright (C) 2002-2012 by Alexander 'hatred' Drozdov <adrozdoff@gmail.com>
    
    This program is free software; you can redistribute it and/or modify
    it under the terms of the version 2 of GNU General Public License as
    published by the Free Software Foundation.
    
    For more information see LICENSE and LICENSE.ru files
    
   ============================================================================================== */

#include "consumer.h"

#ifndef PRODUCER_H
#define PRODUCER_H

#include <cstdio>
#include <list>
#include <tr1/memory>

#include <boost/thread.hpp>
#include <boost/smart_ptr.hpp>
#include <boost/thread/barrier.hpp>

#include "thread.hpp"
#include "pointed.hpp"

class  Consumer;
class  NotifyThread;
struct ResponseHandler;

typedef boost::function<void ()>           ResponseHandlerProc;
typedef boost::shared_ptr<ResponseHandler> ResponseHandlerPtr;
typedef boost::weak_ptr<ResponseHandler>   ResponseHandlerWPtr;

class Producer
{

public:
    Producer();
    virtual ~Producer();

    void subscribe(const boost::weak_ptr<Consumer> &consumer);
    void unsubscribe(const boost::weak_ptr<Consumer> &consumer);

    uint subscribersCount() const;

protected:
    virtual void notifyAll(void *data = 0);
    virtual void waitSubscribers(int64_t waitTimeMsec = -1);

private:
    friend class NotifyThread;

    void responseHandler();
    void responseReset(std::size_t cout);

private:
    typedef std::list<boost::shared_ptr<NotifyThread> > NotifyList;

    mutable boost::mutex consumersMutex;
    NotifyList   notifyList;
    uint         cachedNotifyListSize;

    boost::shared_mutex           responseCompleteMutex;
    boost::condition_variable_any responseCompleteCond;
    bool                          responseCompleteFlag;
    std::size_t                   responseCount;
    std::size_t                   responseNum;

    ResponseHandlerPtr            responseHandlerPrt;
};

DECLARE_PTR_TYPE(Producer);

#endif // PRODUCER_H
