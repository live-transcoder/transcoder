/*

Connections:

     sqlite3://database_file.db
         see http://soci.sourceforge.net/doc/backends/sqlite3.html

     postgresql://host=HOST port=PORT dbname=DB_NAME  user=USER password=PASS
         see http://soci.sourceforge.net/doc/backends/postgresql.html and
             http://www.postgresql.org/docs/8.3/interactive/libpq-connect.html

     mysql://host=HOST PORT=PORT db=DB_NAME user=USER password=PASS
         see http://soci.sourceforge.net/doc/backends/mysql.html

     oracle://service=DB_NAME user=USER password=PASS
         see http://soci.sourceforge.net/doc/backends/oracle.html


*/



SQLite
~~~~~~
CREATE TABLE "transcoders" (
    "name" TEXT UNIQUE NOT NULL,
    "description" TEXT,
    "enabled" INTEGER NOT NULL DEFAULT (1),
    "source" TEXT NOT NULL,
    "destination" TEXT NOT NULL,
    "servirid" INTEGER NOT NULL DEFAULT (0),
    PRIMARY KEY ("name")
);


CREATE TABLE "users" (
    "name" TEXT UNIQUE NOT NULL,
    "password" TEXT NOT NULL,
    PRIMARY KEY ("name")
);


CREATE TABLE "roles" (
    "name" TEXT NOT NULL REFERENCES "users" ("name") ON DELETE CASCADE ON UPDATE CASCADE,
    "role" TEXT NOT NULL
);

CREATE UNIQUE INDEX "users_name" ON "users" ("name");
CREATE INDEX "roles_name" ON "roles" ("name");
CREATE INDEX "roles_role" ON "roles" ("role");
CREATE INDEX "roles_name_role" ON "roles" ("name", "role");


CREATE TRIGGER "users_delete_user" AFTER DELETE ON "users" FOR EACH ROW BEGIN DELETE FROM "roles" WHERE "roles"."name" = OLD."name"; END;

~~~~~~


MySQL 5
~~~~~~
CREATE TABLE `roles` (
  `name` text NOT NULL,
  `role` text NOT NULL,
  KEY `name` (`name`(255)),
  KEY `role` (`role`(255)),
  KEY `roles_name_IDX` (`name`(255),`role`(255))
);


CREATE TABLE users (
    name TEXT NOT NULL,
    password TEXT NOT NULL,
    PRIMARY KEY (name(255)),
    UNIQUE(name(255))
);


CREATE TABLE roles (
    name TEXT NOT NULL,
    role TEXT NOT NULL,
    INDEX(name(255)),
    INDEX(role(255))
);

DELIMITER $$
CREATE TRIGGER users_delete_user AFTER DELETE ON users
FOR EACH ROW
BEGIN
        DELETE FROM roles WHERE roles.name = OLD.name;
END;
$$

DELIMITER ;


alter table roles charset=utf8;
alter table users charset=utf8;
alter table transcoders charset=utf8;


~~~~~~
