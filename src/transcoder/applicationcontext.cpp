#include "applicationcontext.h"

namespace transcoder {

ApplicationContext::ApplicationContext(boost::restricted)
    : daemonize(false),
      verbosity(0),
      transcoderId(0)
{
}

} // ::transcoder
