#ifndef ADMINREST_H
#define ADMINREST_H

#include <map>

#include <boost/bind.hpp>

#include "http_server/server.hpp"


namespace transcoder {
namespace admin {
namespace rest {

using namespace std;


class AdminRest
{
public:

    AdminRest();

    /**
     * @brief handle handle request
     * @param httpRequest
     * @param tcpConnection
     * @return
     */
    bool handle(http::server::request_ptr &req, http::server::reply_ptr &rep);


protected:
    typedef boost::function<void (http::server::request_ptr &req, http::server::reply_ptr &rep)>  InternalRequestHandler;

protected:
    void addResource(const string &resource, const InternalRequestHandler &handler);
    bool findResource(const string &resource, InternalRequestHandler &handler) const;


private:
    void requestValidate(http::server::request_ptr &req, http::server::reply_ptr &rep, const string &method, const string &mime);

    // List modification
    void transcoderList(http::server::request_ptr &req, http::server::reply_ptr &rep);
    void transcoderAdd(http::server::request_ptr &req, http::server::reply_ptr &rep);
    void transcoderModify(http::server::request_ptr &req, http::server::reply_ptr &rep);
    void transcoderDelete(http::server::request_ptr &req, http::server::reply_ptr &rep);

    // Start/stop routines
    void transcoderStart(http::server::request_ptr &req, http::server::reply_ptr &rep);
    void transcoderStop(http::server::request_ptr &req, http::server::reply_ptr &rep);
    void transcoderRestart(http::server::request_ptr &req, http::server::reply_ptr &rep);
    void transcoderRestartAll(http::server::request_ptr &req, http::server::reply_ptr &rep);

    // Save to db routines
    void transcoderSave(http::server::request_ptr &req, http::server::reply_ptr &rep);
    void transcoderSaveAll(http::server::request_ptr &req, http::server::reply_ptr &rep);

private:

    map<string, InternalRequestHandler> resources;
};


}}} // ::trasncoder::admin::rest

#endif // ADMINREST_H
