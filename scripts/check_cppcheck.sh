#!/bin/bash

if [ "$1" == "-do-check" ]; then
    args="--check-config"
    shift
fi

gcc_version=`gcc --version | grep 'gcc (GCC)' | awk '{print $3}'`

cppcheck -v --template "gcc" --enable=all $args -I../src -I/usr/include/c++/$gcc_version/ $@ \
    ../src/av/*.cpp \
    ../src/pion/*.cpp \
    ../src/transcoder/*.cpp
