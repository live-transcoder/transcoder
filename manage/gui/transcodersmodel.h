#ifndef TRANSCODERSMODEL_H
#define TRANSCODERSMODEL_H

#include <QAbstractTableModel>
#include <QList>
#include <QMap>

#include "formatdata.h"

class TranscodersModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    explicit TranscodersModel(QObject *parent = 0);

    void updateTranscoderData(const QList<TranscoderData> &data);
    void updateTranscoderData(int index, const TranscoderData &data);
    void updateTranscoderData(const QModelIndex &index, const TranscoderData &data);
    void putTranscoderData(const QString &transcoderName, const TranscoderData &data);

    const TranscoderData &getTranscoderData(int index) const;
    const TranscoderData &getTranscoderData(const QModelIndex &index) const;
    const QList<TranscoderData> &getTranscoderData() const;

    void  addTranscoderData(const TranscoderData &data);
    void  removeTranscoderData(int index);
    void  removeTranscoderData(const QModelIndex &index);
    void  removeTranscoderData(const QString &name);
    void  clearTranscoderData();
    
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;

signals:
    
public slots:
    

private:
    QList<TranscoderData> transcoders;
    const TranscoderData  fakeData;

};

#endif // TRANSCODERSMODEL_H
