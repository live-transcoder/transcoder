#ifndef HTTPBASICRBACAUTH_H
#define HTTPBASICRBACAUTH_H

#include <list>

#include <pion/net/HTTPAuth.hpp>
#include <pion/net/HTTPBasicAuth.hpp>

#include "httprbac.h"

namespace pion {
namespace net {


class HTTPBasicRbacAuth : public HTTPRbac, public HTTPBasicAuth
{
public:
    HTTPBasicRbacAuth(PionUserManagerPtr userManager, PionRolesManagerPtr rolesManager, const string &realm = "PION::NET");
    virtual ~HTTPBasicRbacAuth();

    virtual bool handleRequest(HTTPRequestPtr &request, TCPConnectionPtr &tcp_conn);

    void addPermit(const string &resource, const string &role);
    void addPermit(const string &resource);
    void addRestrict(const string &resource, const string &role);
    void addRestrict(const string &resource);

//    void removePermit(const string &resource, const string &role);
//    void removeRestrict(const string &resource, const string &role);
//    void clearPermit(const string &resource);
//    void clearRestrict(const string &resource);

private:
    PionUserManagerPtr     userManager;
};


typedef boost::shared_ptr<HTTPBasicRbacAuth> HTTPBasicRbacAuthPtr;

}} // ::pion::net

#endif // HTTPBASICRBACAUTH_H
