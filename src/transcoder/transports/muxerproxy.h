#ifndef TRANSCODER_TRANSPORT_MUXERPROXY_H
#define TRANSCODER_TRANSPORT_MUXERPROXY_H

#include "muxer.h"

namespace transcoder {
namespace transport {

/**
 * This muxer uses FFMPEG uri handler to muxing and writing packets
 */
class MuxerProxy : public Muxer
{
public:
    MuxerProxy();
    MuxerProxy(const std::string& uri);
    MuxerProxy(const FormatDescription &format, const std::string& uri);
    virtual ~MuxerProxy();



protected:
    virtual int writePacket(const PacketPtr &packet);

private:
    std::string uri;
};

DECLARE_PTR_TYPE(MuxerProxy);

} // namespace transport
} // namespace transcoder

#endif // TRANSCODER_TRANSPORT_MUXERPROXY_H
