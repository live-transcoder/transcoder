#include <exception>

#include <boost/lexical_cast.hpp>

#include "formatdescription.h"

using namespace std;
using namespace av;

namespace transcoder
{

/**
 * Parse string option from list
 *
 * @return true if option exists and successfuly parsed, false in any other ways
 */
template<typename T>
inline bool getOption(const map<string, string> &options, const string &option, T &targetValue)
{
    bool isOk = false;
    try
    {
        if (options.find(option) != options.end())
        {
            targetValue = boost::lexical_cast<T>(options.at(option));
            isOk = true;
        }
    }
    catch (const exception &e)
    {
    }

    return isOk;
}

/**
 * Store option as string
 *
 * @return true value correctly parsed, false in any other ways
 */
template<typename T>
inline bool setOption(map<string, string> &options, const string &option, const T &sourceValue)
{
    bool isOk = false;
    try
    {
        string value = boost::lexical_cast<string>(sourceValue);
        options[option] = value;
        isOk = true;
    }
    catch (const exception &e)
    {
    }

    return isOk;
}

FormatDescription::FormatDescription()
{
    reset();
}

void FormatDescription::parseOptionsList(const OptionsList &options)
{
    getOption(options, "format",      format);
}

OptionsList FormatDescription::toOptionsList()
{
    OptionsList options;

    setOption(options, "format",      format);

    return options;
}

void FormatDescription::reset()
{
    isEnabled = false;

    format.clear();
    uri.clear();
    transport.clear();
    resource.clear();
    roles.clear();
    meta.clear();

    /*
    vector<StreamDescription>::iterator it;
    for (it = streams.begin(); it != streams.end(); ++it)
    {
        it->reset();
    }*/

    streams.clear();

}

StreamDescription::StreamDescription()
{
    reset();
}

void StreamDescription::parseOptionsList(const OptionsList &options)
{
    getOption(options, "codec", codec);
    getOption(options, "bit_rate", bitRate);
    getOption(options, "bit_rate_min", bitRateMin);
    getOption(options, "bit_rate_max", bitRateMax);
    getOption(options, "audio_channels", audioChannels);
    getOption(options, "audio_sample_rate", audioSampleRate);
    getOption(options, "video_strict", videoStrict);
    getOption(options, "video_buffer_size", videoBufferSize);
    getOption(options, "video_bit_rate_tolerance", videoBitRateTolerance);
    getOption(options, "video_width", width);
    getOption(options, "video_height", height);
    getOption(options, "video_frame_rate", videoFrameRate);
    getOption(options, "video_gop_size", videoGopSize);
}

OptionsList StreamDescription::toOptionsList()
{
    OptionsList options;
    setOption(options, "codec", codec);
    setOption(options, "bit_rate", bitRate);
    setOption(options, "bit_rate_min", bitRateMin);
    setOption(options, "bit_rate_max", bitRateMax);
    setOption(options, "audio_channels", audioChannels);
    setOption(options, "audio_sample_rate", audioSampleRate);
    setOption(options, "video_strict", videoStrict);
    setOption(options, "video_buffer_size", videoBufferSize);
    setOption(options, "video_bit_rate_tolerance", videoBitRateTolerance);
    setOption(options, "video_width", width);
    setOption(options, "video_height", height);
    setOption(options, "video_frame_rate", videoFrameRate);
    setOption(options, "video_gop_size", videoGopSize);

    return options;
}

AVMediaType StreamDescription::getMediaType() const
{
    if (codec.empty())
    {
        return mediaType;
    }

    AVCodec *c = avcodec_find_decoder_by_name(codec.c_str());
    if (!c)
        c = avcodec_find_encoder_by_name(codec.c_str());

    if (c)
        return c->type;

    return mediaType;

    /*
    avcodec_get_type();

    const AVCodecDescriptor *desc = avcodec_descriptor_get_by_name(codec.c_str());

    if (!desc)
    {
        return mediaType;
    }

    return desc->type;
    */
}

void StreamDescription::reset()
{
    codec.clear();
    meta.clear();

    mapToIndex =
            bitRate =
            bitRateMin =
            bitRateMax =
            audioChannels =
            audioSampleRate =
            videoStrict =
            videoBufferSize =
            videoBitRateTolerance =
            width =
            height =
            videoGopSize = -1;

    timeBase =
            videoFrameRate =
            sampleAspectRatio = Rational();

    pixFormat = PIX_FMT_NONE;
    mediaType = AVMEDIA_TYPE_UNKNOWN;
}

} // ::transcoder
