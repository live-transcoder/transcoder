#include <pion/net/HTTPServer.hpp>

#include "httprbac.h"

namespace pion {
namespace net {


HTTPRbac::HTTPRbac(const PionRolesManagerPtr &rolesManager)
    : rolesManager(rolesManager)
{
}

HTTPRbac::~HTTPRbac()
{
}

void HTTPRbac::addRestrict(const string &resource, const string &role)
{
    boost::mutex::scoped_lock lock(resourceMutex);
    const string cleanResource(HTTPServer::stripTrailingSlash(resource));
    restrictList[role].insert(cleanResource);
}


void HTTPRbac::addPermit(const string &resource, const string &role)
{
    boost::mutex::scoped_lock lock(resourceMutex);
    const string cleanResource(HTTPServer::stripTrailingSlash(resource));
    permitList[role].insert(cleanResource);
}

void HTTPRbac::removePermit(const string &resource, const string &role)
{
    boost::mutex::scoped_lock lock(resourceMutex);
    const string cleanResource(HTTPServer::stripTrailingSlash(resource));
    if (permitList.count(role))
    {
        permitList[role].erase(cleanResource);
    }
}

void HTTPRbac::removeRestrict(const string &resource, const string &role)
{
    boost::mutex::scoped_lock lock(resourceMutex);
    const string cleanResource(HTTPServer::stripTrailingSlash(resource));
    if (restrictList.count(role))
    {
        restrictList[role].erase(cleanResource);
    }

}

void HTTPRbac::clearPermit(const string &resource)
{
    boost::mutex::scoped_lock lock(resourceMutex);
    const string cleanResource(HTTPServer::stripTrailingSlash(resource));
    AuthResourceAndRoleMap::iterator it = permitList.begin();
    for (; it != permitList.end(); ++it)
    {
        it->second.erase(cleanResource);
    }
}

void HTTPRbac::clearRestrict(const string &resource)
{
    boost::mutex::scoped_lock lock(resourceMutex);
    const string cleanResource(HTTPServer::stripTrailingSlash(resource));
    AuthResourceAndRoleMap::iterator it = restrictList.begin();
    for (; it != restrictList.end(); ++it)
    {
        it->second.erase(cleanResource);
    }
}

void HTTPRbac::clearAll()
{
    restrictList.clear();
    permitList.clear();
}

bool HTTPRbac::isAllowed(HTTPRequestPtr &request)
{
    string resource(HTTPServer::stripTrailingSlash(request->getResource()));
    string user(request->getUser()->getUsername());

    if (user.empty() || rolesManager->empty())
    {
        return false;
    }

    boost::mutex::scoped_lock lock(resourceMutex);
    set<string> userRoles = rolesManager->getUserRoles(user);
    userRoles.insert("*");

    bool isRestrict = findResource(restrictList, resource, userRoles);
    bool isPermit   = findResource(permitList,   resource, userRoles);

    if ((isRestrict && isPermit) || isPermit) // allow
    {
        return true;
    }
    else if (isRestrict)        // do not allow
    {
        return false;
    }

    return false;

}

bool HTTPRbac::findResource(const HTTPRbac::AuthResourceAndRoleMap &resources, const string &resource, const string &role)
{
    if (resources.count(role) == 0)
    {
        return false;
    }

    const AuthResourceSet &resourceSet = resources.at(role);
    if (resourceSet.empty() && !resource.empty())
    {
        return false;
    }

    AuthResourceSet::iterator it = resourceSet.find("*");
    if (it != resourceSet.end())
    {
        return true;
    }

    it = resourceSet.begin();
    for (; it != resourceSet.end(); ++it)
    {
        // check for a match if the first part of the strings match
        if (it->empty() || resource.compare(0, it->size(), *it) == 0)
        {
            // only if the resource matches exactly
            // or if resource is followed first with a '/' character
            if (resource.size() == it->size() || resource[it->size()]=='/')
            {
                return true;
            }
        }
    }

    return false;
}

bool HTTPRbac::findResource(const HTTPRbac::AuthResourceAndRoleMap &resources, const string &resource, const set<string> &roles)
{
    set<string>::iterator it = roles.begin();
    for (; it != roles.end(); ++it)
    {
        if (findResource(resources, resource, *it))
        {
            return true;
        }
    }

    return false;
}

}} // ::pion::net
