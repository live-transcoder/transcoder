#include <boost/thread/tss.hpp>
#include <boost/thread/mutex.hpp>

#include "logstream.h"

namespace logstream {

using namespace backend;

struct thread_data
{
    thread_data()
        : priority(level::info),
          use_global_logger(true),
          use_thread_logger(true)
    {}

    level::level priority;
    backend_ptr thread_logger;
    bool use_global_logger;
    bool use_thread_logger;
};


class logger_helper
{
public:
    static logger_helper& logger()
    {
        static logger_helper log;
        return log;
    }


    void set_threshold(level::level priority)
    {
        boost::mutex::scoped_lock lock(mutex);
        this->threshold = priority;
    }


    level::level get_threshold() const
    {
        boost::mutex::scoped_lock lock(mutex);
        return threshold;
    }


    void set_logging_backend(const backend_ptr& backend)
    {
        boost::mutex::scoped_lock lock(mutex);
        this->backend = backend;
    }


    backend_ptr get_logging_backend() const
    {
        boost::mutex::scoped_lock lock(mutex);
        return backend;
    }


    void push_message(level::level priority, const std::string& msg)
    {
        boost::mutex::scoped_lock lock(mutex);
        if (backend)
        {
            backend->push_message(priority, threshold, msg);
        }
    }


    logstream& get_logstream()
    {
        if (!streams.get())
        {
            streams.reset(new logstream(this));
        }
        return *streams;
    }

    thread_data& get_thread_data()
    {
        if (!data.get())
        {
            data.reset(new thread_data);
        }
        return *data;
    }

private:
    logger_helper()
        : threshold(level::info),
          backend(new console_backend)
    {}

private:
    mutable boost::mutex                    mutex;

    level::level                            threshold;
    backend_ptr                             backend;
    boost::thread_specific_ptr<logstream>   streams;
    boost::thread_specific_ptr<thread_data> data;
};



logger_meta::logger_meta(logger_helper *helper)
    : helper(helper)
{
}

void logger_meta::set_threshold(level::level threshold)
{
    helper->set_threshold(threshold);
}

level::level logger_meta::get_threshold() const
{
    return helper->get_threshold();
}

void logger_meta::set_logging_backend(const backend_ptr &backend)
{
    helper->set_logging_backend(backend);
}

backend_ptr logger_meta::get_logging_backend() const
{
    return helper->get_logging_backend();
}

void logger_meta::set_priority(level::level priority)
{
    helper->get_thread_data().priority = priority;
}

level::level logger_meta::get_priority() const
{
    return helper->get_thread_data().priority;
}

void logger_meta::set_thread_logging_backend(const backend_ptr &backend)
{
    helper->get_thread_data().thread_logger = backend;
}

backend_ptr logger_meta::get_thread_logging_backend()
{
    return helper->get_thread_data().thread_logger;
}

void logger_meta::set_global_logger_using(bool use)
{
    helper->get_thread_data().use_global_logger = use;
}

bool logger_meta::is_global_logger_used() const
{
    return helper->get_thread_data().use_global_logger;
}

void logger_meta::set_thread_logger_using(bool use)
{
    helper->get_thread_data().use_thread_logger = use;
}

bool logger_meta::is_thread_logger_used() const
{
    return helper->get_thread_data().use_thread_logger;
}

void logger_meta::push_message(level::level priority, const std::string &message)
{
    helper->push_message(priority, message);
}


logstream& logger(level::level priority)
{
    logstream &st = logger();
    st.set_priority(priority);
    return st;
}

logstream& logger()
{
    return logger_helper::logger().get_logstream();
}


namespace backend {

////////////////////////////////////////////////////////////////////////////////////////////////////
// Console backend
////////////////////////////////////////////////////////////////////////////////////////////////////
void console_backend::push_message(level::level priority, level::level threshold, const std::string &message)
{
    if (priority <= threshold)
    {
        switch (priority)
        {
            case level::silent:
                break;
            case level::fatal:
            case level::error:
                std::clog << message; // unbuffered STDERR
                break;
            case level::warning:
                std::cerr << message; // buffered STDERR
                break;
            case level::info:
            case level::extra:
            case level::debug:
            case level::trace:
                std::cout << message;
                break;
        }
    }
}


////////////////////////////////////////////////////////////////////////////////////////////////////
// String backend
////////////////////////////////////////////////////////////////////////////////////////////////////
string_backend::string_backend()
    : ignore_threshold(false)
{
}

void string_backend::push_message(level::level priority, level::level threshold, const std::string &message)
{
    if (priority <= threshold || ignore_threshold)
    {
        ss << message;
    }
}

std::string string_backend::str()
{
    return ss.str();
}

void string_backend::clear()
{
    ss.str(std::string());
}

void string_backend::set_threshold_ignoring(bool do_ignore)
{
    ignore_threshold = do_ignore;
}

} // ::backend
} // ::logger

