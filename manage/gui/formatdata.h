#ifndef FORMATDATA_H
#define FORMATDATA_H

#include <QtCore>
#include <QList>



class FormatData
{
public:
    FormatData();

    QString format;

    QString name;
    QString description;
    QString uri;
    QString transport;
    QString resource;
    QString roles;

};


class TranscoderData
{
public:

    QString hostPort;

    QString name;
    QString description;
    bool    enabled;
    QString source;
    QString sourceFormat;
    QList<FormatData> destinations;

    bool    active;
    bool    started;
};

struct MatchTranscoderByName
{
    MatchTranscoderByName(const QString &name) : name(name) {}

    bool operator()(const TranscoderData &data)
    {
        if (data.name == name)
            return true;
        else
            return false;
    }

private:
    QString name;
};


#endif // FORMATDATA_H
