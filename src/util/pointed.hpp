#ifndef POINTED_HPP
#define POINTED_HPP

#include <boost/shared_ptr.hpp>
#include <boost/weak_ptr.hpp>

/**
 * Simple macros to declate weak and shared pointer types for given type/class
 *
 * Example:
 * class Foo
 * {
 *     ...
 * };
 * DECLARE_PTR_TYPE(Foo)
 *
 * Now two typedefs is avail:
 * FooPtr - shared pointer
 * FooWPtr - weak pointer
 *
 */
#define DECLARE_PTR_TYPE(clazz) \
    typedef boost::shared_ptr<clazz> clazz ## Ptr; \
    typedef boost::weak_ptr<clazz>   clazz ## WPtr


#endif // POINTED_HPP
