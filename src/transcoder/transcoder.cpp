#include <iostream>
#include <algorithm>

#include <boost/algorithm/string.hpp>

#include "transcoder.h"
#include "applicationcontext.h"
#include "uri.h"

#include "transports/transport.h"
#include "log/logstream.h"

using namespace logstream;

namespace transcoder {


struct FormatDescriptionExists
{
public:
    FormatDescriptionExists(const FormatDescription &search)
    {
        searchValue = search;
    }

    bool operator () (const FormatDescription &value)
    {
        if (value.transport == searchValue.transport &&
            value.resource  == searchValue.resource)
        {
            return true;
        }

        return false;
    }

private:
    FormatDescription searchValue;
};


Transcoder::Transcoder()
    : isEnabledFlag(false)
{
}

Transcoder::Transcoder(const string &name, const string &description, bool isEnabled)
    : name(name),
      description(description),
      isEnabledFlag(isEnabled)
{
}

Transcoder::~Transcoder()
{
    try
    {
        stop();
    }
    catch (const std::exception &e)
    {}
}

static inline
std::string makeThreadName(const std::string& transcoderName, const std::string& suffix)
{
    const int maxThreadNameLength = 15; // from man prctl(2)
    std::string name = transcoderName;
    std::string suff = suffix;

    int size = transcoderName.length() + suffix.length();
    if (size > maxThreadNameLength)
    {

        if (suff.length() > (maxThreadNameLength - 8))
        {
            std::string last(name, name.length() - 3);
            name.resize(5);
            name += last;
            suff.resize(maxThreadNameLength - 8);
        }
        else
        {
            int delta = maxThreadNameLength - suff.length();
            int newSize = delta;
            std::string last;
            if (delta < 8)
            {
                last = std::string(name, name.length() - delta/2);
                newSize = delta - delta/2;
            }
            else
            {
                last = std::string(name, name.length() - 3);
                newSize = delta - 3;
            }

            name.resize(newSize);
            name += last;
        }
    }

    return (name + suff);
}

bool Transcoder::start()
{
    //
    // Checking
    //

    if (decoder)
    {
        logger(level::warning) << "Already running" << endl;
        return false;
    }


    if (inputUri.empty())
    {
        logger(level::warning) << "Can't start transcoder: empty input uri" << endl;
        return false;
    }


    vector<FormatDescription>::iterator it;
    for (it = encoderDesctiptions.begin(); it != encoderDesctiptions.end(); ++it)
    {
        if ((*it).transport.empty())
        {
            logger(level::warning) << "Empty transport for format: " << (*it).format << endl;
            return false;
        }

        if ((*it).resource.empty())
        {
            logger(level::warning) << "Empty resource for format: " << (*it).format << endl;
            return false;
        }

        if ((*it).format.empty())
        {
            logger(level::warning) << "Empty format: " << (*it).transport  << ", " <<  (*it).resource << endl;
            return false;
        }

        // TODO: add new transport registration check
#if 0
        boost::shared_ptr<EncodeThread> enc =
                transcoder::net::ResourcesManager::instance->getResource((*it).transport,
                                                                         (*it).resource);
        if (enc)
        {
            *log << "Encoder for given transport and resource already present: transport = " << (*it).transport
                 << ", resource = " << (*it).resource
                 << ", encoder: " << enc->getFormatDescription().format
                 << endl;
            return false;
        }
#endif
    }


    //
    // Activation
    //

    decoder = boost::shared_ptr<DecodeThread>(new DecodeThread());
    decoder->setStreamUri(inputUri);
    decoder->setEncodersWaitTime(ApplicationContext::instance->encodersWaitTime);
    decoder->setThreadName(makeThreadName(name, "dec"));
    if (!inputFormat.empty())
    {
        decoder->forceInputFormat(inputFormat);
    }

    for (it = encoderDesctiptions.begin(); it != encoderDesctiptions.end(); ++it)
    {
        boost::shared_ptr<EncodeThread> encoder(new EncodeThread(*it));
        encoder->setQueueSize(ApplicationContext::instance->encodeBufferSize);
        encoder->setThreadName(makeThreadName(name, (*it).format));
        encoder->start();
        encoders.push_back(encoder);

        transport::BaseTransport::registerResource((*it).transport,
                                                   (*it).resource,
                                                   encoder);

        if ((*it).transport == "http")
        {
            list<string> roles;
            boost::algorithm::split(roles, (*it).roles, boost::algorithm::is_any_of(","), boost::algorithm::token_compress_on);
            list<string>::iterator j = roles.begin();
            for (; j != roles.end(); ++j)
            {
                //ApplicationContext::instance->httpAuth->addPermit("/media" + (*it).resource, *j);
            }
        }

        decoder->subscribe(encoder);
    }

    decoder->start();

    return true;
}


static
void stop_encoder(EncodeThreadWPtr wthread)
{
    EncodeThreadPtr thread = wthread.lock();
    if (!thread)
        return;

    thread->stop();
    if (!thread->timed_join(boost::posix_time::seconds(5)))
    {
        thread->kill();
        thread->join();
    }

    // save some data before free
    const string& format    = thread->getFormatDescription().format;
    const string& transport = thread->getFormatDescription().transport;
    const string& resource  = thread->getFormatDescription().resource;
    const string& uri       = thread->getFormatDescription().uri;

    transport::BaseTransport::unregisterResource(transport, resource);

    Uri uriInfo(uri);
    if (uriInfo.getTransport() == "http" || uriInfo.getTransport() == "https")
    {
        //ApplicationContext::instance->httpAuth->clearPermit(uriInfo.getPath());
    }


    logger(level::info) << "Encoder stoped: "
                        << format
                        << " - "
                        << transport
                        << " - "
                        << resource
                        << endl;
}


bool Transcoder::stop()
{
    if (!decoder || !decoder->isRunning())
    {
        logger(level::warning) << "Already stoped" << endl;
        return false;
    }

    decoder->stop();
    if (!decoder->timed_join(boost::posix_time::seconds(5)))
    {
        decoder->kill();
        decoder->join();
    }
    logger(level::info) << "Decoder stoped: " << inputUri << endl;

    boost::thread_group threads;
    list<EncodeThreadPtr>::iterator it;
    for (it = encoders.begin(); it != encoders.end(); ++it)
    {
        threads.create_thread(boost::bind(stop_encoder, EncodeThreadWPtr(*it)));
    }

    threads.join_all();

    encoders.clear();
    decoder.reset();

    return true;
}

bool Transcoder::restart()
{
    if (!stop())
    {
        return false;
    }

    if (!start())
    {
        return false;
    }

    return true;
}

bool Transcoder::isRunning()
{
    return (decoder ? true : false);
}

void Transcoder::setInput(const string &uri, const string &inputFormat)
{
    this->inputUri = uri;
    this->inputFormat = inputFormat;
}

const string &Transcoder::getInput() const
{
    return inputUri;
}

const string &Transcoder::getInputFormat() const
{
    return inputFormat;
}

bool Transcoder::isActive() const
{
    bool isActive = false;

    if (!decoder)
    {
        return false;
    }

    isActive = decoder->isActive();

    boost::xtime currentTime = boost::get_xtime(boost::get_system_time());
    const boost::xtime& lastFrameTime = decoder->getLastFrameTime();

    int64_t timeDelta = currentTime.sec - lastFrameTime.sec; // use only seconds part
    if (timeDelta > 10)
    {
        isActive = false;
    }

    return isActive;
}

bool Transcoder::addEncoder(const FormatDescription &desc)
{
    FormatDescriptions::iterator it = findDescription(desc);
    if (it != encoderDesctiptions.end())
    {
        logger(level::warning) << "Encoder for given transport and resource already present: "
                               << desc.transport << ", "
                               << desc.resource
                               << ", exist encoder: " << (*it).format << endl;
        return false;
    }

    if (desc.transport.empty())
    {
        logger(level::warning) << "Encoder has empty transport" << endl;
        return false;
    }

    if (desc.resource.empty())
    {
        logger(level::warning) << "Encoder has empty resource" << endl;
        return false;
    }

    encoderDesctiptions.push_back(desc);
    return true;
}

bool Transcoder::removeEncoder(const FormatDescription &desc)
{
    FormatDescriptions::iterator it = findDescription(desc);
    if (it != encoderDesctiptions.end())
    {
        encoderDesctiptions.erase(it);
        return true;
    }

    logger(level::warning) << "Can't found given description: " << desc.format << ", " << desc.transport << ", " << desc.resource << endl;
    return false;
}

bool Transcoder::removeEncoder(uint32_t id)
{
    if (id < encoderDesctiptions.size())
    {
        encoderDesctiptions.erase(encoderDesctiptions.begin() + id);
        return true;
    }

    logger(level::warning) << "Description with given id does not exists: " << id << endl;
    return false;
}

void Transcoder::removeEncoders()
{
    encoderDesctiptions.clear();
}

const vector<FormatDescription> &Transcoder::getEncoders() const
{
    return encoderDesctiptions;
}

Transcoder::FormatDescriptions::iterator Transcoder::findDescription(const FormatDescription &desc)
{
    return std::find_if(encoderDesctiptions.begin(),
                        encoderDesctiptions.end(),
                        FormatDescriptionExists(desc));

}

} // ::transcoder
