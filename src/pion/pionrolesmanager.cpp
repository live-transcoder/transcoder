#include "pionrolesmanager.h"

namespace pion {
namespace net {

using namespace std;

bool PionRolesManager::addUser(const string &role, const string &user)
{
    if (role.empty() || user.empty())
    {
        return false;
    }

    userRoles[role].insert(user);
    return true;
}

bool PionRolesManager::removeUser(const string &role, const string &user)
{
    if (role.empty() || user.empty())
    {
        return false;
    }

    if (userRoles.count(role))
    {
        userRoles[role].erase(user);
        return true;
    }

    return false;
}

bool PionRolesManager::removeRole(const string &role)
{
    if (role.empty())
    {
        return false;
    }

    if (userRoles.count(role))
    {
        userRoles.erase(role);
        return true;
    }
    return false;
}

bool PionRolesManager::clearUsers(const string &role)
{
    if (role.empty())
    {
        return false;
    }

    if (userRoles.count(role))
    {
        userRoles[role].clear();
        return true;
    }

    return false;
}

void PionRolesManager::clearAll()
{
    userRoles.clear();
}

bool PionRolesManager::isUserInRole(const string &role, const string &user)
{
    if (role.empty() || user.empty())
    {
        return false;
    }

    if (userRoles.count(role) && userRoles.at(role).count(user))
    {
        return true;
    }

    return false;
}

bool PionRolesManager::isUserInRole(const set<string> &roles, const string &user)
{
    if (roles.empty() || user.empty())
    {
        return false;
    }

    set<string>::iterator it;
    for (it = roles.begin(); it != roles.end(); ++it)
    {
        bool result = isUserInRole(*it, user);
        if (result)
            return true;
    }

    return false;
}

set<string> PionRolesManager::getUserRoles(const string &user)
{
    set<string> result;
    map< string, set<string> >::iterator it;
    for (it = userRoles.begin(); it != userRoles.end(); ++it)
    {
        if ((*it).second.count(user))
        {
            result.insert((*it).first);
        }
    }

    return result;
}





}} // ::pion::net
