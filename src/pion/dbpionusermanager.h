#ifndef DBPIONUSERMANAGER_H
#define DBPIONUSERMANAGER_H

#include <string>
#include <soci/soci.h>
#include <pion/net/PionUser.hpp>

namespace pion {
namespace net {

using namespace std;

class DbPionUserManager : public PionUserManager
{
public:
    explicit DbPionUserManager(soci::connection_pool *pool);

    bool    empty();
    virtual PionUserPtr getUser(const string &username);
    virtual PionUserPtr getUser(const string &username, const string &password);

    const pair<bool, string> &getDatabaseError() const;

protected:
    void cleanError();
    void setError(const string &text);

private:   
    soci::connection_pool *dbpool;
    pair<bool, string> databaseError;
};

typedef boost::shared_ptr<DbPionUserManager> DbPionUserManagerPtr;

}} // ::pion::net

#endif // DBPIONUSERMANAGER_H
