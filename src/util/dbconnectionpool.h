#ifndef DBCONNECTIONPOOL_H
#define DBCONNECTIONPOOL_H

#include <soci/connection-pool.h>

/**
 * FIXME: complete this class!
 */
class DbConnectionPool : public soci::connection_pool
{
public:
    // Fixed size pool
    DbConnectionPool(std::size_t size);
    DbConnectionPool(std::size_t initialSize,
                     std::size_t maxSize,
                     std::size_t increaseStep = 1);

    virtual ~DbConnectionPool();

    soci::session & at(std::size_t pos);

    std::size_t lease();
    bool try_lease(std::size_t &pos, int timeout);
    void give_back(std::size_t pos);

private:
    std::size_t currentSize;
    std::size_t initialSize;
    std::size_t maxSize;
    std::size_t increaseStep;

    struct connection_pool_impl;
    connection_pool_impl * pimpl_;
};

#endif // DBCONNECTIONPOOL_H
