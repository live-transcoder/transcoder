#ifndef TRANSCODER_TRANSPORT_FILE_H
#define TRANSCODER_TRANSPORT_FILE_H

#include <boost/thread/mutex.hpp>

#include "transport.h"
#include "muxer.h"

namespace transcoder {
namespace transport {

/**
 * Simple file write transport. It open resource as file and store content to it.
 *
 * You must register it before use, like:
 *    transcoder::transport::File::registerTransport();
 *
 */
class File : public Transport<File>
{
public:
    virtual BaseTransport::ResourceRegistrationStatus
    registerResource(const string &resource,
                     const ProducerPtr &producer,
                     const Options &options);

    virtual bool unregisterResource(const string &resource);

    virtual const std::string getTransportName();

private:
    boost::mutex                    muxersMutex;
    std::map<std::string, MuxerPtr> muxers;
};

} // namespace transport
} // namespace transcoder

#endif // TRANSCODER_TRANSPORT_FILE_H
