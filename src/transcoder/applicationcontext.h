#ifndef RESOURCES_H
#define RESOURCES_H

#include <stdint.h>

#include <string>
#include <soci/soci.h>

#include "boost/utility/mutexed_singleton.hpp"

#include "httpserver.h"

namespace transcoder {

using namespace std;

class ApplicationContext : public boost::mutexed_singleton<ApplicationContext>
{
public:
    ApplicationContext(boost::restricted);

    /// Database connection
    string   connection;

    /// HTTP server port
    uint16_t httpPort;

    /// Maximum simultaneously HTTP connections
    uint32_t maxHttpConnections;

    /// RTSP server port
    uint16_t rtspPort;

    /// Daemonize flag
    bool     daemonize;

    /// Log file for daemon mode, default /dev/null
    string   logFile;

    /// Ecoder buffer size (in frames, min 5)
    uint32_t encodeBufferSize;

    /// Send buffer size (in packets, min 5)
    uint32_t sendBufferSize;

    /// Encoders wait timeout (in msec), if timeout will be occured, frame will dropped
    int64_t  encodersWaitTime;

    /// FFMPEG log level - string representation
    string   ffmpegLogLevel;

    /// HTTP/RTSP user manager
    //DbPionUserManagerPtr userManager;

    /// HTTP/RTSP roles manager
    //DbPionRolesManagerPtr rolesManager;

    /// HTTP/RTSP auth handler
    //HTTPBasicRbacAuthPtr  httpAuth;

    /// HTTP transport instance
    transport::http::HttpServerPtr httpServer;

    /// Verbosity level
    int verbosity;

    /// Unique transcoder ID to use ony data base with different amount of transcoders
    /// In future, transcoding tasks can be migrated via different trasncoders
    int transcoderId;

    /// Database connection pool
    soci::connection_pool *pool;
};


} // ::transcoder

#endif // RESOURCES_H
