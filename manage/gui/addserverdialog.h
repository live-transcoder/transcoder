#ifndef ADDSERVERDIALOG_H
#define ADDSERVERDIALOG_H

#include "ui_addserverdialog.h"

class AddServerDialog : public QDialog, private Ui::AddServerDialog
{
    Q_OBJECT
    
public:
    explicit AddServerDialog(QWidget *parent = 0);
    
    QString getHostAndPort() const;
    QString getLogin() const;
    QString getPassword() const;
    QString getTitle() const;

    void    setHostAndPort(const QString &hostPort);
    void    setLogin(const QString &login);
    void    setPassword(const QString &password);
    void    setTitle(const QString &title);

protected:
    void changeEvent(QEvent *e);
};

#endif // ADDSERVERDIALOG_H
