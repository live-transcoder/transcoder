#ifndef MUTEX_HPP
#define MUTEX_HPP

#include <exception>

#include <boost/thread.hpp>
#include <boost/exception/all.hpp>


#if defined(BOOST_THREAD_PLATFORM_PTHREAD)

/* On linux boost::mutex on lock() operation can raise ABORT signal:
 *   boost __pthread_tpp_change_priority: Assertion `new_prio == -1 || (new_prio >= __sched_fifo_min_prio && new_prio <= __sched_fifo_max_prio)' failed.
 *
 * So, I redeclare boost::mutex as Mutex with posible fix:
 *  - declare variable with type: pthread_mutexattr_t attr;
 *  - init it with pthread_mutexattr_init(&attr);
 *  - pass it to pthread_mutexattr_init() as second param instead NULL;
 */
class Mutex
{
private:
    Mutex(Mutex const&);
    Mutex& operator=(Mutex const&);
    pthread_mutex_t     m;
    pthread_mutexattr_t attr;
public:
    Mutex()
    {
        int const res0 = pthread_mutexattr_init(&attr);
        if (res0)
        {
            boost::throw_exception(boost::thread_resource_error());
        }

        int const res = pthread_mutex_init(&m, &attr);
        if(res)
        {
            boost::throw_exception(boost::thread_resource_error());
        }
    }
    ~Mutex()
    {
        int ret;
        do
        {
            ret = pthread_mutex_destroy(&m);
            if (ret == 0)
                ret = pthread_mutexattr_destroy(&attr);
        } while (ret == EINTR);
    }

    void lock()
    {
        int res;
        do
        {
            res = pthread_mutex_lock(&m);
        } while (res == EINTR);
        if(res)
        {
            boost::throw_exception(boost::lock_error(res));
        }
    }

    void unlock()
    {
        int ret;
        do
        {
            ret = pthread_mutex_unlock(&m);
        } while (ret == EINTR);
        BOOST_VERIFY(!ret);
    }

    bool try_lock()
    {
        int res;
        do
        {
            res = pthread_mutex_trylock(&m);
        } while (res == EINTR);
        if(res && (res!=EBUSY))
        {
            boost::throw_exception(boost::lock_error(res));
        }

        return !res;
    }

    typedef pthread_mutex_t* native_handle_type;
    native_handle_type native_handle()
    {
        return &m;
    }

    typedef boost::unique_lock<Mutex> scoped_lock;
    typedef boost::detail::try_lock_wrapper<Mutex> scoped_try_lock;
};



class ConditionVariable
{
private:
    pthread_mutex_t     internal_mutex;
    pthread_mutexattr_t attr;
    pthread_cond_t      cond;

    ConditionVariable(ConditionVariable&);
    ConditionVariable& operator=(ConditionVariable&);

public:
    ConditionVariable()
    {
        int const res0 = pthread_mutexattr_init(&attr);
        if (res0)
        {
            boost::throw_exception(boost::thread_resource_error());
        }
        int const res=pthread_mutex_init(&internal_mutex, &attr);
        if(res)
        {
            boost::throw_exception(boost::thread_resource_error());
        }
        int const res2=pthread_cond_init(&cond,NULL);
        if(res2)
        {
            BOOST_VERIFY(!pthread_mutex_destroy(&internal_mutex));
            boost::throw_exception(boost::thread_resource_error());
        }
    }
    ~ConditionVariable()
    {
        BOOST_VERIFY(!pthread_mutex_destroy(&internal_mutex));
        BOOST_VERIFY(!pthread_mutexattr_destroy(&attr));
        int ret;
        do {
          ret = pthread_cond_destroy(&cond);
        } while (ret == EINTR);
        BOOST_VERIFY(!ret);
    }

    inline
    void wait(boost::unique_lock<Mutex>& m)
    {
        int res=0;
        {
            boost::thread_cv_detail::lock_on_exit<boost::unique_lock<Mutex> > guard;
            boost::detail::interruption_checker check_for_interruption(&internal_mutex,&cond);
            guard.activate(m);
            res = pthread_cond_wait(&cond,&internal_mutex);
        }
        boost::this_thread::interruption_point();
        if(res)
        {
            boost::throw_exception(boost::condition_error());
        }
    }

    template<typename predicate_type>
    void wait(boost::unique_lock<Mutex>& m,predicate_type pred)
    {
        while(!pred()) wait(m);
    }

    inline bool timed_wait(boost::unique_lock<Mutex>& m,
                           boost::system_time const& wait_until)
    {
        boost::thread_cv_detail::lock_on_exit<boost::unique_lock<Mutex> > guard;
        int cond_res;
        {
            boost::detail::interruption_checker check_for_interruption(&internal_mutex,&cond);
            guard.activate(m);
            struct timespec const timeout = boost::detail::get_timespec(wait_until);
            cond_res = pthread_cond_timedwait(&cond,&internal_mutex,&timeout);
        }
        boost::this_thread::interruption_point();
        if(cond_res==ETIMEDOUT)
        {
            return false;
        }
        if(cond_res)
        {
            boost::throw_exception(boost::condition_error());
        }
        return true;
    }

    bool timed_wait(boost::unique_lock<Mutex>& m, boost::xtime const& wait_until)
    {
        return timed_wait(m, boost::system_time(wait_until));
    }

    template<typename duration_type>
    bool timed_wait(boost::unique_lock<Mutex>& m, duration_type const& wait_duration)
    {
        return timed_wait(m, boost::get_system_time() + wait_duration);
    }

    template<typename predicate_type>
    bool timed_wait(boost::unique_lock<Mutex>& m, boost::system_time const& wait_until, predicate_type pred)
    {
        while (!pred())
        {
            if(!timed_wait(m, wait_until))
                return pred();
        }
        return true;
    }

    template<typename predicate_type>
    bool timed_wait(boost::unique_lock<Mutex>& m, boost::xtime const& wait_until, predicate_type pred)
    {
        return timed_wait(m, boost::system_time(wait_until), pred);
    }

    template<typename duration_type,typename predicate_type>
    bool timed_wait(boost::unique_lock<Mutex>& m, duration_type const& wait_duration, predicate_type pred)
    {
        return timed_wait(m, boost::get_system_time() + wait_duration, pred);
    }

    typedef pthread_cond_t* native_handle_type;
    native_handle_type native_handle()
    {
        return &cond;
    }

    void notify_one()
    {
        boost::pthread::pthread_mutex_scoped_lock internal_lock(&internal_mutex);
        BOOST_VERIFY(!pthread_cond_signal(&cond));
    }


    void notify_all()
    {
        boost::pthread::pthread_mutex_scoped_lock internal_lock(&internal_mutex);
        BOOST_VERIFY(!pthread_cond_broadcast(&cond));
    }
};

#else // Use boost::mutex itself
typedef boost::mutex Mutex;
typedef boost::condition_variable ConditionVariable;
#endif

class Lockable
{
public:
    virtual ~Lockable() {}

    void lock() { mutex.lock(); }
    void unlock() { mutex.unlock(); }
    bool try_lock() { mutex.try_lock(); }

private:
    Mutex mutex;
};


template<typename Lockable>
class ScopedTryLock
{
public:
    explicit ScopedTryLock(Lockable *mutex, bool raiseException = false, const std::string &exceptionText = std::string())
        : mutex(mutex)
    {
        isLockedFlag = this->mutex->try_lock();
        if (!isLockedFlag && raiseException)
        {
            if (exceptionText.empty())
                throw std::runtime_error("Can't lock mutex");
            else
                throw std::runtime_error(exceptionText);
        }
    }

    ~ScopedTryLock()
    {
        if (isLockedFlag)
            mutex->unlock();
    }

    bool operator()()
    {
        return isLockedFlag;
    }

    bool operator!()
    {
        return !isLockedFlag;
    }

private:
    bool isLockedFlag;
    Lockable *mutex;
};

#endif // MUTEX_HPP
