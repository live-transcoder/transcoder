#include <iostream>
#include <map>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/foreach.hpp>

#include "transcoderdao.h"
#include "applicationcontext.h"
#include "util/dbutils.h"

#include "log/logstream.h"

using namespace logstream;

namespace transcoder {

bool ptreeToFormatDescription(const boost::property_tree::ptree &node, FormatDescription &desc)
{
    try
    {
#define GET(name, type, def) desc.name = node.get<type>(#name, def)

        GET(format,                string,   string());
//        GET(audioCodec,            string,   string());
//        GET(videoCodec,            string,   string());
        GET(uri,                   string,   string());
        GET(transport,             string,   string());
        GET(resource,              string,   string());
//        GET(audioBitRate,          int,      -1);
//        GET(audioChannels,         int,      -1);
//        GET(audioSampleRate,       int,      -1);
//        GET(videoBitRateMin,       int,      -1);
//        GET(videoBitRateMax,       int,      -1);
//        GET(videoStrict,           int,      -1);
//        GET(videoBufferSize,       int,      -1);
//        GET(videoBitRateTolerance, int,      -1);
//        GET(width,                 int,      -1);
//        GET(height,                int,      -1);
//        GET(videoGopSize,          int,      -1);
//        GET(videoFrameRate,        Rational, Rational());
        GET(isEnabled,             bool,     false);
        GET(roles,                 string,   string());

#undef GET

        // Meta data
        if (node.get_child_optional("meta").is_initialized())
        {
            BOOST_FOREACH(const boost::property_tree::ptree::value_type &meta,
                          node.get_child("meta"))
            {
                desc.meta[meta.first] = meta.second.get_value<string>(string());
            }
        }
    }
    catch (const exception &e)
    {
        logger(level::error) << "Can't parse format description node: " << e.what() << endl;
        return false;
    }

    return true;
}


bool formatDescriptionToPtree(const FormatDescription &desc, boost::property_tree::ptree &dest)
{
    try
    {
#define PUT(name, type, def) if (desc.name != def) dest.put<type>(#name, desc.name)

        PUT(format,                string,   string());
//        PUT(audioCodec,            string,   string());
//        PUT(videoCodec,            string,   string());
        PUT(uri,                   string,   string());
        PUT(transport,             string,   string());
        PUT(resource,              string,   string());
//        PUT(audioBitRate,          int,      -1);
//        PUT(audioChannels,         int,      -1);
//        PUT(audioSampleRate,       int,      -1);
//        PUT(videoBitRateMin,       int,      -1);
//        PUT(videoBitRateMax,       int,      -1);
//        PUT(videoStrict,           int,      -1);
//        PUT(videoBufferSize,       int,      -1);
//        PUT(videoBitRateTolerance, int,      -1);
//        PUT(width,                 int,      -1);
//        PUT(height,                int,      -1);
//        PUT(videoGopSize,          int,      -1);
//        PUT(videoFrameRate,        Rational, Rational());
        PUT(isEnabled,             bool,     false);
        PUT(roles,                 string,   string());

#undef PUT

        boost::property_tree::ptree meta;
        map<string, string>::const_iterator it = desc.meta.begin();
        if (it != desc.meta.end())
        {
            for (; it != desc.meta.end(); ++it)
            {
                meta.put((*it).first, (*it).second);
            }

            dest.put_child("meta", meta);
        }
    }
    catch (const exception &e)
    {
        logger(level::error) << "Can't conevert format description to ptree: " << e.what() << endl;
        return false;
    }

    return true;
}


TranscoderDAO::TranscoderDAO(soci::connection_pool *pool)
    : dbpool(pool)
{
}


TranscoderDAO::~TranscoderDAO()
{
}


bool TranscoderDAO::isTranscoderExists(const string &name)
{
    bool result = false;

    if (!dbpool)
        return false;

    try
    {
        soci::session sql(*dbpool);
        checkDbSession(sql);

        sql << "SELECT name FROM transcoders WHERE name = :name", soci::use(name);

        if (sql.got_data())
        {
            result = true;
        }
    }
    catch (const exception &e)
    {
    }

    return result;
}


bool TranscoderDAO::putTranscoder(const TranscoderPtr &transcoder)
{
    if (!transcoder || transcoder->getName().empty())
    {
        return false;
    }

    if (!dbpool)
        return false;

    bool exists = isTranscoderExists(transcoder->getName());

    try
    {
        string source;
        string destination;

        //
        {
            FormatDescription desc;
            desc.uri    = transcoder->getInput();
            desc.format = transcoder->getInputFormat();
            boost::property_tree::ptree pt;
            formatDescriptionToPtree(desc, pt);

            stringstream ss;
            boost::property_tree::json_parser::write_json(ss, pt);
            //ss.flush();

            source = ss.str();
        }

        logger(level::debug) << "Source JSON:\n" << source << endl;

        {
            vector<FormatDescription>::const_iterator it = transcoder->getEncoders().begin();
            boost::property_tree::ptree pt;

            boost::property_tree::ptree formats;
            for (; it != transcoder->getEncoders().end(); ++it)
            {
                boost::property_tree::ptree format;
                formatDescriptionToPtree(*it, format);
                formats.push_back( make_pair("", format) );
            }

            pt.put_child("formats", formats);

            stringstream ss;
            boost::property_tree::json_parser::write_json(ss, pt);

            destination = ss.str();
        }

        logger(level::debug) << "Desctinations JSON:\n" << destination << endl;

        soci::session sql(*dbpool);
        checkDbSession(sql);

        if (!exists)
        {
            sql << "INSERT INTO transcoders (name, description, enabled, source, destination, serverid)"
                   " VALUES (:name, :desc, :enabled, :src, :dst, :serverid)",
                    soci::use(transcoder->getName(), "name"),
                    soci::use(transcoder->getDescription(), "desc"),
                    soci::use((int)transcoder->isEnabled(), "enabled"),
                    soci::use(source, "src"),
                    soci::use(destination, "dst"),
                    soci::use(ApplicationContext::instance->transcoderId, "serverid");
        }
        else
        {
            sql << "UPDATE transcoders SET description = :desc, enabled = :enabled, source = :src, destination = :dst"
                   " WHERE name = :name AND serverid = :serverid",
                    soci::use(transcoder->getName(), "name"),
                    soci::use(transcoder->getDescription(), "desc"),
                    soci::use((int)transcoder->isEnabled(), "enabled"),
                    soci::use(source, "src"),
                    soci::use(destination, "dst"),
                    soci::use(ApplicationContext::instance->transcoderId, "serverid");
        }

    }
    catch (const exception &e)
    {
        logger(level::error) << "SQL error: " << e.what() << endl;
        return false;
    }

    return true;
}


TranscoderPtr TranscoderDAO::getTranscoder(const string &name)
{
    TranscoderPtr result;
    list<TranscoderPtr> lst = getTranscoders(name);

    if (!lst.empty())
    {
        // take only first value (from begin iterator)
        result = *(lst.begin());
    }

    return result;
}

list<TranscoderPtr> TranscoderDAO::getTranscoders(const string &transcoderName)
{
    list<TranscoderPtr> result;

    if (!dbpool)
        return result;

    try
    {
        soci::session sql(*dbpool);
        checkDbSession(sql);
        soci::details::prepare_temp_type p(sql);

        if (transcoderName.empty())
        {
            p = sql.prepare << "SELECT name, description, enabled, source, destination"
                                " FROM transcoders WHERE serverid = :serverid",
                    soci::use(ApplicationContext::instance->transcoderId, "serverid");
        }
        else
        {
            p = sql.prepare << "SELECT name, description, enabled, source, destination"
                                " FROM transcoders WHERE name = :name AND serverid = :serverid",
                    soci::use(transcoderName, "name"),
                    soci::use(ApplicationContext::instance->transcoderId, "serverid");
        }

        soci::rowset<soci::row> rows(p);
        soci::rowset<soci::row>::iterator it = rows.begin();
        if (it == rows.end())
        {
            return result;
        }


        for (; it != rows.end(); ++it)
        {
            string name;
            string description;
            int    isEnabled;
            string source;
            string destination;

            (*it) >> name >> description >> isEnabled >> source >> destination;

            TranscoderPtr transcoder = TranscoderPtr(new Transcoder(name, description, !!isEnabled));

            // Read source
            {
                stringstream ss;
                ss << source;

                boost::property_tree::ptree pt;
                boost::property_tree::json_parser::read_json(ss, pt);

                FormatDescription desc;
                ptreeToFormatDescription(pt, desc);

                logger(level::debug) << "INPUT FORMAT: " << desc.format << endl;
                transcoder->setInput(desc.uri, desc.format);
            }

            // Read destinations
            {
                stringstream ss;
                ss << destination;

                boost::property_tree::ptree pt;
                boost::property_tree::json_parser::read_json(ss, pt);

                boost::optional<boost::property_tree::ptree&> formats = pt.get_child_optional("formats");
                if (formats.is_initialized())
                {
                    BOOST_FOREACH(const boost::property_tree::ptree::value_type &child,
                                  formats.get().get_child(""))
                    {
                        FormatDescription desc;
                        if (ptreeToFormatDescription(child.second, desc))
                            transcoder->addEncoder(desc);
                    }
                }
            }


            result.push_back(transcoder);

        }

    }
    catch (const exception &e)
    {
        logger(level::error) << "SQL error: " << e.what() << endl;
    }

    return result;
}


bool TranscoderDAO::putTranscoders(const list<TranscoderPtr> transcoders)
{
    list<TranscoderPtr>::const_iterator it = transcoders.begin();
    int result = 0;
    for (; it != transcoders.end(); ++it)
    {
        result += putTranscoder(*it);
    }

    return (result == transcoders.size());
}


bool TranscoderDAO::delTranscoder(const string &name)
{
    if (!dbpool)
        return false;
    soci::session sql(*dbpool);
    checkDbSession(sql);

    try
    {
        sql << "DELETE FROM transcoders WHERE name = :name AND serverid = :serverid",
                soci::use(name, "name"),
                soci::use(ApplicationContext::instance->transcoderId, "serverid");
    }
    catch (const exception &e)
    {
        logger(level::error) << "SQL error: " << e.what() << endl;
        return false;
    }

    return true;
}

bool TranscoderDAO::delTranscoders(const set<string> &names)
{
    set<string>::iterator it = names.begin();
    int result = 0;
    for (; it != names.end(); ++it)
    {
        logger(level::info) << "[DAO] Delete transcoder: " << (*it) << endl;
        result += delTranscoder(*it);
    }

    return (result == names.size());
}


} // ::transcoder
