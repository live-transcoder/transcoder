/** =============================================================================================
  
    This file is a part of "%ProjectName%" project
    http://htrd.su
    
    @date   2012-4-7
    @brief
    
    Copyright (C) 2002-2012 by Alexander 'hatred' Drozdov <adrozdoff@gmail.com>
    
    This program is free software; you can redistribute it and/or modify
    it under the terms of the version 2 of GNU General Public License as
    published by the Free Software Foundation.
    
    For more information see LICENSE and LICENSE.ru files
    
   ============================================================================================== */

#ifndef THREAD_H
#define THREAD_H

#include <iostream>
#include <exception>
#include <new>

#include <boost/thread.hpp>

/**
 * Set thread name for given thread. Note that Linux limit thread name to 16 chars.
 * @param thr        thread to change name
 * @param threadName new thread name
 */
inline void setThreadName(boost::thread& thr, const std::string &threadName)
{
#ifdef __linux__
    boost::thread::native_handle_type handle = thr.native_handle();

    if (handle == 0 || threadName.empty())
    {
        return;
    }

    pthread_setname_np(handle, threadName.c_str());
#endif
}

/**
 * Set thread name for calling thread. Note that linux limit thread name to 16 chars.
 * @param threadName
 */
inline void setThreadName(const std::string &threadName)
{
#ifdef __linux__
    if (threadName.empty())
    {
        return;
    }

    pthread_setname_np(pthread_self(), threadName.c_str());
#endif
}


template<class T>
class Thread
{
public:
    Thread()
        : thread(0),
          isRun(false)
    {}

    virtual ~Thread()
    {
        kill();
        delete thread;
    }

    void start()
    {
        if (isRun)
        {
            return;
        }

        T* extender = dynamic_cast<T*>(this);
        assert(extender);

        kill();

        if (thread)
        {
            delete thread;
            thread = 0;
        }

        thread = new boost::thread(boost::bind(&Thread::runIt, this, boost::ref(*extender)));
        id = thread->get_id();
    }

    void join()
    {
        if (!thread)
        {
            return;
        }

        thread->join();
    }


    template<typename TimeDuration>
    inline bool timed_join(TimeDuration const& rel_time)
    {
        if (!thread)
        {
            return false;
        }
        return thread->timed_join(rel_time);
    }

    void kill()
    {
        if (!thread)
        {
            return;
        }

        thread->interrupt();
    }

    bool isFinished() const
    {
        return (!isRun);
    }


    inline std::string getThreadName() const
    {
        return threadName;
    }

    inline bool setThreadName(const std::string& name)
    {
        if (name.length() > 16)
        {
            return false;
        }

        threadName = name;
        setThreadName();
        return true;
    }

    virtual void run() = 0;

private:
    void setThreadName()
    {
        if (!thread)
            return;
        ::setThreadName(*thread, threadName);
    }

    void runIt(T& extender)
    {
        try
        {
            isRun = true;
            setThreadName();
            extender.run();
            isRun = false;
            id    = boost::thread::id();
        }
        catch (boost::thread_interrupted &e)
        {
            isRun = false;
        }
        catch (std::exception &e)
        {
            std::cout << "Error on thread execution: " << e.what() << std::endl;
        }
    }

private:
    boost::thread     *thread;
    bool               isRun;
    boost::thread::id  id;
    std::string        threadName;
};



class Condition
{
public:
    Condition()
        : flag(false)
    {}

    virtual ~Condition()
    {
        wakeUpAll();
    }

    void wait()
    {
        boost::shared_lock<boost::shared_mutex> lock(mutex);
        while (flag == false)
            cv.wait(lock);

        boost::upgrade_lock<boost::shared_mutex> guard(mutex, boost::try_to_lock);
        if (guard.owns_lock())
        {
            flag = false;
        }
    }

    void wakeUpAll()
    {
        boost::unique_lock<boost::shared_mutex> lock(mutex);
        flag = true;
        cv.notify_all();
    }

    void wakeUpOne()
    {
        boost::unique_lock<boost::shared_mutex> lock(mutex);
        flag = true;
        cv.notify_one();
    }

private:
    boost::shared_mutex           mutex;
    boost::condition_variable_any cv;
    bool                          flag;
};

#endif // THREAD_H
