#ifndef PREVIEWDIALOG_H
#define PREVIEWDIALOG_H

#include <Qt>

#include "ui_previewdialog.h"

class PreviewDialog : public QDialog, private Ui::PreviewDialog
{
    Q_OBJECT
    
public:
    explicit PreviewDialog(QWidget *parent = 0);
    
    void setLogin(const QString &login);
    void setPassword(const QString &password);

    void setSources(const QStringList &sources);

protected:
    void changeEvent(QEvent *e);
private slots:
    void on_sourcesListWidget_itemDoubleClicked(QListWidgetItem *item);

private:
    QString login;
    QString password;
};

#endif // PREVIEWDIALOG_H
