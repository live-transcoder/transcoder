#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QMap>
#include <QTimer>
#include <QSortFilterProxyModel>


#include "ui_mainwindow.h"
#include "formatdata.h"
#include "previewdialog.h"
#include "transcodersmodel.h"


class MainWindow : public QMainWindow, private Ui::MainWindow
{
    Q_OBJECT

    struct TranscoderServer
    {
        QString hostPort;
        QString login;
        QString password;
        QString title;
    };
    
public:
    explicit MainWindow(QWidget *parent = 0);
    virtual ~MainWindow();
    
protected:
    void changeEvent(QEvent *e);

private slots:
    void on_refreshPushButton_clicked();
    void on_authRequired(QNetworkReply *reply, QAuthenticator *auth);
    void on_requestFinished(QNetworkReply *reply);
    void on_autoRefreshTimerExprired();
    void on_addPushButton_clicked();
    void on_modifyPushButton_clicked();
    void on_savePushButton_clicked();
    void on_saveAllPushButton_clicked();
    void on_deletePushButton_clicked();
    void on_startPushButton_clicked();
    void on_stopPushButton_clicked();
    void on_restartPushButton_clicked();
    void on_restartAllPushButton_clicked();
    void on_addToolButton_clicked();
    void on_removeToolButton_clicked();
    void on_editToolButton_clicked();
    void on_copyPushButton_clicked();
    void on_autoRefreshCheckBox_stateChanged(int arg1);
    void on_autoRefreshTimeSpinBox_valueChanged(int value);
    void on_camsTableView_activated(const QModelIndex &index);

private:
    QString getApiUrl();

    void processList(const QString &data);

    void updateRow(int row, const TranscoderData &transcoder);
    void makeRequest(const QString &apiFunc,
                     QNetworkAccessManager::Operation operation = QNetworkAccessManager::GetOperation,
                     const QString &contentType = QString(),
                     const QByteArray &sendData = QByteArray());
    QStringList getSourcesList(const TranscoderData &data);

private:
    PreviewDialog *preview;

    QNetworkAccessManager         *manager;
    QMap<QNetworkReply*, QString>  replies;
    bool                           requestActive;

    TranscodersModel             *model;
    QSortFilterProxyModel        *sortModel;
    QList<TranscoderServer>       servers;

    QTimer                        autoRefreshTimer;
};


#endif // MAINWINDOW_H
