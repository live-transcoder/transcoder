#include <QProcess>
#include <QUrl>
#include <QDebug>

#include "previewdialog.h"

PreviewDialog::PreviewDialog(QWidget *parent) :
    QDialog(parent)
{
    setupUi(this);
    usePasswordGroupBox->setChecked(false);
}

void PreviewDialog::setLogin(const QString &login)
{
    this->login = login;
}

void PreviewDialog::setPassword(const QString &password)
{
    this->password = password;
}


void PreviewDialog::setSources(const QStringList &sources)
{
    sourcesListWidget->clear();
    sourcesListWidget->addItems(sources);
}


void PreviewDialog::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
        case QEvent::LanguageChange:
            retranslateUi(this);
            break;
        default:
            break;
    }
}


void PreviewDialog::on_sourcesListWidget_itemDoubleClicked(QListWidgetItem *item)
{
    qDebug() << "Selected item: " << item->text();

    QString text = item->text();
    QString player = playerComboBox->currentText();

    if (usePasswordGroupBox->isChecked())
    {
        QString login = this->login;
        QString password = this->password;

        if (overridePasswordGroupBox->isChecked())
        {
            login = loginLineEdit->text();
            password = passwordLineEdit->text();
        }

        QUrl url(text, QUrl::StrictMode);
        if (url.isValid())
        {
            url.setUserName(login);
            url.setPassword(password);
            text = url.toString();
        }
    }

    bool res = QProcess::startDetached(player + " \"" + text + "\"");
    qDebug() << "Started: " << res;
}
