#!/bin/bash

[ "$1" == "-g" ] && ADD="--db-attach=yes"

valgrind -v --track-fds=yes \
            --trace-children=yes \
            --track-origins=yes \
            --undef-value-errors=yes  \
            --leak-check=full \
            --leak-resolution=high \
            --show-reachable=yes $ADD \
            --track-origins=yes \
            ../../transcoder-git-build-debug/src/transcoder/transcoder \
                    -b "mysql://host=10.10.10.129 dbname=transcoder user=transcoder password=x7kYsNaBmJTvqm_myK5wa" -i 2 -c 5 \
            2>&1 | tee valgrind.txt

