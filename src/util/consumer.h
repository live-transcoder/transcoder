/** =============================================================================================
  
    This file is a part of "%ProjectName%" project
    http://htrd.su
    
    @date   2012-4-7
    @brief
    
    Copyright (C) 2002-2012 by Alexander 'hatred' Drozdov <adrozdoff@gmail.com>
    
    This program is free software; you can redistribute it and/or modify
    it under the terms of the version 2 of GNU General Public License as
    published by the Free Software Foundation.
    
    For more information see LICENSE and LICENSE.ru files
    
   ============================================================================================== */

#ifndef CONSUMER_H
#define CONSUMER_H

#include "pointed.hpp"

class Producer;

class Consumer
{
public:
    virtual ~Consumer() {}
    virtual void notify(const Producer *producer) = 0;

};

DECLARE_PTR_TYPE(Consumer);


#endif // CONSUMER_H

