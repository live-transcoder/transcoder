/** =============================================================================================
  
    This file is a part of "%ProjectName%" project
    http://htrd.su
    
    @date   2012-04-04
    @brief
    
    Copyright (C) 2002-2012 by Alexander 'hatred' Drozdov <adrozdoff@gmail.com>
    
    This program is free software; you can redistribute it and/or modify
    it under the terms of the version 2 of GNU General Public License as
    published by the Free Software Foundation.
    
    For more information see LICENSE and LICENSE.ru files
    
   ============================================================================================== */


// Linux spec
#include <signal.h>
#include <unistd.h>


#include <cstdio>
#include <cstdarg>
#include <cstdlib>
#include <limits>
#include <iostream>
#include <sstream>
#include <memory>
#include <locale>


// Boost
#include <boost/thread.hpp>
#include <boost/smart_ptr.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/foreach.hpp>
#include <boost/program_options.hpp>
#include <boost/format.hpp>

#define BOOST_UTF8_BEGIN_NAMESPACE namespace transcoder {
#define BOOST_UTF8_END_NAMESPACE }
#define BOOST_UTF8_DECL
#include <boost/detail/utf8_codecvt_facet.hpp>

// Soci
#include <soci/soci.h>

#include "log/logstream.h"

#include "av/av.h"

#include "decoderthread.h"
#include "encodethread.h"
#include "uri.h"
#include "httpserver.h"
#include "formatdescription.h"
#include "transcoder.h"
#include "transcoderdao.h"
#include "applicationcontext.h"
#include "transcodermanager.h"
#include "../util/utils.h"

#include "transports/file.h"
#include "transports/http.h"
#include "transports/multicast.h"

using namespace transcoder::transport::http;
using namespace transcoder;
using namespace logstream;
namespace options = boost::program_options;

#ifdef WIN32
namespace boost {
 void tss_cleanup_implemented() { }
}
#endif

typedef void (*SignalHandlerPtr)(int);

sig_atomic_t runApp = 1;
void sigQuitHandle(int /*signal*/)
{
    runApp = 0;
}

void sigUsr1Handle(int /*signal*/)
{
    std::clog << '\n'; // move cursor to new line after ^Z
    ScopedTimer::dump();
}


/**
 * Programm usage information
 */
void usage(const options::options_description &desc, bool isError = false)
{
    std::stringstream stream;
    stream << desc;
    std::string helpMsg = stream.str();
    boost::algorithm::replace_all(helpMsg, " [ -- ]", "");

    if (isError)
    {
        std::cerr << helpMsg << std::endl;
        ::exit(1);
    }

    std::cout << helpMsg << std::endl;
    return;
}


static inline
level::level getLogLevel(int level)
{
    if (level < 0)
        return level::silent;
    else if (level > level::trace)
        return level::trace;
    else
        return static_cast<level::level>(level);
}


/**
 * Entry point
 */
int main(int argc, char ** argv)
{
    // Init FFMPEG
    av::init();

    //
    // Locale: UTF8-based
    //
    //locale::global(locale(""));
    //logger(level::debug) << "Locale: " << locale().name() << endl;
    /*locale def("");
    locale utf8(def, new codecvt_byname<wchar_t, char, std::mbstate_t>("en_US.UTF-8"));
    locale::global(utf8);
    logger(level::debug) << "Locale: " << utf8.name() << " / " << def.name() << endl;*/

    //
    // Setting up application context
    //
    {
        ApplicationContext::lease ctx;
        ctx->connection         = string();
        ctx->httpPort           = 8080;
        ctx->rtspPort           = 8554;
        ctx->daemonize          = false;
        ctx->logFile            = "/tmp/transcoder.log";
        ctx->verbosity          = level::info;
        ctx->transcoderId       = 0;
        ctx->maxHttpConnections = 1024;
        ctx->pool               = 0;

        ctx->ffmpegLogLevel     = "fatal";
        ctx->encodeBufferSize   = 25;   // about 1 sec (with FPS 25)
        ctx->sendBufferSize     = 100;  // about 4 sec (with FPS 25)
        ctx->encodersWaitTime   = -1;

        size_t poolSize         = 50;

        //
        // Command line options parse
        //
        options::options_description options(std::string(argv[0]).append(" options"));
        options.add_options()
                (",b", options::value<std::string>(&ctx->connection),      "Database connection descibtion (see README.sql for additional info)")
                (",p", options::value<uint16_t>(&ctx->httpPort),           (boost::format("HTTP port (default: %d)") % ctx->httpPort).str().c_str())
                (",r", options::value<uint16_t>(&ctx->rtspPort),           (boost::format("RTSP port (default: %d)") % ctx->rtspPort).str().c_str())
                (",d", options::value<bool>(&ctx->daemonize),              (boost::format("Run as daemon (default: %d)") % ctx->daemonize).str().c_str())
                (",l", options::value<std::string>(&ctx->logFile),         (boost::format("Log file (default: %s") % ctx->logFile).str().c_str())
                (",i", options::value<int>(&ctx->transcoderId),            (boost::format("Unique transcoder id (default: %d)") % ctx->transcoderId).str().c_str())
                (",c", options::value<uint32_t>(&ctx->maxHttpConnections), (boost::format("Maximum amount of simulatrinously HTTP connections (default: %d)") % ctx->maxHttpConnections).str().c_str())
                (",s", options::value<size_t>(&poolSize),                  (boost::format("Database pool size (default: %d)") % poolSize).str().c_str())
                ("encode-buffer", options::value<uint32_t>(&ctx->encodeBufferSize), "Set encode buffer size in frames (individual frame size may be variable)")
                ("send-buffer",   options::value<uint32_t>(&ctx->sendBufferSize),   "Set send buffer size in packets (individual packet size may be variable)")
                ("ffmpeg-log-level", options::value<string>(&ctx->ffmpegLogLevel),  "Set FFMPEG log level, numeric or symbol ('quiet','panic', 'fatal','error','warning','info','verbose','debug')")
                ("encoders-wait-time", options::value<int64_t>(&ctx->encodersWaitTime), "Encoders wait timeout (in msec), if timeout will be occured, frame will dropped")
                ("verbose,v", options::value<int>(&ctx->verbosity),              "Increase verbosity level")
                ("help,h", "This screen")
                ;

        options::variables_map args;
        int style = options::command_line_style::default_style |
                    (options::command_line_style::allow_long);
        options::store(options::command_line_parser (argc, argv).options(options)
                       .style(style)
                       .run (), args);
        options::notify (args);

        if (args.count("-h") || ctx->connection.empty())
        {
            usage(options);
            return ctx->connection.empty() ? 1 : 0;
        }

        ctx->pool = new soci::connection_pool(poolSize);

        for (int i = 0; i < poolSize; ++i)
        {
            soci::session & sql = ctx->pool->at(i);
            sql.open(ctx->connection);
        }

        av::setFFmpegLoggingLevel(ctx->ffmpegLogLevel);

        // Set logging level
        logger().set_threshold(getLogLevel(ctx->verbosity));
    }    


    // Connect POSIX signals
    runApp = 1;
    ::signal(SIGQUIT, sigQuitHandle);
    ::signal(SIGTERM, sigQuitHandle);
    ::signal(SIGINT,  sigQuitHandle);
    ::signal(SIGUSR1, sigUsr1Handle);
    ::signal(SIGTSTP, sigUsr1Handle);

    // Daemonize
    if (ApplicationContext::instance->daemonize)
    {
        pid_t pid = ::fork();
        if (pid < 0) // error
        {
            logger(level::fatal) << "Can't daemonize\n";
            exit(1);

        }
        else if (pid > 0) // parent
        {
            exit(0);
        }

        setsid();

        close(STDIN_FILENO);
        close(STDOUT_FILENO);
        close(STDERR_FILENO);

        int fd1 = open(ApplicationContext::instance->logFile.c_str(),
                       O_WRONLY|O_CREAT|O_APPEND, S_IRUSR|S_IWUSR);
        int fd2 = open(ApplicationContext::instance->logFile.c_str(),
                       O_WRONLY|O_CREAT|O_APPEND, S_IRUSR|S_IWUSR);

        dup2(fd1, STDOUT_FILENO);
        dup2(fd2, STDERR_FILENO);
    }


    //
    // Server parts
    //
    {
        ApplicationContext::lease ctx;
        //ctx->rolesManager = DbPionRolesManagerPtr(new DbPionRolesManager(ctx->pool));
        //ctx->userManager  = DbPionUserManagerPtr(new DbPionUserManager(ctx->pool));
        //ctx->httpAuth     = HTTPBasicRbacAuthPtr(new HTTPBasicRbacAuth(ctx->userManager, ctx->rolesManager));
        //ctx->httpAuth->addRestrict("*", "*");
        //ctx->httpAuth->addPermit("/admin/",                "admin");
        //ctx->httpAuth->addPermit("/admin/transcoder/list", "operator");

        ctx->httpServer   = HttpServerPtr(new HttpServer());
        //ctx->httpServer->setAuthentication(ctx->httpAuth);
        ctx->httpServer->setMaxConnections(ctx->maxHttpConnections);
        ctx->httpServer->start(ctx->httpPort);
    }

    //
    // Register built-in transports
    //
    transport::File::registerTransport();
    transport::Http::registerTransport();
    transport::Multicast::registerTransport();

    //
    // Start transcoders
    //
    TranscoderManager::instance->load();
    TranscoderManager::instance->startAll();

    while (runApp == 1)
    {
        sleep(1);
    }


    TranscoderManager::instance->save();
    TranscoderManager::instance->stopAll();

    ApplicationContext::instance->httpServer->stop();
    logger(level::info) << "HttpServer finished" << endl;
    logger(level::info) << "Transcoder stoped" << endl;

    return 0;
}

