#ifndef ABSTRACTENCODECONTEXT_H
#define ABSTRACTENCODECONTEXT_H

#include <string>
#include <map>
#include <vector>

#include "av/ffmpeg.h"
#include "av/container.h"
#include "av/containerformat.h"
#include "av/stream.h"
#include "av/streamcoder.h"
#include "av/videoresampler.h"
#include "av/audioresampler.h"

#include "formatdescription.h"

namespace transcoder {
namespace encode {

/**
 * @brief The AbstractEncodeContext class
 *
 * Abstract class for creating encoders and muxers
 *
 */
class BaseEncodeContext
{
public:
    BaseEncodeContext();
    explicit BaseEncodeContext(const FormatDescription &description);
    virtual ~BaseEncodeContext();

    bool init(const FormatDescription &description);

    bool        isValid() const { return isValidFlag; }
    std::string getMimeType() const;

    const FormatDescription &getFormatDescription() const { return description; }

    int mappedStreamIndex(int inputStreamIndex);

protected:
    void initFilters(const char *filtersDesc);

    av::ContainerFormatPtr          format;
    av::ContainerPtr                container;

    std::vector<av::CodecPtr>       codecs;
    std::vector<av::StreamPtr>      streams;
    std::vector<av::StreamCoderPtr> coders;

#if 0
    // HACK
    AVFilterContext          *bufferSinkCtx;
    AVFilterContext          *bufferSrcCtx;
    AVFilterGraph            *filterGraph;
    AVFilterBufferRef        *picref;
#endif

private:
    FormatDescription         description;
    bool                      isValidFlag;
};

}} // ::transcoder::encode

#endif // ABSTRACTENCODECONTEXT_H
