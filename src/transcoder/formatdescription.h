#ifndef FORMATDESCRIPTION_H
#define FORMATDESCRIPTION_H

#include <map>
#include <string>
#include <vector>

#include "av/ffmpeg.h"
#include "av/rational.h"

namespace transcoder
{

typedef std::map<std::string, std::string> OptionsList;

class StreamDescription
{
public:
    StreamDescription();

    void parseOptionsList(const OptionsList &options);
    OptionsList toOptionsList();

    /**
     * Calculate media type by codec name, or, it it does not set by mediaType field
     * @return
     */
    AVMediaType getMediaType() const;
    void reset();

    AVMediaType mediaType;
    std::string codec;
    OptionsList meta;

    int mapFromIndex; /// original stream id
    int mapToIndex;   /// new stream id

    // common
    int          bitRate;
    int          bitRateMin;
    int          bitRateMax;
    av::Rational timeBase;

    // audio specific
    int      audioChannels;
    uint64_t audioChannelsLayout;
    int      audioSampleRate;
    AVSampleFormat audioSampleFormat;

    // video
    int                 videoStrict;
    int                 videoBufferSize;
    int                 videoBitRateTolerance;
    int                 width;
    int                 height;
    av::Rational        videoFrameRate;
    int                 videoGopSize;
    av::Rational        sampleAspectRatio;
    PixelFormat         pixFormat;

};

class FormatDescription
{
public:
    FormatDescription();

    void parseOptionsList(const OptionsList &options);
    OptionsList toOptionsList();

    void reset();

    std::string                      format;
    OptionsList                      meta;

    std::vector<StreamDescription> streams;

    std::string         uri;
    std::string         transport;
    std::string         resource;
    std::string         roles;

    bool                isEnabled;
};

} // ::transcoder


#endif // FORMATDESCRIPTION_H
