#include "httpbasicrbacauth.h"

namespace pion {
namespace net {


HTTPBasicRbacAuth::HTTPBasicRbacAuth(PionUserManagerPtr userManager, PionRolesManagerPtr rolesManager, const string &realm)
    : HTTPBasicAuth(userManager, realm),
      HTTPRbac(rolesManager),
      userManager(userManager)
{
}

HTTPBasicRbacAuth::~HTTPBasicRbacAuth()
{
}

bool HTTPBasicRbacAuth::handleRequest(HTTPRequestPtr &request, TCPConnectionPtr &tcp_conn)
{
    // if we are here, we need to check if access authorized...
    std::string authorization = request->getHeader(HTTPTypes::HEADER_AUTHORIZATION);
    if (!authorization.empty())
    {
        std::string credentials;
        if (parseAuthorization(authorization, credentials))
        {
            std::string username;
            std::string password;

            if (parseCredentials(credentials, username, password))
            {
                // match username/password
                PionUserPtr user = userManager->getUser(username, password);
                if (user)
                {
                    request->setUser(user);
                    bool result = isAllowed(request);
                    if (result)
                        return true;
                }
            }
        }
    }

    // user not found
    handleUnauthorized(request, tcp_conn);
    return false;
}

void HTTPBasicRbacAuth::addPermit(const string &resource, const string &role)
{
    HTTPRbac::addPermit(resource, role);
}

void HTTPBasicRbacAuth::addPermit(const string &resource)
{
    HTTPRbac::addPermit(resource, "*");
}

void HTTPBasicRbacAuth::addRestrict(const string &resource, const string &role)
{
    HTTPRbac::addRestrict(resource, role);
}

void HTTPBasicRbacAuth::addRestrict(const string &resource)
{
    HTTPRbac::addRestrict(resource, "*");
}

//void HTTPBasicRbacAuth::removePermit(const string &resource, const string &role)
//{
//}

//void HTTPBasicRbacAuth::removeRestrict(const string &resource, const string &role)
//{
//}

//void HTTPBasicRbacAuth::clearPermit(const string &resource)
//{
//}

//void HTTPBasicRbacAuth::clearRestrict(const string &resource)
//{
//}




}} // ::pion::net
