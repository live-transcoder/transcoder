#include <algorithm>

#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QAuthenticator>
#include <QtNetwork/QNetworkReply>

#include <QMessageBox>
#include <QDebug>
#include <QSettings>

#include <qjson/serializer.h>
#include <qjson/parser.h>

#include "mainwindow.h"
#include "addeditdialog.h"
#include "addserverdialog.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
{
    setupUi(this);

    manager = new QNetworkAccessManager(this);
    preview = new PreviewDialog(this);

    connect(manager, SIGNAL(authenticationRequired(QNetworkReply*,QAuthenticator*)),
            this,    SLOT(on_authRequired(QNetworkReply*,QAuthenticator*)));
    connect(manager, SIGNAL(finished(QNetworkReply*)),
            this,    SLOT(on_requestFinished(QNetworkReply*)));

    model = new TranscodersModel(this);
    sortModel = new QSortFilterProxyModel(this);
    sortModel->setSourceModel(model);

    camsTableView->setModel(sortModel);
    camsTableView->horizontalHeader()->setResizeMode(0, QHeaderView::ResizeToContents);
    camsTableView->horizontalHeader()->setResizeMode(1, QHeaderView::Stretch);

    requestActive = false;

    QSettings cfg(QSettings::IniFormat, QSettings::UserScope,
                  QCoreApplication::organizationName(),
                  QCoreApplication::applicationName());

    QVariantMap defaultHost;
    defaultHost["host"] = "127.0.0.1";
    defaultHost["login"] = "";
    defaultHost["password"] = "";
    defaultHost["title"] = defaultHost["host"];
    QVariantList hosts = cfg.value("core/hosts", defaultHost).toList();
    hostPortComboBox->clear();
    for (int i = 0; i < hosts.size(); ++i)
    {
        TranscoderServer server;
        QVariantMap      host = hosts.at(i).toMap();
        server.hostPort = host["host"].toString();
        server.login    = host["login"].toString();
        server.password = host["password"].toString();
        server.title    = host["title"].toString();

        if (server.hostPort.trimmed().isEmpty() || server.hostPort.trimmed().isNull())
            continue;

        if (server.title.isEmpty())
            hostPortComboBox->addItem(server.hostPort);
        else
            hostPortComboBox->addItem(server.title);
        servers.append(server);
    }

    int index = cfg.value("core/selected_host", 0).toInt();
    hostPortComboBox->setCurrentIndex(index);

    connect(&autoRefreshTimer, SIGNAL(timeout()),
            this,              SLOT(on_autoRefreshTimerExprired()));
}


MainWindow::~MainWindow()
{
    QSettings cfg(QSettings::IniFormat, QSettings::UserScope,
                  QCoreApplication::organizationName(),
                  QCoreApplication::applicationName());

    QVariantList hosts;
    for (int i = 0; i < servers.size(); ++i)
    {
        QVariantMap host;

        host["host"] = servers[i].hostPort;
        host["login"] = servers[i].login;
        host["password"] = servers[i].password;
        host["title"] = servers[i].title;

        hosts << host;
    }
    cfg.setValue("core/hosts", hosts);
    cfg.setValue("core/selected_host", hostPortComboBox->currentIndex());

}

void MainWindow::changeEvent(QEvent *e)
{
    QMainWindow::changeEvent(e);
    switch (e->type()) {
        case QEvent::LanguageChange:
            retranslateUi(this);
            break;
        default:
            break;
    }
}

void MainWindow::on_refreshPushButton_clicked()
{
    makeRequest("list");
}

void MainWindow::on_authRequired(QNetworkReply *reply, QAuthenticator *auth)
{
    Q_UNUSED(reply);

    int currentIndex = hostPortComboBox->currentIndex();
    if (currentIndex < 0)
        return;

    auth->setUser(servers[currentIndex].login);
    auth->setPassword(servers[currentIndex].password);
}

void MainWindow::on_requestFinished(QNetworkReply *reply)
{
    QString cmd = replies[reply];
    replies.remove(reply);

    QByteArray raw = reply->readAll();
    QString data = QString::fromUtf8(raw.data(), raw.size());
    qDebug() << data;

    QJson::Parser parser;
    bool ok;

    do
    {
        QVariantMap result = parser.parse(data.toUtf8(), &ok).toMap();
        if (!ok)
        {
            QMessageBox::critical(this, "Request error", data);
            break;
        }

        QString errorText = result["error_text"].toString();
        int     errorCode = result["error_code"].toInt();

        if (errorCode != 0)
        {
            QMessageBox::critical(this, "Request error", errorText);
            break;
        }

        if (cmd == "list")
        {
            processList(data);
        }
    }
    while (0);

    requestActive = false;
    if (!autoRefreshCheckBox->isChecked())
        commandsFrame->setEnabled(true);
}

void MainWindow::on_autoRefreshTimerExprired()
{
    makeRequest("list");
}


struct TranscoderDataAlphabeticalCompare
{
    bool operator()(const TranscoderData &first, const TranscoderData &second)
    {
        if (first.name < second.name)
            return true;
        else
            return false;
    }
};

struct TranscoderDataNotEqual
{
    bool operator()(const TranscoderData &first, const TranscoderData &second)
    {
        if (first.name != second.name)
            return true;
        else
            return false;
    }
};


void MainWindow::processList(const QString &data)
{
    QJson::Parser parser;
    bool ok;

    QVariantMap result = parser.parse(data.toUtf8(), &ok).toMap();

    int row = 0;

    QList<TranscoderData> transcoders;
    QList<TranscoderData> currentTranscoders = model->getTranscoderData();
    foreach (QVariant tmp, result["transcoders"].toList())
    {
        QVariantMap transcoder = tmp.toMap();

        TranscoderData item;
        item.name = transcoder["name"].toString();
        item.description  = transcoder["description"].toString();
        item.enabled      = transcoder["enabled"].toBool();
        item.source       = transcoder["source"].toMap()["uri"].toString();
        item.sourceFormat = transcoder["source"].toMap()["format"].toString();
        item.active       = transcoder["active"].toBool();
        item.started      = transcoder["started"].toBool();

        foreach (QVariant tmp2, transcoder["destination"].toList())
        {
            QVariantMap format = tmp2.toMap();
            FormatData formatItem;

            formatItem.format = format["format"].toString();
            formatItem.transport = format["transport"].toString();
            formatItem.resource  = format["resource"].toString();
            formatItem.roles     = format["roles"].toString();

            item.destinations.append(formatItem);
        }

        transcoders.append(item);

        ++row;
    }

    for (int i = 0; i < transcoders.size(); ++i)
    {
        model->putTranscoderData(transcoders[i].name, transcoders[i]);
    }

    std::sort(transcoders.begin(), transcoders.end(), TranscoderDataAlphabeticalCompare());
    std::sort(currentTranscoders.begin(), currentTranscoders.end(), TranscoderDataAlphabeticalCompare());
    {
        QVector<TranscoderData>::iterator it;
        QVector<TranscoderData> result;
        result.resize(qMax(transcoders.size(), currentTranscoders.size()));

        it = std::set_difference(currentTranscoders.begin(),
                                 currentTranscoders.end(),
                                 transcoders.begin(),
                                 transcoders.end(),
                                 result.begin(),
                                 TranscoderDataNotEqual());

        QVector<TranscoderData>::iterator it2;
        for (it2 = result.begin(); it2 != it; ++it2)
        {
            model->removeTranscoderData(it2->name);

            qDebug() << "Remove data: " << it2->name;
        }
    }
}

void MainWindow::makeRequest(const QString &apiFunc,
                             QNetworkAccessManager::Operation operation,
                             const QString &contentType,
                             const QByteArray &sendData)
{
    QString api = getApiUrl() + apiFunc;

    if (requestActive)
        return;

    QNetworkRequest request;
    request.setUrl(QUrl(api));
    if (!contentsRect().isEmpty())
        request.setHeader(QNetworkRequest::ContentTypeHeader, contentType);

    QNetworkReply *reply = 0 ;

    switch (operation)
    {
        case QNetworkAccessManager::HeadOperation:
            break;
        case QNetworkAccessManager::GetOperation:
            reply = manager->get(request);
            break;
        case QNetworkAccessManager::PutOperation:
            break;
        case QNetworkAccessManager::PostOperation:
            qDebug() << "Send data:\n" << sendData;
            reply = manager->post(request, sendData);
            break;
        case QNetworkAccessManager::DeleteOperation:
            break;
        case QNetworkAccessManager::CustomOperation:
            break;
        case QNetworkAccessManager::UnknownOperation:
            break;
    }

    if (reply)
    {
        replies.insert(reply, apiFunc);
        requestActive = true;
        commandsFrame->setEnabled(false);
    }
}

QStringList MainWindow::getSourcesList(const TranscoderData &data)
{
    QStringList result;

    int serverId = hostPortComboBox->currentIndex();
    if (serverId < 0)
    {
        return result;
    }

    for (int i = 0; i < data.destinations.size(); ++i)
    {
        QString uri;
        const QString &transport = data.destinations[i].transport;
        const QString &resource  = data.destinations[i].resource;
        if (transport == "http" || transport == "rtsp")
        {
            uri = "http://" + servers[serverId].hostPort + "/media" + resource;
            QUrl url(uri, QUrl::TolerantMode);
            if (transport == "rtsp")
            {
                url.setPort(554);
                url.setScheme(transport);
                uri = url.toString();
            }

            result.append(uri);
        }
    }

    return result;
}


QString MainWindow::getApiUrl()
{
    int serverId = hostPortComboBox->currentIndex();
    if (serverId < 0)
    {
        return QString();
    }

    QString api = "http://" + servers[serverId].hostPort + "/admin/transcoder/";
    return api;
}


void MainWindow::on_addPushButton_clicked()
{
    AddEditDialog *dlg = new AddEditDialog(this);
    if (dlg->exec())
    {
        TranscoderData data = dlg->getData();
        model->addTranscoderData(data);

        QJson::Serializer serializer;
        QVariantMap root;
        QVariantMap transcoder;
        transcoder.insert("name", data.name);
        transcoder.insert("description", data.description);
        transcoder.insert("enabled", data.enabled);

        QVariantMap  source;
        source["uri"] = data.source;
        if (!data.sourceFormat.isEmpty())
            source["format"] = data.sourceFormat;
        transcoder.insert("source", source);

        QVariantList destinations;
        for (int i = 0; i < data.destinations.size(); ++i)
        {
            QVariantMap destination;
            destination["format"]    = data.destinations[i].format;
            destination["transport"] = data.destinations[i].transport;
            destination["resource"]  = data.destinations[i].resource;
            destination["roles"]     = data.destinations[i].roles;

            destinations << destination;
        }
        transcoder["destination"] = destinations;

        root["transcoder"] = transcoder;
        QByteArray json = serializer.serialize(root);

        makeRequest("add", QNetworkAccessManager::PostOperation, "application/json", json);
    }

    dlg->deleteLater();
}

void MainWindow::on_modifyPushButton_clicked()
{
    QModelIndex idx = camsTableView->currentIndex();
    QModelIndex sourceIndex = sortModel->mapToSource(idx);
    if (!sourceIndex.isValid())
        return;

    int currentRow = sourceIndex.row();
    if (currentRow < 0)
        return;

    TranscoderData data = model->getTranscoderData(sourceIndex);
    const QString        name = data.name;

    AddEditDialog *dlg = new AddEditDialog(this);
    dlg->setData(data);
    dlg->setNameFieldEnabled(false);

    if (dlg->exec())
    {
        data = dlg->getData();
        model->updateTranscoderData(sourceIndex, data);

        QJson::Serializer serializer;
        QVariantMap root;
        QVariantMap transcoder;
        transcoder.insert("name", name);
        transcoder.insert("description", data.description);
        transcoder.insert("enabled", data.enabled);

        QVariantMap  source;
        source["uri"] = data.source;
        if (!data.sourceFormat.isEmpty())
            source["format"] = data.sourceFormat;
        transcoder.insert("source", source);

        QVariantList destinations;
        for (int i = 0; i < data.destinations.size(); ++i)
        {
            QVariantMap destination;
            destination["format"]    = data.destinations[i].format;
            destination["transport"] = data.destinations[i].transport;
            destination["resource"]  = data.destinations[i].resource;
            destination["roles"]     = data.destinations[i].roles;

            destinations << destination;
        }
        transcoder["destination"] = destinations;

        root["transcoder"] = transcoder;
        QByteArray json = serializer.serialize(root);

        makeRequest("modify", QNetworkAccessManager::PostOperation, "application/json", json);
    }

    dlg->deleteLater();
}

void MainWindow::on_savePushButton_clicked()
{
    QModelIndex idx = camsTableView->currentIndex();
    QModelIndex sourceIndex = sortModel->mapToSource(idx);
    if (!sourceIndex.isValid())
        return;

    int currentRow = sourceIndex.row();
    if (currentRow < 0)
        return;

    const TranscoderData &data = model->getTranscoderData(sourceIndex);
    const QString        name = data.name;

    QJson::Serializer serializer;
    QVariantMap root;
    QVariantMap transcoder;
    transcoder["name"] = name;
    root["transcoder"] = transcoder;
    QByteArray json = serializer.serialize(root);

    makeRequest("save", QNetworkAccessManager::PostOperation, "application/json", json);
}

void MainWindow::on_saveAllPushButton_clicked()
{
    makeRequest("save-all");
}

void MainWindow::on_deletePushButton_clicked()
{
    QModelIndex idx = camsTableView->currentIndex();
    QModelIndex sourceIndex = sortModel->mapToSource(idx);
    if (!sourceIndex.isValid())
        return;

    int currentRow = sourceIndex.row();
    if (currentRow < 0)
        return;

    const TranscoderData &data = model->getTranscoderData(sourceIndex);
    const QString         name = data.name;

    if (QMessageBox::question(this,
                              tr("Transcoder delete"),
                              tr("Do you realy want delete transcoder: ") + name + "?",
                              QMessageBox::Yes | QMessageBox::No) != QMessageBox::Yes)
    {
        return;
    }

    QJson::Serializer serializer;
    QVariantMap root;
    QVariantMap transcoder;
    transcoder["name"] = name;
    root["transcoder"] = transcoder;
    QByteArray json = serializer.serialize(root);

    model->removeTranscoderData(sourceIndex);
    makeRequest("delete", QNetworkAccessManager::PostOperation, "application/json", json);
}


void MainWindow::on_startPushButton_clicked()
{
    QModelIndex idx = camsTableView->currentIndex();
    QModelIndex sourceIndex = sortModel->mapToSource(idx);
    if (!sourceIndex.isValid())
        return;

    int currentRow = sourceIndex.row();
    if (currentRow < 0)
        return;

    const TranscoderData &data = model->getTranscoderData(sourceIndex);
    const QString        name = data.name;

    QJson::Serializer serializer;
    QVariantMap root;
    QVariantMap transcoder;
    transcoder["name"] = name;
    root["transcoder"] = transcoder;
    QByteArray json = serializer.serialize(root);

    makeRequest("start", QNetworkAccessManager::PostOperation, "application/json", json);
}

void MainWindow::on_stopPushButton_clicked()
{
    QModelIndex idx = camsTableView->currentIndex();
    QModelIndex sourceIndex = sortModel->mapToSource(idx);
    if (!sourceIndex.isValid())
        return;

    int currentRow = sourceIndex.row();
    if (currentRow < 0)
        return;

    const TranscoderData &data = model->getTranscoderData(sourceIndex);
    const QString        name = data.name;

    QJson::Serializer serializer;
    QVariantMap root;
    QVariantMap transcoder;
    transcoder["name"] = name;
    root["transcoder"] = transcoder;
    QByteArray json = serializer.serialize(root);

    makeRequest("stop", QNetworkAccessManager::PostOperation, "application/json", json);
}

void MainWindow::on_restartPushButton_clicked()
{
    QModelIndex idx = camsTableView->currentIndex();
    QModelIndex sourceIndex = sortModel->mapToSource(idx);
    if (!sourceIndex.isValid())
        return;

    int currentRow = sourceIndex.row();
    if (currentRow < 0)
        return;

    const TranscoderData &data = model->getTranscoderData(sourceIndex);
    const QString         name = data.name;

    QJson::Serializer serializer;
    QVariantMap root;
    QVariantMap transcoder;
    transcoder["name"] = name;
    root["transcoder"] = transcoder;
    QByteArray json = serializer.serialize(root);

    makeRequest("restart", QNetworkAccessManager::PostOperation, "application/json", json);
}

void MainWindow::on_restartAllPushButton_clicked()
{
    makeRequest("restart-all");
}


void MainWindow::on_addToolButton_clicked()
{
    AddServerDialog *dlg = new AddServerDialog(this);

    if (dlg->exec())
    {
        TranscoderServer server;
        server.hostPort = dlg->getHostAndPort();
        server.login    = dlg->getLogin();
        server.password = dlg->getPassword();
        server.title    = dlg->getTitle();

        servers.append(server);

        if (server.title.isEmpty())
            hostPortComboBox->addItem(server.hostPort);
        else
            hostPortComboBox->addItem(server.title);
    }

    dlg->deleteLater();
}

void MainWindow::on_removeToolButton_clicked()
{
    int serverId = hostPortComboBox->currentIndex();
    if (serverId < 0)
        return;

    if (QMessageBox::question(this, tr("Server delete"),
                              tr("Do you realy want delete server: ") + servers[serverId].hostPort + "?",
                              QMessageBox::Yes | QMessageBox::No) != QMessageBox::Yes)
    {
        return;
    }

    hostPortComboBox->removeItem(serverId);
    servers.removeAt(serverId);
}

void MainWindow::on_editToolButton_clicked()
{
    int serverId = hostPortComboBox->currentIndex();
    if (serverId < 0)
        return;

    TranscoderServer &server = servers[serverId];
    AddServerDialog *dlg = new AddServerDialog(this);

    dlg->setHostAndPort(server.hostPort);
    dlg->setLogin(server.login);
    dlg->setPassword(server.password);
    dlg->setTitle(server.title);

    if (dlg->exec())
    {
        server.hostPort = dlg->getHostAndPort();
        server.login    = dlg->getLogin();
        server.password = dlg->getPassword();
        server.title    = dlg->getTitle();

        if (server.title.isEmpty())
            hostPortComboBox->setItemText(serverId, server.hostPort);
        else
            hostPortComboBox->setItemText(serverId, server.title);
    }

    dlg->deleteLater();
}


void MainWindow::on_copyPushButton_clicked()
{
    QModelIndex idx = camsTableView->currentIndex();
    QModelIndex sourceIndex = sortModel->mapToSource(idx);
    if (!sourceIndex.isValid())
        return;

    int currentRow = sourceIndex.row();
    if (currentRow < 0)
        return;

    const TranscoderData &data = model->getTranscoderData(sourceIndex);

    AddEditDialog *dlg = new AddEditDialog(this);
    dlg->setData(data);

    if (dlg->exec())
    {
        TranscoderData dataNew = dlg->getData();
        model->addTranscoderData(dataNew);

        QJson::Serializer serializer;
        QVariantMap root;
        QVariantMap transcoder;
        transcoder.insert("name", dataNew.name);
        transcoder.insert("description", dataNew.description);
        transcoder.insert("enabled", dataNew.enabled);

        QVariantMap  source;
        source["uri"] = dataNew.source;
        if (!dataNew.sourceFormat.isEmpty())
            source["format"] = dataNew.sourceFormat;

        transcoder.insert("source", source);

        QVariantList destinations;
        for (int i = 0; i < dataNew.destinations.size(); ++i)
        {
            QVariantMap destination;
            destination["format"]    = dataNew.destinations[i].format;
            destination["transport"] = dataNew.destinations[i].transport;
            destination["resource"]  = dataNew.destinations[i].resource;
            destination["roles"]     = dataNew.destinations[i].roles;

            destinations << destination;
        }
        transcoder["destination"] = destinations;

        root["transcoder"] = transcoder;
        QByteArray json = serializer.serialize(root);

        makeRequest("add", QNetworkAccessManager::PostOperation, "application/json", json);
    }

    dlg->deleteLater();
}

void MainWindow::on_autoRefreshCheckBox_stateChanged(int state)
{
    if (state == Qt::Unchecked)
    {
        autoRefreshTimer.stop();
        autoRefreshTimeSpinBox->setEnabled(false);
        commandsFrame->setEnabled(true);
    }
    else if (state == Qt::Checked)
    {
        autoRefreshTimer.start(autoRefreshTimeSpinBox->value() * 1000);
        autoRefreshTimeSpinBox->setEnabled(true);
        commandsFrame->setEnabled(false);
    }
}

void MainWindow::on_autoRefreshTimeSpinBox_valueChanged(int value)
{
    autoRefreshTimer.setInterval(value * 1000);
}


void MainWindow::on_camsTableView_activated(const QModelIndex &index)
{
    QModelIndex sourceIndex = sortModel->mapToSource(index);
    if (!sourceIndex.isValid() || sourceIndex.row() >= model->rowCount())
        return;

    const TranscoderData &data = model->getTranscoderData(sourceIndex);

    QStringList destUris;

    logTextEdit->clear();
    logTextEdit->append(QString("Transcoder:    ") + data.name + "\n");
    logTextEdit->append(QString("Description:   ") + data.description + "\n");
    logTextEdit->append(QString("Enabled:       ") + (data.enabled ? "true" : "false") + "\n");
    logTextEdit->append(QString("Source:        ") + data.source + "\n");

    if (!data.sourceFormat.isEmpty())
        logTextEdit->append(QString("Source format: ") + data.sourceFormat + "\n");

    logTextEdit->append("Destinations: \n");
    for (int i = 0; i < data.destinations.size(); ++i)
    {
        logTextEdit->append(QString("    format:    ") + data.destinations[i].format + "\n");
        logTextEdit->append(QString("         transport: ") + data.destinations[i].transport + "\n");
        logTextEdit->append(QString("         resource:  ") + data.destinations[i].resource + "\n");
        logTextEdit->append(QString("         roles:     ") + data.destinations[i].roles + "\n");
    }

    destUris = getSourcesList(data);

    logTextEdit->append("Media URLS: \n");
    for (int i = 0; i < destUris.size(); ++i)
    {
        logTextEdit->append(QString("   ") + destUris[i] + "\n");
    }

    logTextEdit->append("\n");
    QTextCursor cur = logTextEdit->textCursor();
    cur.movePosition(QTextCursor::Start);
    logTextEdit->setTextCursor(cur);

    // Show preview dialog
    destUris.prepend(data.source);

    preview->setSources(destUris);

    int serverId = hostPortComboBox->currentIndex();
    if (serverId >= 0)
    {
        preview->setLogin(servers[serverId].login);
        preview->setPassword(servers[serverId].password);
    }
    preview->exec();

}

