#!/bin/sh

[ "$1" == "-g" ] && ADD="--db-attach=yes"

valgrind -v --tool=callgrind \
            --track-fds=yes \
            --trace-children=yes \
            $ADD \
            ./transcoder \
                    -b "mysql://host=10.10.10.129 dbname=transcoder user=transcoder password=x7kYsNaBmJTvqm_myK5wa" -p 8080 -i 3 \
            2>&1 | tee callgrind.txt
