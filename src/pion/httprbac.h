#ifndef HTTPRBAC_H
#define HTTPRBAC_H

#include <string>
#include <set>

#include <boost/noncopyable.hpp>
#include <boost/thread.hpp>

#include <pion/net/HTTPRequest.hpp>

#include "pionrolesmanager.h"

namespace pion {
namespace net {

using namespace std;

class HTTPRbac : private boost::noncopyable
{
public:
    explicit HTTPRbac(const PionRolesManagerPtr &rolesManager);
    virtual ~HTTPRbac();

    virtual void addRestrict(const string &resource, const string &role);
    virtual void addPermit(const string &resource, const string &role);

    virtual void removePermit(const string &resource, const string &role);
    virtual void removeRestrict(const string &resource, const string &role);
    virtual void clearPermit(const string &resource);
    virtual void clearRestrict(const string &resource);
    virtual void clearAll();


    virtual bool isAllowed(HTTPRequestPtr& request);

protected:
    typedef set<string>                  AuthResourceSet;
    typedef map<string, AuthResourceSet> AuthResourceAndRoleMap;

protected:
    bool findResource(const AuthResourceAndRoleMap &resources, const string &resource, const string &role);
    bool findResource(const AuthResourceAndRoleMap &resources, const string &resource, const set<string> &roles);

private:
    PionRolesManagerPtr rolesManager;

    AuthResourceAndRoleMap permitList;
    AuthResourceAndRoleMap restrictList;

    mutable boost::mutex   resourceMutex;
};

}} // ::pion::net

#endif // HTTPRBAC_H
