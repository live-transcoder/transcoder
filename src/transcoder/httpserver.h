#ifndef HTTPSERVER_H
#define HTTPSERVER_H

#include <iostream>

#include <boost/function.hpp>
#include <boost/thread.hpp>

#include "util/mutex.hpp"
#include "http_server/server.hpp"
#include "http_server/auth/auth.hpp"

#include "pointed.hpp"

namespace transcoder {
namespace transport {
namespace http {

using namespace std;
using namespace ::http::server;

class HttpServer
{

public:
    HttpServer();

    void start(uint16_t port = 8080);
    void stop();

    void 	setAuthentication(const auth_ptr &auth);
    void    setMaxAdminConnections(int count);

    void    setMaxConnections(int connectionsCount);
    int     getMaxConnections();

    void    addHandler(const std::string& resource, const request_handler& handler);
    void    removeHandler(const std::string& resource);

private:
    void adminHandler(request_ptr &req,      reply_ptr &rep);
    void badRequestHandler(request_ptr &req, reply_ptr &rep);
    void errorHandler(request_ptr &req,      reply_ptr &rep, const string &message);

private:
    server_ptr   httpServer;
    auth_ptr     httpAuth;

    int          maxConnectionsCount;

    int                        maxAdminConnections;
    int                        adminConnections;
    Mutex                      adminConnectionsMutex;
};

DECLARE_PTR_TYPE(HttpServer);

}}} // tra7nscoder::transport::http


#endif // HTTPSERVER_H
